package com.psybergate.grad2021.jeefnds.servlets.hwweb6;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.List;

@WebListener
public class NameInitializer implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    List<String> names = new ArrayList<>();

    names.add("James");
    names.add("Chris");
    names.add("Daniel");
    names.add("John");
    names.add("Samson");
    names.add("Peter");
    names.add("Piet");
    names.add("Jones");
    names.add("Judas");

    sce.getServletContext().setAttribute("names", names);
  }
}
