package com.psybergate.grad2021.jeefnds.enterprise.hwent0;

import java.time.LocalDate;

public class CustomerUtils {

  public static void main(String[] args) {
    Customer customer = new Customer(238, "James", "Daniels", LocalDate.of(1994, 9, 17));
    try {
      CustomerService.addCustomer(customer);
      System.out.println("Leaving main... Never play f6!");
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
