package com.psybergate.grad2021.jeefnds.enterprise.hwent0;

import java.sql.*;
import java.time.LocalDate;

public class CustomerService {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DB_PASSWORD = "pa+t";

  private static final String DB_USER = "patrick";

  private static final String DB_URL = "jdbc:postgresql://localhost:5432/enterprise";

  public static void addCustomer(Customer customer) throws Exception {
    Connection conn = null;
    try {
      conn = getConnection();
      conn.setAutoCommit(false); //begin transaction
      String sql = "insert into customer (customernum, name, surname, dateofbirth) values(?," +
              " ?, ?, ?)";
      PreparedStatement statement = conn.prepareStatement(sql);
      statement.setLong(1, customer.getCustomerNum());
      statement.setString(2, customer.getName());
      statement.setString(3, customer.getSurname());
      statement.setDate(4, Date.valueOf(customer.getDob()));
      statement.execute();

      //#################### - Audit stuff - #####################
      Audit audit = new Audit("Customer added successfully", LocalDate.now());
      sql = "insert into audit (status, auditdate) values(?, ?)";
      statement = conn.prepareStatement(sql);
      statement.setString(1, audit.getStatus());
      statement.setDate(2, Date.valueOf(audit.getDate()));
      statement.execute();
      conn.commit(); //end transaction
    } catch (Throwable e){
      if (conn != null) {
        conn.rollback();//don't update permanent storage in case of failure
      }
      e.printStackTrace();
    }
  }

  private static Connection getConnection() {
    try {
      Class.forName(JDBC_DRIVER);
      Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
      return conn;
    } catch (SQLException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
