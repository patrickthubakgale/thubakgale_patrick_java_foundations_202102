package com.psybergate.grad2021.jeefnds.enterprise.hwent0;

import java.time.LocalDate;

public class Customer {

  private long customerNum;

  private String name;

  private  String surname;

  private LocalDate dob;

  public Customer(long customerNum, String name, String surname, LocalDate dob) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dob = dob;
  }

  public long getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(long customerNum) {
    this.customerNum = customerNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getDob() {
    return dob;
  }

  public void setDob(LocalDate dob) {
    this.dob = dob;
  }
}
