package com.psybergate.grad2021.jeefnds.enterprise.hwent0;

import java.time.LocalDate;

public class Audit {

  private String status;

  private LocalDate date;

  public Audit(String status, LocalDate date) {
    this.status = status;
    this.date = date;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }
}
