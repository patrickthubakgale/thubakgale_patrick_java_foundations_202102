package com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Audit;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public interface AuditResource {

  public void saveAudit(Audit audit);

}
