package com.psybergate.grad2021.jeefnds.enterprise.hwent4.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource.CustomerResource;

import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.time.LocalDate;

@ApplicationScoped
@Transactional
public class CustomerServiceImpl implements CustomerService {

  @Named("CustomerResource")
  private CustomerResource customerResource;

  @Named("AuditResource")
  private AuditResource auditResource;

  @Inject
  public CustomerServiceImpl(CustomerResource customerResource, AuditResource auditResource) {
    this.customerResource = customerResource;
    this.auditResource = auditResource;
  }

  @Override public void save(Customer customer) {
    try {
      Audit audit = new Audit(LocalDate.now(), "Customer successfully added");
      customerResource.saveCustomer(customer);
      auditResource.saveAudit(audit);
    } catch (Exception e) {
      throw new RuntimeException("Failed to persist the customer");
    }
  }
}

