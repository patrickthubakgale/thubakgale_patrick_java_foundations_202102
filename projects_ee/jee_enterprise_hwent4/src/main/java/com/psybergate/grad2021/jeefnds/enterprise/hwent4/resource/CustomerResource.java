package com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public interface CustomerResource {

  public void saveCustomer(Customer customer);
}
