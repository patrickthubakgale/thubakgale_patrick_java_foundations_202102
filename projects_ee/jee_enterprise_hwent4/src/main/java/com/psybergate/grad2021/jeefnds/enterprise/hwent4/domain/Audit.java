package com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column
  private String status;

  @Column
  private LocalDate auditDate;

  public Audit() {
  }

  public Audit(LocalDate auditDate, String status) {
    this.auditDate = auditDate;
    this.status = status;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public LocalDate getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(LocalDate auditDate) {
    this.auditDate = auditDate;
  }
}
