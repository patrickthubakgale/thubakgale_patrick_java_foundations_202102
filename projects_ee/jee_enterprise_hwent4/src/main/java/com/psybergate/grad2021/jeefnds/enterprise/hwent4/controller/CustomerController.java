package com.psybergate.grad2021.jeefnds.enterprise.hwent4.controller;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.service.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;

@ApplicationScoped
public class CustomerController {

  @Inject
  private CustomerService customerService;

  public CustomerController() {
  }

  public void addCustomer(HttpServletRequest req, HttpServletResponse resp)
          throws ClassNotFoundException, SQLException, ServletException, IOException {
    try {
      RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/views/addCustomer.jsp");
      rd.forward(req, resp);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public void save(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    try {
      long customerNum = Long.parseLong(request.getParameter("customerNum"));
      String name = request.getParameter("name");
      String surname = request.getParameter("surname");
      LocalDate dateOfBirth = LocalDate.parse(request.getParameter("dateOfBirth"));
      Customer customer = new Customer(customerNum, name, surname, dateOfBirth);

      customerService.save(customer);
      RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/success.jsp");
      rd.forward(request, response);
    } catch (Exception e) {
      RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/error.jsp");
      rd.forward(request, response);
    }
  }
}
