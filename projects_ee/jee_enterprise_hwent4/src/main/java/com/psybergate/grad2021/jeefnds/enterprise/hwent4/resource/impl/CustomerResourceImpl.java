package com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource.impl;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource.CustomerResource;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class CustomerResourceImpl implements CustomerResource {

  @PersistenceContext
  private EntityManager entityManager;

  public CustomerResourceImpl() {
  }

  public void saveCustomer(Customer customer) {
    entityManager.persist(customer);
  }
}