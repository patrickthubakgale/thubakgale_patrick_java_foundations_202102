package com.psybergate.grad2021.jeefnds.servlets.arch1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet("/getcurrentdate")
public class GetCurrentDateServlet extends HttpServlet {

  @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    // set content type
    resp.setContentType("text/html");

    // get the printWriter
    PrintWriter out = resp.getWriter();

    // generate HTML content
    out.println("<html><body>");
    out.println("Current date is: " + LocalDate.now());
    out.println("</body></html>");
  }
}
