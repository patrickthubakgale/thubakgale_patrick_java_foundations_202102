package com.psybergate.grad2021.jeefnds.servlets.arch1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/helloworld")
public class HelloWorldServlet extends HttpServlet {

  @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    // set content type
    resp.setContentType("text/html");
    // get the printWriter
    PrintWriter out = resp.getWriter();
    // retrieve name from the url
    String name = req.getParameter("name");

    // generate HTML content
    out.println("<html><body>");
    if (name == null) {
      out.println("Name was not specified");
    } else {
      out.println("Hello " + name);
    }
    out.println("</body></html>");
  }
}
