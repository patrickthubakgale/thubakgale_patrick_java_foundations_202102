package com.psybergate.grad2021.jeefnds.servlets.hmweb1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet("/homework/*")
public class HttpInfoServlet extends HttpServlet {

  @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter pw = resp.getWriter();
    pw.println("<html><body>");
    pw.println("<h2>Hello http headers</h2>");

    String name = req.getParameter("name");
    pw.println("<h1>Hello " + name + "</h1>");
    //header names and values
    for (Enumeration<?> e = req.getHeaderNames(); e.hasMoreElements(); ) {
      String nextHeaderName = (String) e.nextElement();
      String headerValue = req.getHeader(nextHeaderName);
      pw.println("header name: " + nextHeaderName + ", header value: " + headerValue + "<br/>");
    }

    // get currently used http protocol
    pw.println("<h3>Http protocol is: " + req.getProtocol() + "</h3>");

    // get method used for this request
    pw.println("<h3>http method used: " + req.getMethod() + "</h3>");

    // get request uri
    pw.println("<h3>Request uri: " + req.getRequestURI() + "</h3>");

    // get path info
    pw.println("<h3>Path info: " + req.getPathInfo() + "</h3>");

    String languages[] = req.getParameterValues("language");

    if (languages != null) {
      System.out.println("Languages are: ");
      for (String lang : languages) {
        pw.println("Favorite languages are: " + lang + "<br/>");
      }
    }

    pw.println("</body></html>");
  }
}
