package com.psybergate.grad2021.jeefnds.servlets.arch3.framework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet("/*")
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadControllers();
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    String path = request.getPathInfo();
    Controller controller = getController(path.substring(1));
    controller.execute(request, response);
  }

  private Controller getController(String parameter) {
    return CONTROLLERS.get(parameter);
  }

  private void loadControllers() {
    try {
      InputStream stream =
              getServletConfig().getServletContext().getClassLoader().getResourceAsStream(
                      "controllers.properties");
      Properties properties = new Properties();
      properties.load(stream);

      for (Map.Entry entry : properties.entrySet()) {
        Controller controller =
                (Controller) Class.forName(entry.getValue().toString()).newInstance();
        CONTROLLERS.put(entry.getKey().toString(), controller);
      }
    } catch (Exception e) {
      throw new RuntimeException("Error - Experienced difficulty loading the controllers", e);
    }
  }
}
