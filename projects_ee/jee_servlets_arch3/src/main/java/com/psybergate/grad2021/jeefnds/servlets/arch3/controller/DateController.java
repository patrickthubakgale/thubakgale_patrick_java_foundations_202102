package com.psybergate.grad2021.jeefnds.servlets.arch3.controller;

import com.psybergate.grad2021.jeefnds.servlets.arch3.framework.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

public class DateController implements Controller {

  public LocalDate getCurrentDate() {
    return LocalDate.now();
  }

  public LocalDate getTomorrowsDate() {
    LocalDate today = LocalDate.now();
    LocalDate tomorrow = today.plusDays(1);
    return tomorrow;
  }

  @Override public void execute(HttpServletRequest request, HttpServletResponse response)
          throws IOException {
    PrintWriter pw = response.getWriter();
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html><body>");
    if (request.getPathInfo().substring(1).equalsIgnoreCase("getcurrentdate")) {
      out.println("<h2>Today's date is: " + getCurrentDate() + "</h2>");
    } else if (request.getPathInfo().substring(1).equalsIgnoreCase("gettomorrowsdate")) {
      out.println("<h2>Tomorrow's date is: " + getTomorrowsDate() + "</h2>");
    } else {
      out.println("<h2>Date controller does not have that method</h2>");
    }
    out.println("</body></html>");
  }
}
