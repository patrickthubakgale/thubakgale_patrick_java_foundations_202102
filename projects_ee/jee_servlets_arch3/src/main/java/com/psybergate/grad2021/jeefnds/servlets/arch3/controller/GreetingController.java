package com.psybergate.grad2021.jeefnds.servlets.arch3.controller;

import com.psybergate.grad2021.jeefnds.servlets.arch3.framework.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GreetingController implements Controller {

  public String helloWorld(String name) {
    return "Hello " + name;
  }

  public String goodByeWorld(String name) {
    return "Goodbye " + name;
  }

  @Override public void execute(HttpServletRequest request, HttpServletResponse response)
          throws IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String name = request.getParameter("name");
    out.println("<html><body>");
    if (request.getPathInfo().substring(1).equalsIgnoreCase("helloworld")) {
      out.println("<h2>" + helloWorld(name) + "</h2>");
    } else if (request.getPathInfo().substring(1).equalsIgnoreCase("goodbye")) {
      out.println("<h2>" + goodByeWorld(name) + "</h2>");
    } else {
      out.println("<h2>Greeting controller does not have that method</h2>");
    }
    out.println("</body></html>");
  }
}
