package com.psybergate.grad2021.jeefnds.servlets.hwweb3.framework;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Controller {

  public void execute(HttpServletRequest request, HttpServletResponse response);

}
