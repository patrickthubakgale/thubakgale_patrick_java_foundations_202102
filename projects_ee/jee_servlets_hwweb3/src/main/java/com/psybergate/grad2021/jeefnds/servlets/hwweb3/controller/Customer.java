package com.psybergate.grad2021.jeefnds.servlets.hwweb3.controller;

import java.sql.Date;
import java.time.LocalDate;

public class Customer {

  private long CustomerNum;

  private String name;

  private String surname;

  private LocalDate DateOfBirth;

  public Customer(long customerNum, String name, String surname, LocalDate dateOfBirth) {
    CustomerNum = customerNum;
    this.name = name;
    this.surname = surname;
    DateOfBirth = dateOfBirth;
  }

  public long getCustomerNum() {
    return CustomerNum;
  }

  public void setCustomerNum(long customerNum) {
    CustomerNum = customerNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getDateOfBirth() {
    return DateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    DateOfBirth = dateOfBirth;
  }

  @Override public String toString() {
    return "Customer{" +
            "CustomerNum=" + CustomerNum +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", DateOfBirth=" + DateOfBirth +
            '}';
  }
}
