package com.psybergate.grad2021.jeefnds.servlets.hwweb3.database;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3.controller.Customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class CustomerService {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DB_PASSWORD = "pa+t";

  private static final String DB_USER = "patrick";

  private static final String DB_URL = "jdbc:postgresql://localhost:5432/customers";

  public static void addCustomer(Customer customer, HttpServletRequest request,
                                 HttpServletResponse response) throws Exception {
    Connection conn = null;
    PreparedStatement statement = null;

    try {
      conn = getConnection();
      String sql = "insert into customer_tbl (CustomerNum, Name, Surname, DateOfBirth) values(?," +
              " ?, ?, ?)";
      statement = conn.prepareStatement(sql);
      statement.setLong(1, customer.getCustomerNum());
      statement.setString(2, customer.getName());
      statement.setString(3, customer.getSurname());
      statement.setDate(4, Date.valueOf(customer.getDateOfBirth()));
      statement.execute();

      request.getRequestDispatcher("/WEB-INF/views/success.jsp").forward(request, response);
    } finally {
      close(conn, statement, null);
    }
  }

  //In case I wanna display customers to the browser.
  private static List<Customer> getCustomers() throws SQLException {
    List<Customer> customers = new ArrayList<>();

    Connection conn = getConnection();
    Statement stmt = null;
    ResultSet rs = null;

    try {
      String sql = "select * from customer_tbl order by Surname";
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);
      while (rs.next()) {
        long customerNum = rs.getLong("CustomerNum");
        String name = rs.getString("Name");
        String surname = rs.getString("Surname");
        Date date = rs.getDate("DateOfBirth");
        LocalDate dob = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Customer customer = new Customer(customerNum, name, surname, dob);
        customers.add(customer);
      }
    } finally {
      close(conn, stmt, rs);
    }
    return customers;
  }

  private static Connection getConnection() {
    try {
      Class.forName(JDBC_DRIVER);
      Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
      return conn;
    } catch (SQLException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  private static void close(Connection conn, Statement stmt, ResultSet rs) {
    try {
      if (rs != null) {
        rs.close();
      }
      if (stmt != null) {
        stmt.close();
      }
      if (conn != null) {
        conn.close();
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
