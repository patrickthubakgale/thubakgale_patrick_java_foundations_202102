package com.psybergate.grad2021.jeefnds.servlets.hwweb3.controller;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3.database.CustomerService;
import com.psybergate.grad2021.jeefnds.servlets.hwweb3.framework.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class CustomerController implements Controller {

  public void addCustomer(HttpServletRequest request, HttpServletResponse response) {
    try {
      Long customerNum = Long.valueOf(request.getParameter("customerNum"));
      String name = request.getParameter("name");
      String surname = request.getParameter("surname");
      LocalDate date = LocalDate.parse(request.getParameter("dob"));

      Customer customer = new Customer(customerNum, name, surname, date);
      CustomerService.addCustomer(customer, request, response);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override public void execute(HttpServletRequest request, HttpServletResponse response) {
    String pathInfo = request.getPathInfo();
    String method = pathInfo.split("/")[2];

    if(method.equalsIgnoreCase("add")){
      addCustomer(request,response);
    }
  }
}
