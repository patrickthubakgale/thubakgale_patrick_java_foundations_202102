package com.psybergate.grad2021.jeefnds.servlets.hwweb3.framework;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3.controller.CustomerController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet("/dispatch/*")
public class DispatcherServlet extends HttpServlet {

  private Map<String, Controller> controllers = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadControllers();
  }

  @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/views/index.jsp");
    rd.forward(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathInfo = req.getPathInfo();
    String controllerName = pathInfo.split("/")[1];
    controllers.get(controllerName).execute(req, resp);
  }

  private void loadControllers() {
    InputStream stream = DispatcherServlet.class.getClassLoader().getResourceAsStream(
            "controllers.properties");
    Properties properties = new Properties();
    try {
      properties.load(stream);
      Enumeration<Object> e = properties.keys();
      while (e.hasMoreElements()) {
        String name = (String) e.nextElement();
        String value = properties.getProperty(name);
        controllers.put(name, (CustomerController) Class.forName(value).newInstance());
      }
    } catch (InstantiationException | IOException | ClassNotFoundException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }
}
