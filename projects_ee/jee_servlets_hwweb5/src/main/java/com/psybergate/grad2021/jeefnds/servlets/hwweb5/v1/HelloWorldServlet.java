package com.psybergate.grad2021.jeefnds.servlets.hwweb5.v1;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

@WebServlet("/hello/*")
public class HelloWorldServlet extends HttpServlet {

  private String[] names = {"name1", "name2", "name3", "name4", "name5", "name6"};

  private Random rand = new Random();

  @Override public void init() throws ServletException {
    super.init();
  }

  @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    ServletContext sc = getServletContext();
    String name = sc.getInitParameter(names[rand.nextInt(6)]);
    PrintWriter pw = resp.getWriter();
    resp.setContentType("text/html");
    pw.println("<html><body>");
    pw.println("<h2>Hello " + name + "</h2>");
    pw.println("</body></html>");
  }
}
