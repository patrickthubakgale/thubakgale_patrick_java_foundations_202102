package com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Audit;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public interface AuditResource {

  public void saveAudit(Audit audit);

}
