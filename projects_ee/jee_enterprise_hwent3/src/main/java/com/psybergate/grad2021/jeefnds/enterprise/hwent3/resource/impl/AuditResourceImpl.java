package com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource.impl;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource.AuditResource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class AuditResourceImpl implements AuditResource {

  @PersistenceContext
  private EntityManager entityManager;

  public AuditResourceImpl() {
  }

  public void saveAudit(Audit audit) {
    entityManager.persist(audit);
  }

}
