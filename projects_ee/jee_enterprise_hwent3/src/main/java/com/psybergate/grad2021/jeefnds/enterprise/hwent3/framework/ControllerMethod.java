package com.psybergate.grad2021.jeefnds.enterprise.hwent3.framework;

import java.lang.reflect.Method;

public class ControllerMethod {

  private Object controller;

  private Method method;

  public ControllerMethod(Object controller, Method method) {
    this.controller = controller;
    this.method = method;
  }

  public Object getController() {
    return controller;
  }

  public Method getMethod() {
    return method;
  }
}
