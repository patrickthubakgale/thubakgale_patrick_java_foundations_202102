package com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource.impl;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource.CustomerResource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class CustomerResourceImpl implements CustomerResource {

  @PersistenceContext
  private EntityManager entityManager;

  public CustomerResourceImpl() {
  }

  public void saveCustomer(Customer customer) {
    entityManager.persist(customer);
  }
}