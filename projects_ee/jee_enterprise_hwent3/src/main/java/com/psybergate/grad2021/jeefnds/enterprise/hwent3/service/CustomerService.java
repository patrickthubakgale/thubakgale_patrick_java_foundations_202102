package com.psybergate.grad2021.jeefnds.enterprise.hwent3.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Customer;

import javax.ejb.Local;

@Local
public interface CustomerService {

  public void save(Customer customer);
}
