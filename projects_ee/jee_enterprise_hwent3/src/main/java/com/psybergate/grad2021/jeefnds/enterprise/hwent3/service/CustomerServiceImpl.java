package com.psybergate.grad2021.jeefnds.enterprise.hwent3.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource.CustomerResource;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDate;


@Stateless
public class CustomerServiceImpl implements CustomerService {

  @Named("CustomerResource")
  private CustomerResource customerResource;

  @Named("AuditResource")
  private AuditResource auditResource;

  @Inject
  public CustomerServiceImpl(CustomerResource customerResource, AuditResource auditResource) {
    this.customerResource = customerResource;
    this.auditResource = auditResource;
  }

  public void save(Customer customer) {
    try {
      Audit audit = new Audit(LocalDate.now(), "Customer successfully added");
      customerResource.saveCustomer(customer);
      auditResource.saveAudit(audit);
    } catch (Exception e) {
      throw new RuntimeException("Failed to persist the customer");
    }
  }
}

