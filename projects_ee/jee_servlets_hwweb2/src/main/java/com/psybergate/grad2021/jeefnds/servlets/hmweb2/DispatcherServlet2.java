package com.psybergate.grad2021.jeefnds.servlets.hmweb2;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/dispatcher2/*")
public class DispatcherServlet2 extends HttpServlet {

  @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/views/index.jsp");
    rd.forward(req,resp);
  }
}
