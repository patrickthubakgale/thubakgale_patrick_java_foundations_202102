<%@ page import="java.util.Enumeration" %>
<html>

<body>

       <h1>Header names and values:</h1>

       <%
             for (Enumeration<?> e = request.getHeaderNames(); e.hasMoreElements(); ) {
               String nextHeaderName = (String) e.nextElement();
               String headerValue = request.getHeader(nextHeaderName);
       %>
               <%= "<br/>header name: " + nextHeaderName + ", header value: " + headerValue %>
            <%}%>

       <h1>My name is: ${param.name}</h1>

       <%= "<br/>Http protocol is: " + request.getProtocol() %>

       <%= "<br/>http method used: " + request.getMethod() %>

       <%= "<br/>Request uri: " + request.getRequestURI() %>

       <%= "<br/>Path info: " + request.getPathInfo() %>

</body>

</html>