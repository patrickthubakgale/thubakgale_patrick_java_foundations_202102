package com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Audit.class)
public abstract class Audit_ {

	public static volatile SingularAttribute<Audit, Long> id;
	public static volatile SingularAttribute<Audit, LocalDate> auditDate;
	public static volatile SingularAttribute<Audit, String> status;

	public static final String ID = "id";
	public static final String AUDIT_DATE = "auditDate";
	public static final String STATUS = "status";

}

