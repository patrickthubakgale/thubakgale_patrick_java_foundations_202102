package com.psybergate.grad2021.jeefnds.enterprise.hwent2.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.CustomerResource;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.impl.AuditResourceImpl;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.impl.CustomerResourceImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CustomerServiceImpl implements CustomerService{

  public static final EntityManagerFactory emf;

  static {
    emf = Persistence.createEntityManagerFactory("CustomerAudit_JPA");
  }

  private EntityManager entityManager;

  public void save(Customer customer, Audit audit) {
    entityManager = emf.createEntityManager();
    entityManager.getTransaction().begin();
    CustomerResource customerResource = new CustomerResourceImpl();
    AuditResource auditResource = new AuditResourceImpl();
    customerResource.saveCustomer(customer, entityManager);
    auditResource.saveAudit(audit, entityManager);
    entityManager.getTransaction().commit();
    entityManager.close();
    emf.close();
  }
}