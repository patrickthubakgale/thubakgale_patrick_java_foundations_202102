package com.psybergate.grad2021.jeefnds.enterprise.hwent2.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Customer;

public interface CustomerService {

  public void save(Customer customer, Audit audit);

}
