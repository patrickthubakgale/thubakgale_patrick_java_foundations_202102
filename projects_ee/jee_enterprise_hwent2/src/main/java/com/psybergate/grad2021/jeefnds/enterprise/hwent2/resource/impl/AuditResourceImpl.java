package com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.impl;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.AuditResource;

import javax.persistence.EntityManager;

public class AuditResourceImpl implements AuditResource {

  @Override public void saveAudit(Audit audit, EntityManager entityManager) {
    entityManager.persist(audit);
  }
}
