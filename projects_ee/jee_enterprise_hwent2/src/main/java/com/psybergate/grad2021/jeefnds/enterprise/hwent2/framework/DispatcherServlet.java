package com.psybergate.grad2021.jeefnds.enterprise.hwent2.framework;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.controller.CustomerController;

import javax.enterprise.inject.spi.CDI;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.*;

@WebServlet("/dispatch/*")
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, ControllerMethod> CONTROLLER_MAPPING = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadProperties();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    try {
      String pathInfo = req.getPathInfo().substring(1);
      ControllerMethod controllerMethod = CONTROLLER_MAPPING.get(pathInfo);
      if (controllerMethod == null) {
        throw new RuntimeException("No controller for request with path info: " + pathInfo);
      }
      Object controller = controllerMethod.getController();
      Method method = controllerMethod.getMethod();
      method.invoke(controller, req, resp);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    doGet(req, resp);
  }

  private void loadProperties() {
    Map<String, Object> controllers = new HashMap<>();
    try {
      InputStream is = CustomerController.class.getClassLoader().getResourceAsStream("controller.properties");
      Properties props = new Properties();
      props.load(is);
      for (Enumeration<?> e = props.propertyNames(); e.hasMoreElements(); ) {
        String request = (String) e.nextElement();
        String value = props.getProperty(request);
        StringTokenizer tokenizer = new StringTokenizer(value, "#");
        String className = tokenizer.nextToken();
        String methodName = tokenizer.nextToken();
        Class clazz = Class.forName(className);
        Object controller = controllers.get(className);
        if (controller == null) {
          controller = CDI.current().select(clazz).get();
          controllers.put(className, controller);
        }
        Method method = clazz.getMethod(methodName, HttpServletRequest.class,
                HttpServletResponse.class);
        CONTROLLER_MAPPING.put(request, new ControllerMethod(controller, method));
      }
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }
}
