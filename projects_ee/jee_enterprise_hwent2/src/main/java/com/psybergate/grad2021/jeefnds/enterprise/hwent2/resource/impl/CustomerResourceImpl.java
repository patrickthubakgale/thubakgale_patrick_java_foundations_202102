package com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.impl;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource.CustomerResource;

import javax.persistence.EntityManager;

public class CustomerResourceImpl implements CustomerResource {

  @Override public void saveCustomer(Customer customer, EntityManager entityManager) {
    entityManager.persist(customer);
  }
}
