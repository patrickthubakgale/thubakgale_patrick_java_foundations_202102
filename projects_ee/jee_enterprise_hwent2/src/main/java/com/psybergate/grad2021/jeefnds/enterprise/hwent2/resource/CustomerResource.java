package com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Customer;

import javax.persistence.EntityManager;

public interface CustomerResource {

  public void saveCustomer(Customer customer, EntityManager entityManager);
}
