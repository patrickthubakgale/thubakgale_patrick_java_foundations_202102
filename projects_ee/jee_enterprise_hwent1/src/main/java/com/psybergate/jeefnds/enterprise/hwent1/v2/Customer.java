package com.psybergate.jeefnds.enterprise.hwent1.v2;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(unique = true)
  private long customerNum;

  @Column
  private String name;

  @Column
  private String surname;

  @Column
  private LocalDate dateOfBirth;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(long customerNum) {
    this.customerNum = customerNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }
}
