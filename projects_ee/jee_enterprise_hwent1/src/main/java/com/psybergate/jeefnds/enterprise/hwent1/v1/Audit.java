package com.psybergate.jeefnds.enterprise.hwent1.v1;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "Audit")
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "date")
  private LocalDate date;

  @Column(name = "action")
  private String action;

  @Column(name = "warnings")
  private String warnings;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getWarnings() {
    return warnings;
  }

  public void setWarnings(String warnings) {
    this.warnings = warnings;
  }
}
