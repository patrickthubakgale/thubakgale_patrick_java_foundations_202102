package com.psybergate.jeefnds.enterprise.hwent1.v2;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column
  private LocalDate auditDate;

  @Column
  private String status;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public LocalDate getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(LocalDate auditDate) {
    this.auditDate = auditDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
