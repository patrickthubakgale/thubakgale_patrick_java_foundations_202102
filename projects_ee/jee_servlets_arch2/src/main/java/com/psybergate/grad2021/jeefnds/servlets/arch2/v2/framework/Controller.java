package com.psybergate.grad2021.jeefnds.servlets.arch2.v2.framework;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Basic interface for all the controllers handling web requests
 */
public interface Controller {

  /**
   *
   * @param request
   * @param response
   * @throws IOException
   */
  void execute(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
