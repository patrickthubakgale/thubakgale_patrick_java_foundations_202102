package com.psybergate.grad2021.jeefnds.servlets.arch2.v2.framework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet("/dispatcher/*")
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, com.psybergate.grad2021.jeefnds.servlets.arch2.framework.Controller> CONTROLLERS = new HashMap<>();

  @Override public void init() throws ServletException {
    loadControllers();
  }

  @Override protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    com.psybergate.grad2021.jeefnds.servlets.arch2.framework.Controller controller = getController(request.getPathInfo());
    System.out.println(controller.toString());
    System.out.println("entered doGet method with path information" + request.getPathInfo());
    controller.execute(request, response);
  }

  private com.psybergate.grad2021.jeefnds.servlets.arch2.framework.Controller getController(String pathInfo) {
    return CONTROLLERS.get(pathInfo);
  }

  private void loadControllers() {
    try {
      InputStream stream =
              getServletConfig().getServletContext().getClassLoader().getResourceAsStream(
              "controllers.properties");
      Properties properties = new Properties();
      properties.load(stream);

      for (Map.Entry entry : properties.entrySet()) {
        com.psybergate.grad2021.jeefnds.servlets.arch2.framework.Controller controller =
                (com.psybergate.grad2021.jeefnds.servlets.arch2.framework.Controller) Class.forName(entry.getValue().toString()).newInstance();
        CONTROLLERS.put(entry.getKey().toString(), controller);
      }
    } catch (Exception e) {
      throw new RuntimeException("Error - Experienced difficulty loading the controllers", e);
    }
  }
}
