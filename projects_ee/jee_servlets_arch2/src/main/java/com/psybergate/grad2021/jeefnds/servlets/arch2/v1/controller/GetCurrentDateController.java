package com.psybergate.grad2021.jeefnds.servlets.arch2.v1.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

public class GetCurrentDateController implements
                                      com.psybergate.grad2021.jeefnds.servlets.arch2.framework.Controller {

  @Override public void execute(HttpServletRequest request, HttpServletResponse response)
          throws IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html><body>");
    out.println("<h2>Current date is: " + LocalDate.now() + "</h2>");
    out.println("</body></html>");
  }
}
