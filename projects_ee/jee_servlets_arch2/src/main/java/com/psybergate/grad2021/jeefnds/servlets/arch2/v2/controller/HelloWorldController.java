package com.psybergate.grad2021.jeefnds.servlets.arch2.v2.controller;

import com.psybergate.grad2021.jeefnds.servlets.arch2.v2.framework.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldController implements Controller {

  @Override public void execute(HttpServletRequest request, HttpServletResponse response)
          throws IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html><body>");
    out.println("<h2>Hello World</h2>");
    out.println("</body></html>");
  }
}
