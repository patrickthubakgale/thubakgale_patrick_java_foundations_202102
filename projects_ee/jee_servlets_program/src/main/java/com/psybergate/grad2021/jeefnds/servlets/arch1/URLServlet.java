package com.psybergate.grad2021.jeefnds.servlets.arch1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class URLServlet {

  public static void main(String[] args) throws IOException {
    URL servletURL = new URL("http://localhost:8080/jee_servlets_helloworldann/hello");
    URLConnection conn = servletURL.openConnection();
    BufferedReader reader = new BufferedReader(new InputStreamReader(
            conn.getInputStream()));
    String text = null;
    while ((text = reader.readLine()) != null) {
      System.out.println(text);
    }
    reader.close();
  }

}
