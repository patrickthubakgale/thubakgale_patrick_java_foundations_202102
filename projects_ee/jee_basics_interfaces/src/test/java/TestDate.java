//package com.psybergate.grad2021.main.ce1a.vendorimpl1;
//
//import com.psybergate.grad2021.main.ce1a.standard.DateFactoryLoader;
//import com.psybergate.grad2021.main.ce1a.standard.InvalidDateException;
//import com.psybergate.grad2021.interfaces.ce1a.standards.Date;
//import com.psybergate.grad2021.interfaces.ce1a.standards.DateFactory;
//import com.psybergate.grad2021.interfaces.ce1a.standards.InvalidDateException;
//import com.psybergate.grad2021.interfaces.ce1a.vendor1.V1DateFactory;
//import com.psybergate.grad2021.interfaces.ce1a.vendor2.V2DateFactory;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.io.IOException;
//import java.lang.reflect.InvocationTargetException;
//import java.time.LocalDate;
//
///**
// * Not gonna test negative years (BC dates).
// */
//public class TestDate {
//  private DateFactoryLoader dateFactory;
//
//  @Before
//  public void setUp() {
//    dateFactory = new DateFactoryLoader();
//  }
//
//  @Test
//  public void testIsLeapYear() {
//    try {
//      Assert.assertFalse(dateFactory.createDate(1900, 1, 1).isLeapYear());
//    } catch (InvalidDateException e) {
//      e.printStackTrace();
//    } catch (IOException e) {
//      e.printStackTrace();
//    } catch (NoSuchMethodException e) {
//      e.printStackTrace();
//    } catch (ClassNotFoundException e) {
//      e.printStackTrace();
//    } catch (InvocationTargetException e) {
//      e.printStackTrace();
//    } catch (InstantiationException e) {
//      e.printStackTrace();
//    } catch (IllegalAccessException e) {
//      e.printStackTrace();
//    }
//    Assert.assertFalse(dateFactory.createDate(1800, 1, 1).isLeapYear());
//    Assert.assertFalse(dateFactory.createDate(1700, 1, 1).isLeapYear());
//    Assert.assertFalse(dateFactory.createDate(2100, 1, 1).isLeapYear());
//
//    Assert.assertTrue(dateFactory.createDate(1600, 1, 1).isLeapYear());
//    Assert.assertTrue(dateFactory.createDate(2000, 1, 1).isLeapYear());
//    Assert.assertTrue(dateFactory.createDate(2020, 1, 1).isLeapYear());
//  }
//
//  @Test(expected = InvalidDateException.class)
//  public void tryToCreateInvalidDate() {
//    this.dateFactory.createDate(234, 123, 25);
//  }
//
//  @Test(expected = InvalidDateException.class)
//  public void tryToCreateInvalidDateNonLeapYear() {
//    this.dateFactory.createDate(2100, 2, 29);
//
//  }
//
//  @Test
//  public void tryToCreateValidDateLeapYear() {
//    this.dateFactory.createDate(2000, 2, 29);
//  }
//
//  @Test
//  public void addOneDayToDates() {
//    this.addToDateAndCheck(2020, 1, 1, 1);
//
//    this.addToDateAndCheck(2020, 1, 31, 1);
//
//    this.addToDateAndCheck(2020, 12, 31, 1);
//  }
//
//  @Test
//  public void addManyDaysToDate() {
//    this.addToDateAndCheck(2020, 1, 1, 35);
//    this.addToDateAndCheck(2020, 1, 1, 30);
//    this.addToDateAndCheck(2020, 1, 1, 7 * 7);
//    this.addToDateAndCheck(2020, 1, 1, 365);
//    this.addToDateAndCheck(2020, 1, 1, 1654);
//    this.addToDateAndCheck(2020, 1, 1, 365 * 129);
//    this.addToDateAndCheck(2020, 2, 29, 365 * 129);
//    this.addToDateAndCheck(1900, 2, 28, 365 * 129);
//    this.addToDateAndCheck(0, 1, 1, 365 * 2021);
//  }
//
//  private void addToDateAndCheck(int year, int month, int day, int daysToAdd) {
//    Date actualDate = dateFactory.createDate(year, month, day);
//    actualDate = actualDate.addDays(daysToAdd);
//
//    LocalDate expectedDate = LocalDate.of(year, month, day);
//    expectedDate = expectedDate.plusDays(daysToAdd);
//
//    Assert.assertTrue(this.isEqual(expectedDate, actualDate));
//  }
//
//  public boolean isEqual(LocalDate localDate, Date myDate) {
//    return localDate.getYear() == myDate.getYear() &&
//            localDate.getMonthValue() == myDate.getMonth() &&
//            localDate.getDayOfMonth() == myDate.getDay();
//  }
//}
