package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards;

import java.util.List;

/**
 *
 */
public interface CommandRequest {

  public Command getCommand();

  public void setData(String data, Object value);

  public Object getParameter(String data);

}
