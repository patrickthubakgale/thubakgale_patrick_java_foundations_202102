package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards;

/**
 *
 */
public interface CommandEngine {

  CommandResponse processCommands(Command command);
}
