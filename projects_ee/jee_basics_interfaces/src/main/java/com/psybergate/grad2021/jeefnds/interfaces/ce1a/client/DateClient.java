package com.psybergate.grad2021.jeefnds.interfaces.ce1a.client;

import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.Date;
import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.DateFactory;
import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.DateFactoryLoader;
import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.InvalidDateException;

public class DateClient {

  public static void main(String[] args) throws InvalidDateException {
    DateFactory factory = DateFactoryLoader.getDateFactory();
    Date date = factory.createDate(2000, 11, 29);
    System.out.println("Date is: " + date.getYear() + "/" + date.getMonth() + "/" + date.getDay());
  }
}
