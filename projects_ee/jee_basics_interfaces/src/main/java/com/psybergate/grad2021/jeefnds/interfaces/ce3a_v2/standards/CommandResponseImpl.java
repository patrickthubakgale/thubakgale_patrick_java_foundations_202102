package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponse;

public class CommandResponseImpl implements CommandResponse {

  private String response;

  public CommandResponseImpl(String response) {
    this.response = response;
  }

  @Override public String getResponse() {
    return response;
  }
}
