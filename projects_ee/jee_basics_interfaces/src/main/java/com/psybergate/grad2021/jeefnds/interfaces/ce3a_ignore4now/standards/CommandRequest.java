package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards;

import java.util.List;

/**
 *
 */
public interface CommandRequest {

  List<Object> getData();

  void setData(List<Object> data);
}
