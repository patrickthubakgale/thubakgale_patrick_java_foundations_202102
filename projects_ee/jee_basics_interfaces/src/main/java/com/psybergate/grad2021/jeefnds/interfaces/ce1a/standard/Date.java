package com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard;

/**
 * Provides a standard interface for Date. Date is immutable
 */
public interface Date {

  /**
   * @return current date's year
   */
  int getYear();

  /**
   * @return current date's month
   */
  int getMonth();

  /**
   * @return current date's day
   */
  int getDay();

  /**
   * @return true if current date is a leap year. False otherwise.
   */
  boolean isLeapYear();

  /**
   * Adds numDays to current date
   * @return a new Date
   */
  Date addDays(int numDays);
}
