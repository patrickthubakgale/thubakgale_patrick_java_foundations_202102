package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.commands;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponse;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponseImpl;

import java.time.LocalDate;
import java.util.List;

public class GetCurrentDate implements Command {

  @Override public CommandResponse execute(CommandRequest request) {
    LocalDate date = (LocalDate) request.getParameter("date");
    return new CommandResponseImpl(date+"");
  }
}
