package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards;

/**
 * Receives a command request and produces command response
 */
public interface Command {
  /**
   * @return CommandResponse which is simply a response to the request made
   */
  CommandResponse execute(CommandRequest request);
}
