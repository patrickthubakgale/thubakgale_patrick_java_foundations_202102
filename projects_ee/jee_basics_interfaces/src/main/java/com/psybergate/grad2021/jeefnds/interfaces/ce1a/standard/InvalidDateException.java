package com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard;

/**
 * It's an exception thrown for invalid date
 */
public class InvalidDateException extends Exception {

  public InvalidDateException() {
  }

  public InvalidDateException(String message) {
    super(message);
  }
}
