package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.client.commands;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandResponse;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.vendorimpl1.CommandResponseImpl;

import java.time.LocalDate;

public class GetCurrentDate implements Command {

  public static final String CURRENT_DATE = "get_current_date";

  @Override public String commandName() {
    return CURRENT_DATE;
  }

  @Override public CommandResponse execute(CommandRequest request) {
    CommandResponse response = (CommandResponse) new CommandResponseImpl();
    response.setResponseData(getCurrentDate());
    return response;
  }

  private LocalDate getCurrentDate() {
    return LocalDate.now();
  }
}
