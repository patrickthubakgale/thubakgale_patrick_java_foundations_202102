package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.commands;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponse;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponseImpl;

public class CalcFactorial implements Command {

  @Override public CommandResponse execute(CommandRequest request) {
    Integer number = (Integer) request.getParameter("factorial");
    int result = 1;
    for (int i = 1; i <= number; i++) {
      result *= i;
    }
    return new CommandResponseImpl(result+"");
  }
}
