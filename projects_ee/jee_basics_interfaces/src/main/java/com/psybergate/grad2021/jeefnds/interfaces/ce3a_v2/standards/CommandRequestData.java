package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards;

import java.util.HashMap;
import java.util.Map;

public class CommandRequestData implements CommandRequest {

  private Command command;

  private Map<String, Object> parameters = new HashMap<>();

  public CommandRequestData(Command command) {
    this.command = command;
  }

  public void setData(String data, Object value) {
    parameters.put(data, value);
  }

  public Object getParameter(String data) {
    return parameters.get(data);
  }

  @Override public Command getCommand() {
    return command;
  }

}
