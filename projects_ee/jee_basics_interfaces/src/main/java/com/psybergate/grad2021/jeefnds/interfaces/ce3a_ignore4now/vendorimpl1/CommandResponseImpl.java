package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.vendorimpl1;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandResponse;

public class CommandResponseImpl implements CommandResponse {

  private String response;

  public void setResponseData(Object response) {
    response = response;
  }

  @Override public String getResponse() {
    return response;
  }
}
