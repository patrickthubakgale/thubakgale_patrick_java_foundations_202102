package com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Loads properties file and load the DateFactory using reflection
 * Returns DateFactory instance
 */
public class DateFactoryLoader {

  private static DateFactory dateFactory;

  public static DateFactory getDateFactory() {
    if (dateFactory != null) {
      return dateFactory;
    }
    try (InputStream stream =
                 DateFactoryLoader.class.getClassLoader().getResourceAsStream("date.properties")) {
      Properties properties = new Properties();
      properties.load(stream);
      String dateFactoryClass = properties.getProperty("date.factory");
      dateFactory = (DateFactory) Class.forName(dateFactoryClass).newInstance();
      return dateFactory;
    } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
      throw new RuntimeException("Error loading date factory class dynamically", e);
    }
  }
}
