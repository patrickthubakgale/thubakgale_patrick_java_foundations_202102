package com.psybergate.grad2021.jeefnds.interfaces.ce1a.vendorimpl2;

import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.Date;
import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.DateFactory;
import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.InvalidDateException;

/**
 * Implements the DateFactory interface
 * return a new Date according to the standards in the JSR
 */
public class MyDateFactoryImpl2 implements DateFactory {

  /**
   * @return a new Date
   * @throws InvalidDateException if year/month/day are invalid for a date
   */
  @Override public Date createDate(int year, int month, int day) throws InvalidDateException {
    return new MyDateImpl2(year, month, day);
  }
}
