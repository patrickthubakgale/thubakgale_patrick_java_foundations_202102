package com.psybergate.grad2021.jeefnds.interfaces.ce1a.vendorimpl2;

import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.Date;
import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.InvalidDateException;

public class MyDateImpl2 implements Date {

  private int year;

  private int month;

  private int day;

  public MyDateImpl2(int year, int month, int day) throws InvalidDateException {
    if (day <= 0 || day > 31) {
      throw new InvalidDateException("Day invalid");
    }
    if (month <= 0 || month > 12) {
      throw new InvalidDateException("Month invalid");
    }
    if (year < 1900 || year > 2050) {
      throw new InvalidDateException("Year invalid");
    }
    if (month == 2 && day > 28) {
      if (day != 29 || !(year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))) {
        throw new InvalidDateException("Day invalid");
      }
    }
    if (month == 4 || month == 6 || month == 9 || month == 11) {
      if (day > 30) {
        throw new InvalidDateException("Day invalid");
      }
    }

    this.year = year;
    this.month = month;
    this.day = day;
  }

  @Override public int getYear() {
    return year;
  }

  @Override public int getMonth() {
    return month;
  }

  @Override public int getDay() {
    return day;
  }

  @Override public boolean isLeapYear() {
    if (month == 2 && day == 29 && (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))) {
      return true;
    }
    return false;
  }

  @Override public Date addDays(int numDays) {
    return null;
  }
}
