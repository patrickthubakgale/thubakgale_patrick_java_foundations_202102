package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.client.commands;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandResponse;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.vendorimpl1.CommandResponseImpl;

public class CalcFactorial implements Command {

  public static final String FACTORIAL = "calc_factorial";

  @Override public String commandName() {
    return FACTORIAL;
  }

  @Override public CommandResponse execute(CommandRequest request) {
    CommandResponse response = new CommandResponseImpl();
    long factorial = factorial(6);
    response.setResponseData(factorial);
    return response;
  }

  private long factorial(int number) {
    long result = 1;
    for (long i = 1; i <= number; i++) {
      result *= i;
    }
    return result;
  }
}
