package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.vendorimpl1;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandEngine;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponse;

public class CommandEngineImpl implements CommandEngine {

  @Override public CommandResponse processCommands(CommandRequest request) {
    Command command = request.getCommand();
    CommandResponse response = command.execute(request);
    return response;
  }

}
