//package com.psybergate.grad2021.jeefnds.interfaces.ce3a.vendorimpl1;
//
//import com.psybergate.grad2021.main.ce1a.ce3a.client.CalcFactorial;
//import com.psybergate.grad2021.main.ce1a.ce3a.client.GetCurrentDate;
//import com.psybergate.grad2021.main.ce1a.ce3a.client.SumNumbers;
//import com.psybergate.grad2021.main.ce1a.ce3a.standards.Command;
//
//public class CommandFactory {
//
//  public static Command getCommand(String commandName) {
//    if (commandName.equalsIgnoreCase("get_current_date")) {
//      return new GetCurrentDate();
//    } else if (commandName.equalsIgnoreCase("calc_factorial")) {
//      return new CalcFactorial();
//    } else if (commandName.equalsIgnoreCase("sum_numbers")) {
//      return new SumNumbers();
//    } else {
//      return null;
//    }
//  }
//}
