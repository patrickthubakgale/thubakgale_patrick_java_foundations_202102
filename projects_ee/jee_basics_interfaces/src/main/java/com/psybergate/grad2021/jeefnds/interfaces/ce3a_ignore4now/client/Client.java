package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.client;


import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandEngine;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandResponse;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.vendorimpl1.CommandEngineImpl;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.commands.GetCurrentDate;

public class Client {

  public static void main(String[] args) {
    CommandEngine engine = new CommandEngineImpl();
    CommandResponse response = engine.processCommands((Command) new GetCurrentDate());
    System.out.println(response.getResponse());
//    Command command = new CommandFactory().getCommand("sum_numbers");
  }
}
