package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.client;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.commands.CalcFactorial;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.commands.SumNumbers;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.*;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.vendorimpl1.CommandEngineImpl;

import java.util.Arrays;
import java.util.List;

public class Client {

  public static void main(String[] args) {
    CommandRequest request1 = new CommandRequestData(new SumNumbers());
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
    request1.setData("numbers", numbers);
    CommandEngine engine = new CommandEngineImpl();
    CommandResponse response = engine.processCommands(request1);
    System.out.println("response.getResponse() = " + response.getResponse());

    //---------------------------------------------------------

    CommandRequest request2 = new CommandRequestData(new CalcFactorial());
    request2.setData("factorial", 4);
    response = engine.processCommands(request2);
    System.out.println("response.getResponse() = " + response.getResponse());

  }

}
