package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards;

/**
 *
 */
public interface CommandResponse {

  String getResponse();
}
