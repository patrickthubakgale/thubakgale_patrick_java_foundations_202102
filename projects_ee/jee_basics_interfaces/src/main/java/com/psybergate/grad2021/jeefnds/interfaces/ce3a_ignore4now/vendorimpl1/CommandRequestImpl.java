package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.vendorimpl1;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandRequest;

import java.util.List;

public class CommandRequestImpl implements CommandRequest {

  private List<Object> data;

  @Override public List<Object> getData() {
    return data;
  }

  @Override public void setData(List<Object> data) {
    this.data = data;
  }
}
