package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.client.commands;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards.CommandResponse;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.vendorimpl1.CommandResponseImpl;

public class SumNumbers implements Command {

  public static final String SUM = "sum_numbers";

  @Override public String commandName() {
    return SUM;
  }

  @Override public CommandResponse execute(CommandRequest request) {
    CommandResponse response = (CommandResponse) new CommandResponseImpl();
    int sumNumbers = sum(2, 4, 9, 10, 6);//TODO: put CommandRequest instead
    response.setResponseData(sumNumbers);
    return response;
  }

  private int sum(int... numbers) {
    int sumNumbers = 0;
    for (int number : numbers) {
      sumNumbers += number;
    }
    return sumNumbers;
  }
}
