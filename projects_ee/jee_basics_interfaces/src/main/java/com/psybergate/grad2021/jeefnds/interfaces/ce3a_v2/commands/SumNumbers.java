package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.commands;

import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.Command;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponse;
import com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards.CommandResponseImpl;

import java.util.List;

/**
 *
 */
public class SumNumbers implements Command {

  @Override public CommandResponse execute(CommandRequest request) {
    List<Integer> numbers = (List<Integer>) request.getParameter("numbers");
    int result = 0;
    for (Integer number : numbers) {
      result += number;
    }
    return new CommandResponseImpl(result+"");
  }
}
