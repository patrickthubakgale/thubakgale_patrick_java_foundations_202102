package com.psybergate.grad2021.jeefnds.interfaces.ce3a_ignore4now.standards;

/**
 * Receives a command request and produces command response
 */
public interface Command {

  /**
   * @return name of the command to be executed
   */
  String commandName();

  /**
   * @return CommandResponse which is simply a response to the request made
   */
  CommandResponse execute(CommandRequest request);
}
