package com.psybergate.grad2021.jeefnds.interfaces.ce1a.vendorimpl1;

import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.Date;
import com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard.InvalidDateException;

public class MyDateImpl implements Date {

  private int year;

  private int month;

  private int day;

  private static final int[] daysInEachMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  public MyDateImpl(int year, int month, int day) throws InvalidDateException {
    validateDate(year, month, day);
    this.year = year;
    this.month = month;
    this.day = day;
  }

  private void validateDate(int year, int month, int day) throws InvalidDateException {
    //check if year is within this range
    if (year <= 1950 || year > 2050) {
      throw new InvalidDateException("Year is invalid");
    }
    //check if month in 12-month range
    if (month <= 0 || month > 12) {
      throw new InvalidDateException("Month out of range");
    }
    //check if day in range for this month
    if (day <= 0 || day > daysInEachMonth[month - 1] && !(month == 2 && day == 29)) {
      throw new InvalidDateException("Day out of range for specified month");
    }
    //check if it's a leap year
    if (isLeapYear()) {
      throw new InvalidDateException("Day out of range for specified month");
    }
  }

  @Override public int getYear() {
    return year;
  }

  @Override public int getMonth() {
    return month;
  }

  @Override public int getDay() {
    return day;
  }

  @Override public boolean isLeapYear() {
    if (month == 2 && day == 29 && (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))) {
      return true;
    }
    return false;
  }

  @Override public Date addDays(int numDays) {
    return null;
  }

  @Override public String toString() {
    return getYear() + "/" + getMonth() + "/" + getDay();
  }
}
