package com.psybergate.grad2021.jeefnds.interfaces.ce3a_v2.standards;

/**
 *
 */
public interface CommandEngine {

  CommandResponse processCommands(CommandRequest command);
}
