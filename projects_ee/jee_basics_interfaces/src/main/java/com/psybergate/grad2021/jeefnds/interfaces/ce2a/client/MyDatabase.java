package com.psybergate.grad2021.jeefnds.interfaces.ce2a.client;

import com.psybergate.grad2021.jeefnds.interfaces.ce2a.vendorimpl.MyConnection;
import com.psybergate.grad2021.jeefnds.interfaces.ce2a.vendorimpl.MyDriver;
import com.psybergate.grad2021.jeefnds.interfaces.ce2a.vendorimpl.MyResultSet;
import com.psybergate.grad2021.jeefnds.interfaces.ce2a.vendorimpl.MyStatement;

import java.sql.DriverManager;
import java.sql.SQLException;

public class MyDatabase {

  public static void main(String[] args) {
    makeConnection();
  }

  private static final String JDBC_DRIVER = "com.psybergate.mydatabase.jdbcdriver.MyDriver";

  private static final String DB_URL = "jdbc:mydatabase://localhost:1234/customers";

  private static final String DB_USER = "patrick";

  private static final String DB_PASSWORD = "admin";

  static {
    try {
      DriverManager.registerDriver(new MyDriver()); //register the driver
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  public static void makeConnection() {
    try {
      Class.forName(JDBC_DRIVER); //load the driver
      MyConnection conn = (MyConnection) DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
      MyStatement statement = (MyStatement) conn.createStatement();
      MyResultSet resultSet = (MyResultSet) statement.executeQuery("select from * from mytable");
      /*
         do stuff here...
      */
      conn.close();
    } catch (Throwable e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

}