package com.psybergate.grad2021.jeefnds.interfaces.ce1a.standard;

/**
 * It is a factory that produces a date given a year, month and day
 */
public interface DateFactory {

  /**
   * @return a new Date object
   * @throws InvalidDateException if year/month/day are not valid for a date
   */
  public Date createDate(int year, int month, int day) throws InvalidDateException;
}
