package com.psybergate.javafnds;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class PrintWriter {

    public static void main(String[] args) {
	    try {
	        FileWriter myfile = new FileWriter("text01.txt");
	        myfile.write("The cat sat on the hat.");
	        myfile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	    //Do all the tasks in here
        String directory = System.getProperty("user.dir");
        String fileName = "text01.txt";
        Path path = Paths.get(directory, fileName);

        try {
            byte[] data = Files.readAllBytes(path);
            for(byte num: data) {
                String str = Integer.toHexString(num);
                System.out.printf("decimal: %d, hexadecimal: %s%n", num, str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}