package com.psybergate.javafnds;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ByteReader {

    public static void main(String[] args) {
//        String directory = System.getProperty("user.home");
//        String currentDir = System.getProperty("user.dir");
//        System.out.println(directory);
//        System.out.println(currentDir);
        String directory = System.getProperty("user.dir");
        String fileName = "file.class";
        String absolutePath = directory + File.separator + fileName;
        Path path = Paths.get(directory, fileName);

        try {
            byte[] allBytes = Files.readAllBytes(Paths.get(absolutePath));
            for(int i = 0; i < allBytes.length; i++) {
                String str = Integer.toHexString(allBytes[i]);
                if(str.length() > 2) {
                    System.out.printf("%s%s", str.charAt(str.length()-2),str.charAt(str.length()-1));
                    System.out.println();
                }
                else if(str.length() == 1) {
                    System.out.printf("%d%s",0,str);
                    System.out.println();
                }
                else {
                    System.out.println(Integer.toHexString(allBytes[i]));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
