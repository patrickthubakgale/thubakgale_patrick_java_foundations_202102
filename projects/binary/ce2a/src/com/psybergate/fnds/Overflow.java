package com.psybergate.fnds;

public class Overflow {
    public static void main(String[] args) {
        byte a = 101;
        byte b = 57;
        byte c = (byte)(a+b);
        System.out.println(c);
    }
}
/*
byte has b bits which are able to represent 256 numbers. Numbers are from -128 to 127.
The sum of the above operation gives 158 which is more than 127 hence the overflow.
*/
