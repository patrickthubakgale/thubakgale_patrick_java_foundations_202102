package com.psybergate.grad2021.core.enums.hw1a_1;

public class Utils {

  public static void main(String[] args) {
    Customer customer1 = new Customer("123", "James", Car.AUDI);
    Customer customer2 = new Customer("100", "Jeffery", Car.TOYOTA);
    Customer customer3 = new Customer("524", "Jerremy", Car.BMW);

    System.out.println("customer name is " + customer1.getName() + ", customer car is " + customer1.getCar());
  }
}
