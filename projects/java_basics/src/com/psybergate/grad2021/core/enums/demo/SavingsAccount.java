package com.psybergate.grad2021.core.enums.demo;

public class SavingsAccount extends Account {

  private double rate;

  public SavingsAccount(String accountNum, String name, AccountType accountType, double balance,
                        double rate) {
    super(accountNum, name, accountType, balance);
    this.rate = rate;
  }

  @Override public void withdraw(double amount) {
    if (this.balance < amount) {
      throw new RuntimeException("Insufficient funds");
    }
    this.balance -= (amount - amount * (rate/10));
  }
}
