package com.psybergate.grad2021.core.enums.demo;

public class Utils {

  public static void main(String[] args) {
    Account account1 = new CheckAccount("123", "Joe", AccountType.CURRENT, 1000);
    Account account2 = new SavingsAccount("234", "Dan", AccountType.SAVINGS, 2000, 0.2);

    System.out.println("account1.getBalance() = " + account1.getBalance());
    System.out.println("account2.getBalance() = " + account2.getBalance());
  }
}
