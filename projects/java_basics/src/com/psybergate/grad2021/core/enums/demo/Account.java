package com.psybergate.grad2021.core.enums.demo;

public abstract class Account {

  private String accountNum;

  private AccountType accountType;

  private String name;

  protected double balance;

  public Account(String accountNum, String name, AccountType accountType, double balance) {
    this.accountNum = accountNum;
    this.accountType = accountType;
    this.name = name;
    this.balance = balance;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  public double getBalance() {
    return balance;
  }

  public String getName() {
    return name;
  }

  public String getAccountNum() {
    return accountNum;
  }

  public AccountType getAccountType() {
    return accountType;
  }

  public void deposit(double amount) {
    if (amount > 1_000_000) {
      throw new RuntimeException("That is a of money to deposit");
    }
    this.balance += amount;
  }


  public abstract void withdraw(double amount);
}
