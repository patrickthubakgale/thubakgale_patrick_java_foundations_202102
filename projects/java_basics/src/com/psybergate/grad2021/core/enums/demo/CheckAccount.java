package com.psybergate.grad2021.core.enums.demo;

public class CheckAccount extends Account {

  public CheckAccount(String accountNum, String name, AccountType accountType, double balance) {
    super(accountNum, name, accountType, balance);
  }

  @Override public void withdraw(double amount) {
    if (this.balance < amount) {
      throw new RuntimeException("Insufficient funds");
    }
    this.balance -= amount;
  }
}
