package com.psybergate.grad2021.core.enums.exploring;

public enum Directions {
  //fields
  EAST(0), WEST(180), NORTH(90), SOUTH(270);

  private final double angle;

  private Directions(final double angle) {
    this.angle = angle;
  }

  public double getAngle() {
    return angle;
  }

  public String print() {
    String message = "You are moving in " + this.name() + " direction";
    System.out.println(message);
    return message;
  }
}
