package com.psybergate.grad2021.core.enums.demo;

public enum AccountType {
  CURRENT, SAVINGS;

  //enums with state

  private double bonus;

  private AccountType(double bonus) {
    this.bonus = bonus;
  }

  //Object reference of type AccountType are created using the default constructor of AccountType
  AccountType() {
  }
}
