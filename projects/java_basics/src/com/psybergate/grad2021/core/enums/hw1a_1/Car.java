package com.psybergate.grad2021.core.enums.hw1a_1;

public enum Car {
  BMW, MERCEDES, AUDI, TOYOTA("1234");

  private String vinNum;

  private Car(String vinNumber) {
    this.vinNum = vinNumber;
  }

  private Car() {

  }
}
