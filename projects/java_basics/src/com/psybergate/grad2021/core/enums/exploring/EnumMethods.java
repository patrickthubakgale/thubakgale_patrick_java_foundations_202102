package com.psybergate.grad2021.core.enums.exploring;

public class EnumMethods {

  public static void main(String[] args) {
    Directions direction = Directions.NORTH;

    System.out.println("direction = " + direction);

    //ordinal() returns the order of the enum instance
    System.out.println("direction.ordinal() = " + direction.ordinal());
    System.out.println("Directions.EAST.ordinal() = " + Directions.EAST.ordinal());
    System.out.println();

    //enum values() and valueOf()
    Directions[] directions = Directions.values();

    for (Directions direction1 : directions) {
      System.out.println(direction1);
    }

    Directions north = Directions.valueOf("NORTH");
    System.out.println("Value of north is " + north);

    //###########################################

    //let's get the direction
    Directions west = Directions.WEST;
    System.out.println("west = " + west);
    System.out.println("west.getAngle() = " + west.getAngle());

    System.out.println("west.print() = " + west.print());

    System.out.println(west.name()+" is in "+ west.getClass()+" enum class");
  }
}
