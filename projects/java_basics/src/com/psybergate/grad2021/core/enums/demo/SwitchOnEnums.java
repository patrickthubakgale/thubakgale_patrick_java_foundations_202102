package com.psybergate.grad2021.core.enums.demo;

import java.time.Month;
import java.util.Scanner;

public class SwitchOnEnums {

  public enum Months {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
  }

  public static void main(String[] args) {
    System.out.println("Enter your month of birth");
    Scanner scanner = new Scanner(System.in);

    //assuming user entered this month
    Months month = Months.APRIL;

    switch (month) {
      case JANUARY:
        System.out.println("It's january!");
        break;
      case FEBRUARY:
        System.out.println("It's february!");
        break;
      case MARCH:
        System.out.println("It's march!");
        break;
      case APRIL:
        System.out.println("It's april!");
        break;
      case MAY:
        System.out.println("It's may!");
        break;
      case JUNE:
        System.out.println("It's june!");
        break;
      case JULY:
        System.out.println("It's july!");
        break;
      default:
        System.out.println("Unknown month");
    }
  }
}
