package com.psybergate.grad2021.core.enums.hw1a_1;

public class Customer {

  private String customerNum;

  private String name;

  private Car car;

  public Customer(String customerNum, String name, Car car) {
    this.customerNum = customerNum;
    this.name = name;
    this.car = car;
  }

  public String getName() {
    return name;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public Car getCar() {
    return car;
  }
}
