package com.psybergate.grad2021.core.databases.hw3a;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PopulateDatabase {

  public static final Random RAND = new Random();

  public static void main(String[] args) {
    writeToFile();
  }

  public static void writeToFile() {
    try {
      FileWriter myWriter = new FileWriter("src\\com\\psybergate\\grad2021\\core\\databases\\hw3a" +
              "\\data.sql");
      myWriter.write(sqlQuery());
      myWriter.close();
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  public static String sqlQuery() {
    StringBuilder sql = new StringBuilder();
    sql.append(createTables());
    sql.append(insertCustomers());
    sql.append(insertAccountType());
    sql.append(insertAccounts());
    sql.append(insertTransactionType());
    sql.append(insertTransaction());
    sql.append(insertReviewColumn());
    sql.append(deleteAccount());
    return sql.toString();
  }

  public static String createTables() {
    String sql = createCustomer() + createAccountType() + createAccount() + createTransactionType() + createTransaction();
    return sql;
  }

  public static String createCustomer() {
    String sql = "CREATE TABLE IF NOT EXISTS customer (CustomerNum SERIAL PRIMARY KEY, Name varchar(20), " +
            "Surname varchar(20), DateOfBirth date, Address varchar(20));";
    return sql; 
  }

  public static String createAccount() {
    String sql = "CREATE TABLE IF NOT EXISTS account (AccountNum SERIAL PRIMARY KEY, OpenDate date, " +
            "Balance float, CustomerNum INT, AccountType_ID INT, FOREIGN KEY(CustomerNum) REFERENCES " +
            "customer(CustomerNum) ON DELETE CASCADE, CONSTRAINT fk_acc FOREIGN KEY(AccountType_ID) REFERENCES " +
            "accounttype(AccountType_ID) ON DELETE CASCADE);";
    return sql;
  }

  public static String createAccountType() {
    String sql = "CREATE TABLE IF NOT EXISTS accounttype (AccountType_ID SERIAL PRIMARY KEY, AccountType VARCHAR(20));";
    return sql;
  }

  public static String createTransaction() {
    String sql = "CREATE TABLE IF NOT EXISTS transaction (TransactionNumber SERIAL PRIMARY KEY, TransactionDate date, " +
            "Trans_ID INT, AccountNum INT, CONSTRAINT fk_trans FOREIGN KEY(Trans_ID) REFERENCES " +
            "transactiontype(Trans_ID) ON DELETE CASCADE, " +
            "CONSTRAINT fk_accnum FOREIGN KEY(AccountNum) REFERENCES account(AccountNum) ON DELETE CASCADE);";
    return sql;
  }

  public static String createTransactionType() {
    String sql = "CREATE TABLE IF NOT EXISTS transactiontype (Trans_ID SERIAL PRIMARY KEY, TransactionType VARCHAR(20));";
    return sql;
  }

  public static String insertCustomers() {
    List<String> addressList = Arrays.asList("Bloemfontein", "Midrand", "Johannesburg", "Durban", "Pretoria", "Parktown", "Soweto", "Orange Farm", "Witbank");
    int numOfCustomers = 20;
    String sql = "INSERT INTO customer VALUES";

    for (int i = 1; i <= numOfCustomers; i++) {
      sql += " (DEFAULT, '" + customerNames().get(i-1) + "', '" + cusomerSurnames().get(i-1) +
              "', '" + generateRandomInt(2003, 1960) + "-"
              + generateRandomInt(12, 1) + "-" + generateRandomInt(27, 1) + "', '" + addressList.get(generateRandomInt(8,0)) + "')";
      if (i != numOfCustomers) {
        sql += ",";
      }
    }
    return sql + ";";
  }

  public static String insertAccounts() {
    int numOfAccounts = 150;
    String sql = "INSERT INTO account VALUES";

    for (int i = 1; i <= numOfAccounts; i++) {
      sql += " (DEFAULT, '" + generateRandomInt(2021, 1999) + "-" + generateRandomInt(12, 1)
              + "-" + generateRandomInt(27, 1) + "', " + generateRandomInt(50000, -10000) + ", "
              + generateRandomInt(20, 1) + ", " + generateRandomInt(3, 1) + ")";
      if (i != numOfAccounts) {
        sql += ",";
      }
    }
    return sql + ";";
  }

  public static String insertAccountType() {
    String sql = "INSERT INTO accounttype VALUES (DEFAULT, 'creditcard'), (DEFAULT, 'current'), " +
            "(DEFAULT, 'savings');";
    return sql;
  }

  public static String insertTransaction() {
    int numberOfTransactions = 500;
    String sql = "INSERT INTO transaction VALUES";

    for (int i = 1; i <= numberOfTransactions; i++) {
      sql += " (DEFAULT, '" + generateRandomInt(2021, 2005) + "-" + generateRandomInt(12, 1)
              + "-" + generateRandomInt(27, 1) + "', " + generateRandomInt(2, 1) + ", "
              + generateRandomInt(80, 1) + ")";
      if (i != numberOfTransactions) {
        sql += ",";
      }
    }
    return sql + ";";
  }

  public static String insertTransactionType() {
    String sql = "INSERT INTO transactiontype VALUES (DEFAULT, 'deposit'), (DEFAULT, 'withdraw');";
    return sql;
  }

  public static String insertReviewColumn() {
    String sql = "ALTER TABLE account ADD COLUMN needsToBeReviewed BOOLEAN NULL; UPDATE account SET " +
            "needsToBeReviewed = FALSE; ALTER TABLE account ALTER COLUMN needsToBeReviewed SET " +
            "NOT NULL; UPDATE account SET needsToBeReviewed = TRUE WHERE openDate < (CURRENT_DATE" +
            " - INTERVAL '90 days');";
    return sql;
  }

  public static String deleteAccount() {
    String sql = "DELETE FROM account WHERE balance = (SELECT MIN(balance) FROM account);";
    return sql;
  }

  public static String view() {
    String sql = "CREATE OR REPLACE VIEW cur_accounts AS SELECT * FROM account WHERE " +
            "AccountType_ID = 2;";
    return sql;
  }

  private static List<String> customerNames() {
    List<String> names = Arrays.asList("jeremy", "Chris", "Joel", "Jacob", "James", "Daniel",
            "Magnus", "Hikaru", "Aronian", "Ricardo", "Moses", "Joel", "Nathaniel", "Bob", "Lucas",
            "Babar", "Precious", "Palesa", "Tumi", "Nomsa");
    return names;
  }

  private static List<String> cusomerSurnames() {
    List<String> surnames = Arrays.asList("Walters", "Daniels", "Mattheus", "Patterson", "Koke",
            "Hazrad", "Jacobs", "de Villiers", "de Rossi", "Morgan", "Reddington", "Cooper",
            "Pope", "Burger", "Armstrong", "James", "Josephs", "Mccarthy", "Adams", "Troskar");
    return surnames;
  }


  public static int generateRandomInt(int max, int min) {
    return RAND.nextInt((max-min) + 1)+min;
  }
}
