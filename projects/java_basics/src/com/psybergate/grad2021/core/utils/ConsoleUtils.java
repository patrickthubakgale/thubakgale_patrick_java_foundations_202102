package com.psybergate.grad2021.core.utils;

import java.util.Scanner;

public class ConsoleUtils {

  public static final String readAnything() {
    Scanner sc1 = new Scanner(System.in);
    System.out.println("Please enter anything: ");
    return sc1.nextLine();
  }

}
