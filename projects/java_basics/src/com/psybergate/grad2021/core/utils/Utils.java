package com.psybergate.grad2021.core.utils;

import java.time.LocalDate;
import java.util.Calendar;

/**
 * @since 02 Apr 2020
 */
public class Utils {

  public static final String VERSION = "APP_V002";

  public static java.sql.Date getTodaysDateSql() {
    return new java.sql.Date(Calendar.getInstance().getTimeInMillis());
  }

  public static int random(int maxNum) {
    return (int) (Math.random() * maxNum);
  }

  public static int random(int minNum, int maxNum) {
    return ((int) (Math.random() * (maxNum - minNum))) + minNum;
  }

  public static int random() {
    return random(1_000_000_000);
  }

  public static long randomLong(long maxNum) {
    return (long) (Math.random() * maxNum);
  }

  public static long randomLong() {
    return randomLong(1_000_000_000_000L);
  }

  public static LocalDate randomDate(int minYear, int maxYear) {
    return LocalDate.ofYearDay(random(minYear, maxYear), random(1, 365));
  }

  public static LocalDate randomDate(int year) {
    return randomDate(year, year);
  }

  public static LocalDate randomDate() {
    return randomDate(2001, 2017);
  }

  public static void print(String message) {
    System.out.println(VERSION + ": " + message);
  }

  public static void printExecutingQuery() {
    print("Executing query...");
  }

  public static void print(Object object) {
    print(object.toString());
  }

  public static void print(long num) {
    print(num + "");
  }

  public static void print(String prefix, String message) {
    System.out.println(VERSION + ": " + prefix + " - " + message);
  }

  public static void delayMillis(int millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException ex) {
      ex.printStackTrace();
      throw new RuntimeException("Error - Exception not handled");
    }
  }

  public static void delaySeconds(int secs) {
    delayMillis(1000 * secs);
  }

}
