package com.psybergate.grad2021.core.streamsandlambdas.learning;

public class Employee {

  private String firstName;
  private String lastName;
  private double salary;
  private String department;

  public Employee(String firstName, String lastName, double salary, String department) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.salary = salary;
    this.department = department;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getName() {
    return getLastName() + " " + getFirstName();
  }

  public double getSalary() {
    return salary;
  }

  public String getDepartment() {
    return department;
  }

  @Override
  public String toString() {
    return firstName + " " + lastName + " " + salary + " " + department;
  }
}
