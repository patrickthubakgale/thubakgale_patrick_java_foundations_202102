package com.psybergate.grad2021.core.streamsandlambdas.learning;

import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public class IntStreamUsingLogicalAnd {

  public static void main(String[] args) {
    int[] numbers = {2, 5, 1, 10, 7, 6, 4, 8, 2, 9, 3};
    IntPredicate even = value -> value % 2 == 0;
    IntPredicate greaterThanFive = value -> value > 5;

    IntStream.of(numbers).filter(even)
            .filter(greaterThanFive)
            .forEach(System.out::println);

    IntStream.of(numbers).filter(value -> value % 2 == 1)
            .map(value -> value * 10)
            .sorted()
            .forEach(System.out::println);
  }
}
