package com.psybergate.grad2021.core.streamsandlambdas.learning;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class ProcessingEmployees {

  public static void main(String[] args) {
    Employee[] employees = {
            new Employee("Jason", "Red", 5000, "IT"),
            new Employee("Ashley", "Green", 7600, "IT"),
            new Employee("Matthew", "Indigo", 3587.5, "Sales"),
            new Employee("James", "Indigo", 4700.77, "Marketing"),
            new Employee("Luke", "Indigo", 6200, "IT"),
            new Employee("Jason", "Blue", 3200, "Sales"),
            new Employee("Wendy", "Brown", 4236.4, "Marketing")};

    //get list view of employees
    List<Employee> list = Arrays.asList(employees);
    //display
    System.out.println("Complete Employee list:");
    list.stream().forEach(System.out::println);

    Predicate<Employee> fourToSixThousand = e -> (e.getSalary() >= 4000 && e.getSalary() <= 6000);
    System.out.println("\nEmployees earning R4000-R6000 per month sorted by salary: \n");
    list.stream()
            .filter(fourToSixThousand)
            .sorted(Comparator.comparing(Employee::getSalary))
            .forEach(System.out::println);


    System.out.println("First employee who earns R4000-R6000: " +
            list.stream()
                    .filter(fourToSixThousand)
                    .findFirst()
                    .get());

    Function<Employee, String> byFirstName = Employee::getFirstName;
    Function<Employee, String> byLastName = Employee::getLastName;
//    Function<Employee, String> demo = new Function<Employee, String>() {
//      @Override
//      public String apply(Employee employee) {
//        return employee.getLastName();
//      }
//    };

    //Comparator for comparing Employees by first name then last name
    Comparator<Employee> lastThenFirst = Comparator.comparing(byLastName).thenComparing(byFirstName);

    //sort employees by last name, then first name
    System.out.println("\nEmployees in ascending order by last name then first: \n");
    list.stream()
            .sorted(lastThenFirst)
            .forEach(System.out::println);

    //display unique employee last names sorted
    System.out.println("\nUnique employee last names:\n");
    list.stream()
            .map(Employee::getLastName)
            .distinct()
            .sorted()
            .forEach(System.out::println);

    //display only first and last names
    System.out.println("\nEmployee names in order by last name then first name:\n");
    list.stream()
            .sorted(lastThenFirst)
            .map(Employee::getName)
            .forEach(System.out::println);
  }
}

