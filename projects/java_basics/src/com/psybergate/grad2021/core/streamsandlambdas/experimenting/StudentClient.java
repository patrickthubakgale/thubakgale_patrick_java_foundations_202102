package com.psybergate.grad2021.core.streamsandlambdas.experimenting;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentClient {

  public static void main(String[] args) {
   //I did not want to immediately put student objects in a collection! I don't know why though
    Student[] studentArray = {
            new Student("Thabang", "Thoko", 21, "Accounting"),
            new Student("Chris", "Daniels", 23, "Computer Science"),
            new Student("James", "Brown", 29, "Electrical"),
            new Student("Tokyo", "Daniels", 27, "Accounting"),
            new Student("Matthew", "Jacobs", 24, "Mathematics"),
            new Student("Kinshasa", "Daniels", 26, "Accounting"),
            new Student("Sam", "Requillion", 28, "Mathematics"),
            new Student("Lorah", "Sturtle", 32, "Electrical")
    };

    List<Student> students = Arrays.asList(studentArray);
    System.out.println("Wits student list:");
   /* students.stream().forEach(System.out::println);

    //I wanted to clearly register this in my mind that the filter takes in Predicate argument
    Predicate<Student> testRange = e -> (e.getAge() >= 24 && e.getAge() <= 28);
    System.out.println("\nStudents aged between 24 and 28 years old sorted by age: \n");
    students.stream()
            .filter(testRange)
            .sorted(Comparator.comparing(Student::getAge)) //perhaps method refs are much
            // readable than lambdas! Equivalent: e -> e.getAge()?
            .forEach(System.out::println);

    //Finding first student who is between 24 and 28
    students.stream()
            .filter(testRange)
            .findFirst() // returns Optional
            .get();

    //We can sort Students by their last names and then by their first names using Comparator
    Comparator<Student> lastThenFirst =
            Comparator.comparing(Student::getLastName).thenComparing(Student::getFirstName);
    System.out.println("\nStudents sorted alphabetically by last name, and then first name:\n");
    students.stream()
            .sorted(lastThenFirst) //can call reversed() function on the Comparator to do the
            // descending order. Although, not interested now
            .forEach(System.out::println);

    //Exploring distinct() method, not really comfortable with it for now
    System.out.println("\nUnique last names:\n");
    students.stream()
            .map(Student::getLastName)
            .distinct()
            .sorted()
            .forEach(System.out::println);

    //Really went too far, this probably unnecessary!
    System.out.println("\nName and surname of students in sorted order");
    students.stream()
            .sorted(lastThenFirst)
            .map(Student::getName)
            .forEach(System.out::println);
*/
    //I feel like I am doing something wrong here. Do I need a Map though?
    Map<String, List<Student>> groupBySchool = students.stream()
            .collect(Collectors.groupingBy(Student::getSchool));
//    System.out.println(groupBySchool);
    groupBySchool.forEach((school, studentsInThisSchool) -> {
      System.out.println(school);
      studentsInThisSchool.forEach(student -> System.out.println(" " + student.getName()));
    });

  }
}
