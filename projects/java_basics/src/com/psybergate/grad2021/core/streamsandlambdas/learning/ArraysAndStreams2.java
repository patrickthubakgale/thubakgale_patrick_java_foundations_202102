package com.psybergate.grad2021.core.streamsandlambdas.learning;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ArraysAndStreams2 {

  public static void main(String[] args) {
    String[] strings = {"Red", "orange", "Yellow", "green", "Blue", "indigo", "Violet"};

    //display original strings
    System.out.println("Original strings: " + Arrays.asList(strings));

    //Strings in uppercase
    System.out.println("Strings in uppercase: " +
            Arrays.stream(strings)
                    .map(String::toUpperCase)
                    .collect(Collectors.toList()));

    //strings less than "n" (case sensitive) sorted ascending
    System.out.println("Strings greater than n sorted ascending: " +
            Arrays.stream(strings)
                    .filter(s -> s.compareToIgnoreCase("n") > 0)
                    .sorted(String.CASE_INSENSITIVE_ORDER)
                    .collect(Collectors.toList()));
    System.out.println("Strings greater than n sorted descending: " +
            Arrays.stream(strings)
                    .filter(s -> s.compareToIgnoreCase("n") > 0)
                    .sorted(String.CASE_INSENSITIVE_ORDER.reversed())
                    .collect(Collectors.toList()));

  }
}
