package com.psybergate.grad2021.core.streamsandlambdas.learning;

import java.util.stream.IntStream;

public class IntStreamOperations {

  public static void main(String[] args) {
    int[] values = {3, 10, 6, 1, 4, 8, 2, 5, 9, 7};

    //display original values
    System.out.print("Original values: ");
    //forEach takes in as its argument an object that implements the IntConsumer functional
    // interface. This is an int-specific version of the generic Consumer functional interface.
    //implementation: new IntConsumer() {
    //   void public accept(int value) {
    //      System.out.println(value + " ");
    //   }
    // }
    //or: value -> System.out.println(value + " ");
    IntStream.of(values).forEach(value -> System.out.print(value + " "));
    System.out.println();

    //count, min, max, sum and average of values
    System.out.println("Count: " + IntStream.of(values).count());
    System.out.println("Min: " + IntStream.of(values).min().getAsInt());
    System.out.println("Max: " + IntStream.of(values).max().getAsInt());
    System.out.println("Sum: " + IntStream.of(values).sum());
    System.out.println("Average: " + IntStream.of(values).average().getAsDouble());

    //sum of values with reduce method
    System.out.println("\nSum via reduce method: " + IntStream.of(values).reduce(0,
            (x, y) -> x + y));

    //sum of squares of values with reduce method
    System.out.println("\nSum of squares via reduce method: " + IntStream.of(values).reduce(0,
            (x, y) -> x + y * y));

    //product of values with reduce method
    System.out.println("Product via reduce method: " + IntStream.of(values).reduce(1,
            (x, y) -> x * y));

    //even values displayed in sorted order
    System.out.print("\nEven values displayed in sorted order: ");
    IntStream.of(values)
            .filter(value -> value % 2 == 0)
            .sorted()
            .forEach(value -> System.out.print(value + " "));

    System.out.print("\nOdd values multiplied by 10 displayed in sorted order: ");
    IntStream.of(values)
            .filter(value -> value % 2 != 0)
            .map(value -> value * 10)
            .sorted()
            .forEach(value -> System.out.print(value + " "));
    System.out.println();

    //sum of range of integers from 1 to 10, inclusive
    System.out.println("Sum of integers from 1 to 9: " + IntStream.range(1, 10).sum());

    //method summaryStatistics performs the count, min, max, sum and average operation in one
    // pass of an IntStream's elements
    System.out.println(IntStream.of(values).summaryStatistics());
  }
}
