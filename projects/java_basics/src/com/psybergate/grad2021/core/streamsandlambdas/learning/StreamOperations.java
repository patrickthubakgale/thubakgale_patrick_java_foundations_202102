package com.psybergate.grad2021.core.streamsandlambdas.learning;

import java.util.stream.Stream;

public class StreamOperations {

  public static void main(String[] args) {
    Stream<String> fruitStream = Stream.of("apple", "banana", "pear", "kiwi", "orange");

    fruitStream.filter(s -> s.contains("a"))
            .map(String::toUpperCase)
            .sorted()
            .forEach(System.out::println);
  }
}
