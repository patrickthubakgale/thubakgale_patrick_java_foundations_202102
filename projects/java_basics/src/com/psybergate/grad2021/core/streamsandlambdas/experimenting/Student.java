package com.psybergate.grad2021.core.streamsandlambdas.experimenting;

public class Student {

  private String firstName;

  private String lastName;

  private int age;

  private String school;

  public Student(String firstName, String lastName, int age, String school) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.school = school;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getAge() {
    return age;
  }

  public String getSchool() {
    return school;
  }

  public String getName() {
    return getFirstName() + " " + getLastName();
  }

  @Override public String toString() {
    return "Name: " + firstName + ", Surname: "
            + lastName + ", Age: " + age
            + ", School: " + school;
  }
}
