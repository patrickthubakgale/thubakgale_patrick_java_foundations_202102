package com.psybergate.grad2021.core.collections.hw2a;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private int customerNum;

  private String name;

  private final String CUSTOMER_TYPE;

  private final List<Order> orders = new ArrayList<>();

  public Customer(int customerNum, String name, String CUSTOMER_TYPE) {
    this.customerNum = customerNum;
    this.name = name;
    this.CUSTOMER_TYPE = CUSTOMER_TYPE;
  }

  public void makeOrder(Order order) {
    orders.add(order);
  }

  public double getCustomerOrderTotals() {
    double total = 0;
    for (Order order : orders) {
      total += order.totalAfterDiscount();
    }
    return total;
  }

  public String getCUSTOMER_TYPE() {
    return CUSTOMER_TYPE;
  }

  public String getName() {
    return name;
  }

  public int getCustomerNum() {
    return customerNum;
  }

  public List<Order> getOrders() {
    return orders;
  }
}