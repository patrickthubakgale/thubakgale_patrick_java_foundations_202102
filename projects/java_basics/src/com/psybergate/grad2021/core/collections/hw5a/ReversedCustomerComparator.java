package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Comparator;

public class ReversedCustomerComparator implements Comparator {
  @Override public int compare(Object o1, Object o2) {
    Customer customer1 = (Customer) o1;
    Customer customer2 = (Customer) o2;
    return -1 * (customer1.getCustomerNum() - customer2.getCustomerNum());
  }
}
