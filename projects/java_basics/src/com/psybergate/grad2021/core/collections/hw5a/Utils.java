package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Iterator;

public class Utils {

  public static void main(String[] args) {
    SortedStack sortedStack = new TreeStack();

    sortedStack.push("apples");
    sortedStack.push("strawberries");
    sortedStack.push("grapes");
    sortedStack.push("banana");
    sortedStack.push("guava");

    //printStack(sortedStack);
    //##########################

    Customer customer1 = new Customer(34, "John");
    Customer customer2 = new Customer(28, "Walter");
    Customer customer3 = new Customer(71, "Piet");
    Customer customer4 = new Customer(2, "Steve");
    Customer customer5 = new Customer(1, "Marius");

    SortedStack sortedStack1 = new TreeStack(new ReversedCustomerComparator());
    sortedStack1.push(customer1);
    sortedStack1.push(customer2);
    sortedStack1.push(customer3);
    sortedStack1.push(customer4);
    sortedStack1.push(customer5);
    printStack(sortedStack1);
    //#############

    System.out.println("sortedStack1.size() = " + sortedStack1.size());
  }

  public static void printStack(SortedStack sortedStack) {
    for (Iterator iter = sortedStack.iterator(); iter.hasNext(); ) {
      System.out.println(iter.next());
    }
  }
}
