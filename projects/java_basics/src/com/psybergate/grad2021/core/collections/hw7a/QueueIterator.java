package com.psybergate.grad2021.core.collections.hw7a;

import java.util.Iterator;

public class QueueIterator implements Iterator {

  private Node head;

  public QueueIterator(Node head) {
    this.head = head;
  }

  @Override public boolean hasNext() {
    return head != null;
  }

  @Override public Object next() {
    if (!hasNext()) {
      throw new IndexOutOfBoundsException("Reached end of the Queue");
    }
    Object data = head.getData();
    head = head.getNext();
    return data;
  }
}
