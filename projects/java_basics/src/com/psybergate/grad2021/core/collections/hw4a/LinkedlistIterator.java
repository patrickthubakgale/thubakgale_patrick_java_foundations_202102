package com.psybergate.grad2021.core.collections.hw4a;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedlistIterator implements Iterator {

  private Node current;

  public LinkedlistIterator(Node head) {
    this.current = head.getNext();
  }

  @Override public boolean hasNext() {
    return current != null;
  }

  @Override public Object next() {
    if (!hasNext()) {
      throw new NoSuchElementException("No element found");
    }
    Object data = current.getElement();
    current = current.getNext();
    return data;
  }
}
