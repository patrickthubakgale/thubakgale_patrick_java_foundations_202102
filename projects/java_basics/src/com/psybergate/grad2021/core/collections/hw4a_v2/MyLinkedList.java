package com.psybergate.grad2021.core.collections.hw4a_v2;

import java.util.Iterator;

public class MyLinkedList extends AbstractLinkedList {

  Node head;

  private Node tail;

  private int size;

  public MyLinkedList() {
  }

  @Override
  public boolean add(Object element) {
    ++size;
    if (isEmpty()) {
      head = new Node(null, element);
      tail = head;
      return true;
    }
    Node newNode = new Node(tail, element);
    tail = newNode;
    return true;
  }

  @Override
  public boolean contains(Object o) {
    for (Iterator iterator = iterator(); iterator.hasNext(); ) {
      if (o == iterator.next()) {
        return true;
      }
    }
    return false;
  }

  public int size () {
    return this.size;
  }

  public boolean isEmpty() {
    return head == null;
  }

  @Override
  public Iterator iterator() {
    return new LinkedlistIterator(this);
  }

  @Override public Object get(int index) {
    if (isEmpty()) {
      throw new IndexOutOfBoundsException("List is empty");
    }
    int position = 0;
    for (Iterator iter = iterator(); iter.hasNext(); ) {
      Object data = iter.next();
      if (position == index) {
        return data;
      }
      ++position;
    }
    throw new IndexOutOfBoundsException("No element in the specified index");
  }

  @Override public Object remove(int index) {
    if (index < 0 || index > size() - 1) {
      throw new ArrayIndexOutOfBoundsException("Out of bounds");
    }
    Node current = head;
    int position = 0;
    while (current != null) {
      if (position == index) {
        Object data = current.next().getElement();
        Node temp = current.next();
        current.setNext(current.next().next());
        temp.setNext(null);
        --size;
        return data;
      }
      position++;
      current = current.next();
    }
    return null;
  }

  @Override
  public Object set(int index, Object element) {
    if (index > size() - 1) {
      throw new ArrayIndexOutOfBoundsException("Index out of bounds");
    }
    int position = 0;

    Node current = head;
    while (current != null) {
      if (position == index) {
        Object data = current.getElement();
        current.setElement(element);
        return data;
      }
      current = current.next();
      ++position;
    }
    return null;
  }
}
