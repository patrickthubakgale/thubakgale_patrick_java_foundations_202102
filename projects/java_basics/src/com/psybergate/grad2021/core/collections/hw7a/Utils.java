package com.psybergate.grad2021.core.collections.hw7a;

import java.util.Iterator;

public class Utils {

  public static void main(String[] args) {
    Queue queue = new Queue();
    queue.add("Darius");
    queue.add("Steve");
    queue.add("Lizzy");
    queue.add("Dan");
    queue.add("Redington");
    queue.add("James");

    printQueue(queue);

    queue.poll();
    printQueue(queue);
    System.out.println("queue.peek() = " + queue.peek());
  }

  private static void printQueue(Queue queue) {
    for (Iterator iter = queue.iterator(); iter.hasNext(); ) {
      System.out.println(iter.next());
    }
    System.out.println();
  }


}
