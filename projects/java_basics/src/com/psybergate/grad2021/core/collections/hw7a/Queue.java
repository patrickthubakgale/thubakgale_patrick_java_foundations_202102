package com.psybergate.grad2021.core.collections.hw7a;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue implements Collection {

  private Node head;

  private Node tail;

  private Node previous;

  private int size;

  public Queue() {
    head = null;
    tail = null;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return head == null;
  }

  @Override public boolean add(Object o) {
    Node temp = new Node();
    temp.setData(o);
    temp.setNext(null);

    if (isEmpty()) {
      head = temp;
      tail = temp;
    } else {
      previous = tail;
      tail.setNext(temp);
      tail = temp;
    }

    return true;
  }

  public Object remove() {
    if (isEmpty()) {
      throw new NoSuchElementException("Element not found");
    }
    Node temp = head;
    Object data = temp.getData();
    head = head.getNext();
    temp = null;
    return data;
  }

  public Object poll() {
    if (isEmpty()) {
      return null;
    }
    Node temp = head;
    Object data = temp.getData();
    head = head.getNext();
    temp = null;
    return data;
  }

  public Object peek() {
    if (isEmpty()) {
      return null;
    }
    return head.getData();
  }

  public Object element() {
    if (isEmpty()) {
      throw new NoSuchElementException("No such element");
    }
    return head.getData();
  }

  @Override public boolean remove(Object o) {
//    if (isEmpty()) {
//      throw new NoSuchElementException("Element not found");
//    }
//    Node temp = head;
//    head = head.getNext();
//    temp = null;
    return true;
  }

  @Override public boolean contains(Object o) {
    if (isEmpty()) {
      return false;
    }
    Node current = head;
    for (Iterator iter = iterator(); iter.hasNext(); ) {
      Object data = iter.next();
      if (data == o) {
        return true;
      }
    }
    return false;
  }

  @Override public Iterator iterator() {
    return new QueueIterator(head);
  }

  @Override public Object[] toArray() {
    return new Object[0];
  }

  @Override public boolean addAll(Collection c) {
    return false;
  }

  @Override public void clear() {

  }

  @Override public boolean retainAll(Collection c) {
    return false;
  }

  @Override public boolean removeAll(Collection c) {
    return false;
  }

  @Override public boolean containsAll(Collection c) {
    return false;
  }

  @Override public Object[] toArray(Object[] a) {
    return new Object[0];
  }


}
