package com.psybergate.grad2021.core.collections.hw6a;

import java.util.Collection;
import java.util.Set;

public abstract class AbstractHashSet implements Set {

  @Override public Object[] toArray() {
    return new Object[0];
  }

  @Override public boolean addAll(Collection c) {
    return false;
  }

  @Override public void clear() {

  }

  @Override public boolean retainAll(Collection c) {
    return false;
  }

  @Override public boolean removeAll(Collection c) {
    return false;
  }

  @Override public boolean containsAll(Collection c) {
    return false;
  }

  @Override public Object[] toArray(Object[] a) {
    return new Object[0];
  }
}
