package com.psybergate.grad2021.core.collections.ce1a;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayStack extends AbstractStack {

  private int size = 0;

  private Object[] elements = new Object[100];

  private int capacity = elements.length;

  @Override public void push(Object o) {
    add(o);
  }

  @Override public Object pop() {
    Object temp = elements[size-1];
    elements[size] = null;
    --size;
    return temp;
  }

  @Override public Object get(int position) {
    return elements[size()-position-1];
  }

  @Override public int size() {
    return size;
  }

  @Override public boolean isEmpty() {
    return size() == 0;
  }

  @Override public boolean contains(Object o) {
    for (Object element : elements) {
      if (element.equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override public Iterator iterator() {
    return new ArrayIterator();
  }

  @Override public boolean add(Object o) {
    if (size == capacity) {
      capacity = capacity*2;
      Object[] data = new Object[capacity];

      for (int i = 0; i < size; i++) {
        data[i] = elements[i];
      }
      elements = data;
    }
    elements[size++] = o;
    return true;
  }

  public Object[] getElements() {
    return elements;
  }

  class ArrayIterator implements Iterator {

    private int currentPos = size-1; //inverted since going from top to bottom

    public boolean hasNext() {
      return currentPos >= 0;
    }

    public Object next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }
      return elements[currentPos--];
    }
  }
}
