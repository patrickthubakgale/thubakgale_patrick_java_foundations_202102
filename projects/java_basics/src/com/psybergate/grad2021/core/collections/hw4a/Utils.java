package com.psybergate.grad2021.core.collections.hw4a;

public class Utils {
  public static void main(String[] args) {
    MyLinkedList list = new MyLinkedList();
    list.add("apple");
    list.add("pineapple");
    list.add("avoccado");
    list.add("banana");
    list.add("strawberries");
    System.out.println(list.get(0));
    list.set(0, "grapes");
    System.out.println(list.get(0));

//    list.printLinkedList();
//    System.out.println("list.size() = " + list.size());
//    System.out.println();
//    list.remove(2);
//    list.printLinkedList();
//    System.out.println("list.size() = " + list.size());
  }

}
