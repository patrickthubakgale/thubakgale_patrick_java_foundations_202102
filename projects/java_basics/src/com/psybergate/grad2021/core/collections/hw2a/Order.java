package com.psybergate.grad2021.core.collections.hw2a;

import java.util.ArrayList;
import java.util.List;

public abstract class Order {

  private final String orderNumber;

  private final String policy;

  private final int customerDuration;

  List<OrderItem> orderItems = new ArrayList<>();

  public Order(final String orderNumber, final String policy, final int customerDuration) {
    this.orderNumber = orderNumber;
    this.policy = policy;
    this.customerDuration = customerDuration;
  }

  public void addOrder(final String name, final double price, final int quantity) {
    this.orderItems.add(new OrderItem(quantity, new Product(price, name)));
  }

  public double calcTotal() {
    double total = 0;
    for (OrderItem orderItem : orderItems) {
      total += (orderItem.getProduct().getPrice() * orderItem.getQuantity());
    }
    return total;
  }

  public String getPolicy() {
    return this.policy;
  }

  public double applyPolicy() {
    if (this.getPolicy().equalsIgnoreCase("Local")) {
      return this.getLocalDiscount();
    } else {
      return this.getInternationalDiscount();
    }
  }

  private double getInternationalDiscount() {
    if (this.calcTotal() <= 500_000) {
      return 0;
    } else if (this.calcTotal() > 500_000 && this.calcTotal() <= 1_000_000) {
      return 0.05;
    } else {
      return  0.1;
    }
  }

  private double getLocalDiscount() {
    if (this.customerDuration <= 2) {
      return  0;
    } else if (this.customerDuration > 2 && this.customerDuration <= 5) {
      return 0.075;
    } else {
      return 0.12;
    }
  }

  public int getCustomerDuration() {
    return this.customerDuration;
  }

  public List<OrderItem> getOrderItems() {
    return this.orderItems;
  }

  public String getOrderNumber() {
    return this.orderNumber;
  }

  public abstract double totalAfterDiscount();
}