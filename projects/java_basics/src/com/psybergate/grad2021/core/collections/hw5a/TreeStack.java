package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class TreeStack extends MyAbstractTreeStack {

  Object[] elements = new Object[100];

  private Comparator comparator;

  private int size = 0;

  public TreeStack() {
    comparator = null;
  }

  public TreeStack(Comparator comparator) {
    this.comparator = comparator;
  }

  @Override
  public Comparator comparator() {
    return comparator;
  }

  @Override public Object first() {
    if (isEmpty()) {
      throw new NoSuchElementException("Stack is empty");
    }
    return elements[0];
  }

  @Override public Object last() {
    if (isEmpty()) {
      throw new NoSuchElementException("Stack is empty");
    }
    return elements[size-1];
  }

  @Override public int size() {
    return size;
  }

  @Override public boolean isEmpty() {
    return size() == 0;
  }

  @Override public boolean contains(Object o) {
    for (Object element : elements) {
      if (element.equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override public Object get(int position) {
    return elements[size()-position-1];
  }

  @Override public Object pop() {
    Object temp = elements[size-1];
    elements[size-1] = null;
    --size;
    return temp;
  }

  @Override public void push(Object o) {
    add(o);
  }

  @Override public boolean add(Object o) {
    if (size == elements.length) {
      doubleList();
    }
    //use the provided comparator
    if (comparator() != null) {
      comparatorAdd(o);
    } else { //use comparable
      comparableAdd(o);
    }
    ++size;
    return true;
  }

  private void comparatorAdd(Object o) {
    for (int i = 0; i < size; i++) {
      int cpr = comparator().compare(o, elements[i]); //[2, 3, 4, 6 ,7, 8]
      if (cpr < 0) {
        shiftElements(i);
        elements[i] = o;
        return;
      }
    }
    elements[size] = o;
  }

  private void comparableAdd(Object o) {
    Comparable comp = (Comparable) o;
    for (int i = 0; i < size; i++) {
      int cmp = comp.compareTo(elements[i]);
      if (cmp < 0) {
        shiftElements(i);
        elements[i] = o;
        return;
      }
    }
    elements[size] = o;
  }

  private void shiftElements(int index) {
    for (int i = size-1; i >= index; i--) {
      elements[i+1] = elements[i];
    }
  }

  private void doubleList() {
    Object[] data = new Object[elements.length*2];

    for (int i = 0; i < size; i++) {
      data[i] = elements[i];
    }
    elements = data;
  }

  @Override
  public Object[] getElements() {
    return elements;
  }

  @Override public Iterator iterator() {
    return new TreeStackIterator(this);
  }
}
