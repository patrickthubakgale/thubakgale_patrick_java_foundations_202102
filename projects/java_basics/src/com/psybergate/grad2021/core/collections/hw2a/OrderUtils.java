package com.psybergate.grad2021.core.collections.hw2a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderUtils {

  private static Map m1 = new HashMap();

  public static void main(String[] args) {
    Customer customer1 = new Customer(1, "lerato", "LocalCustomer");
    Customer customer2 = new Customer(2, "James", "LocalCustomer");
    Customer customer3 = new Customer(3, "Amos", "InternationalCustomer");
    Customer customer4 = new Customer(4, "Robert", "InternationalCustomer");

    //customer(custType, order)
    //return order -> from this, do order.addOrder()
    Order order1 = new InternationalOrder("123", "Local", 2, 5_000);
    Order order2 = new InternationalOrder("001", "International", 5, 10_000);
    Order order3 = new LocalOrder("564", "Local", 7);
    Order order4 = new LocalOrder("967", "International", 3);

    order1.addOrder("Apples", 5, 200);
    order1.addOrder("Strawberries", 10, 100);
    order1.addOrder("Banana", 3, 200);

    order2.addOrder("Shoes", 50, 10);
    order2.addOrder("Socks", 10, 20);
    order2.addOrder("Trousers", 100, 10);
    order2.addOrder("Cigarette", 50, 500);

    order3.addOrder("Biscuits", 10, 100);
    order3.addOrder("Coke", 10, 300);
    order3.addOrder("Pillows", 40, 150);
    order3.addOrder("Sim Card", 1, 300);
    order3.addOrder("Router", 200, 500);

    order4.addOrder("Biscuits", 10, 100);
    order4.addOrder("Coke", 10, 300);
    order4.addOrder("Pillows", 40, 150);
    order4.addOrder("Sim Card", 1, 300);
    order4.addOrder("Router", 200, 500);
    order4.addOrder("Router", 200, 500);

    customer1.makeOrder(order1);
    customer2.makeOrder(order2);
    customer3.makeOrder(order3);
    customer4.makeOrder(order4);

    printCustomerTotals(customer1);
    printCustomerTotals(customer2);
    printCustomerTotals(customer3);
    printCustomerTotals(customer4);

    //Store customers in a list
    List<Customer> customers = new ArrayList<>();
    customers.add(customer1);
    customers.add(customer2);
    customers.add(customer3);
    customers.add(customer4);

    System.out.println("\nGet customer salary");
    customerInfo(customers);
    printEmployeeSalary(customer4);
  }

  private static void printEmployeeSalary(Customer customer1) {
    for (Object o : m1.keySet()) {
      if (o.equals(customer1)) {
        System.out.println(m1.get(customer1));
        return;
      }
    }
    System.out.println("Unknown key");
  }

  public static void printCustomerTotals(Customer customer) {
    System.out.println("Total cost for " + customer.getCUSTOMER_TYPE() + " is " + customer.getCustomerOrderTotals());
  }

  public static void customerInfo(List<Customer> customers) {
    for (Customer customer : customers) {
      m1.put(customer, customer.getCustomerOrderTotals());
    }
  }



}
