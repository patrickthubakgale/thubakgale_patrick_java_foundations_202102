package com.psybergate.grad2021.core.collections.hw1a;

public enum PolicyType {
  INTERNATIONAL, LOCAL
}
