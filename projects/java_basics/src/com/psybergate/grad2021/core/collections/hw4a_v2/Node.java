package com.psybergate.grad2021.core.collections.hw4a_v2;

public class Node {

  Node next;

  Node previous;

  private Object element;

  public Node(Node previous, Object element) {
    this.element = element;
    if (previous != null) {
      previous.next = this;
    }
    this.previous = previous;
  }

  public Object getElement() {
    return this.element;
  }

  public Node next() {
    return this.next;
  }

  public void setNext(Node next) {
    this.next = next;
  }

  public void setElement(Object element) {
    this.element = element;
  }
}
