package com.psybergate.grad2021.core.collections.hw4a_v2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedlistIterator implements Iterator {

  private MyLinkedList myList;

  private Node current;

  public LinkedlistIterator(MyLinkedList myList) {
    this.myList = myList;
    this.current = myList.head;
  }

  @Override public boolean hasNext() {
    return current != null;
  }

  @Override public Object next() {
    if (!hasNext()) {
      throw new NoSuchElementException("No more elements");
    }
    Object data = current.getElement();
    current = current.next();
    return data;
  }
}
