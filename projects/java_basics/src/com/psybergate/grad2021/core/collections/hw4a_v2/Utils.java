package com.psybergate.grad2021.core.collections.hw4a_v2;

import java.util.Iterator;
import java.util.List;

public class Utils {

  public static void main(String[] args) {
    MyLinkedList list = new MyLinkedList();
    list.add("apple");
    list.add("pineapple");
    list.add("avoccado");
    list.add("banana");
    list.add("strawberries");

    System.out.println("list.get(2) = " + list.get(2));
    printList(list);
  }

  private static void printList(List list) {
    for (Iterator iter = list.iterator(); iter.hasNext(); ) {
      System.out.println(iter.next());
    }
  }
}
