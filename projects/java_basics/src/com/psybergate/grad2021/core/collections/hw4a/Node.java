package com.psybergate.grad2021.core.collections.hw4a;

public class Node {

    private Object element;

    private Node next;

    public Node(Object element, Node next) {
      this.element = element;
      this.next = next;
    }

    public Object getElement() {
      return this.element;
    }

    public Node getNext() {
      return this.next;
    }

    public void setNext(Node next) {
      this.next = next;
    }

    public void setElement(Object element) {
      this.element = element;
    }
}
