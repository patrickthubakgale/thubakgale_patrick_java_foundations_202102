package com.psybergate.grad2021.core.collections.hw5a;

public class Customer implements Comparable{

  private int customerNum;

  private String name;

  public Customer(int customerNum, String name) {
    this.customerNum = customerNum;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getCustomerNum() {
    return customerNum;
  }

  @Override public String toString() {
    return "Customer{" +
            "customerNum='" + customerNum + '\'' +
            ", name='" + name + '\'' +
            '}';
  }

  @Override public int compareTo(Object o) {
    return (this.getCustomerNum() - ((Customer) o).getCustomerNum());
  }
}
