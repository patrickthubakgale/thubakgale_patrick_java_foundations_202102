package com.psybergate.grad2021.core.collections;

public class ArrayInitialization {

  public static void main(String[] args) {
    int arr[][][] = new int[3][][];

    //It looks like an array of 2d arrays
    int arr2[][][] = {
            {
              {1, 2, 3},
              {4, 9, 1}
            },
            {
              {5, 9, 2},
              {3, 8, 1, 7},
              {2, 0, 4}
            },
            {
              {1, 2}
            }
    };
  }
}
