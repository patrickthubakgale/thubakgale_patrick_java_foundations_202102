package com.psybergate.grad2021.core.collections.hw6a;

import java.util.Iterator;

public class HashSetIterator implements Iterator {

  private Object[] elements;

  private int cursor;

  private int size;

  public HashSetIterator(Object[] elements, int size) {
    this.elements = elements;
    this.size = size;
    this.cursor = 0;
  }

  @Override public boolean hasNext() {
    return cursor != size;
  }

  @Override public Object next() {
    if (!hasNext()) {
      throw new ArrayIndexOutOfBoundsException("Out of bounds");
    }
    Object data = elements[cursor++];
    return data;
  }
}
