package com.psybergate.grad2021.core.collections.hw1a;

import java.time.LocalDate;
import java.time.Period;

public class Customer implements Comparable {

  private int customerNum;

  private String name;

  private CustomerType customerType;

  private LocalDate customerStartDate;

  public Customer(int customerNum, String name, CustomerType customerType,
                  LocalDate customerStartDate) {
    this.customerNum = customerNum;
    this.name = name;
    this.customerType = customerType;
    this.customerStartDate = customerStartDate;
  }

  public String getName() {
    return name;
  }

  public int getCustomerNum() {
    return customerNum;
  }

  public CustomerType getCustomerType() {
    return customerType;
  }

  public int getCustomerDuration() {
    Period period = Period.between(LocalDate.now(), customerStartDate);
    return Math.abs(period.getYears());
  }

  @Override public String toString() {
    return "Customer{" +
            "customerNum=" + customerNum +
            ", name='" + name + '\'' +
            '}';
  }

  @Override public int compareTo(Object o) {
    return this.getCustomerNum() - ((Customer) o).getCustomerNum();
  }
}
