package com.psybergate.grad2021.core.collections.hw3a;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class StringReverse {

  public static void main(String[] args) {
    SortedSet stringSet = new TreeSet(new Comparator<String>() {
      @Override public int compare(String o1, String o2) {
        return -1 * o1.compareToIgnoreCase(o2);
      }
    });

    stringSet.add("Jeremy");
    stringSet.add("Chris");
    stringSet.add("sean");
    stringSet.add("Patrick");
    stringSet.add("Suhail");
    stringSet.add("Ashraf");
    stringSet.add("Irfan");
    stringSet.add("Stanley");

    printCollection(stringSet);
  }

  private static void printCollection(SortedSet stringSet) {
    for (Object o : stringSet) {
      System.out.println(o);
    }
  }


}

