package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Comparator;

public interface SortedStack extends MyStack {

  public Comparator comparator();

  public Object first();

  public Object last();

  public Object[] getElements();
}
