package com.psybergate.grad2021.core.collections.hw7a;

public class Node {

  private Object data;

  private Node next;

  public void setNext(Node next) {
    this.next = next;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public Node getNext() {
    return next;
  }

  public Object getData() {
    return data;
  }
}
