package com.psybergate.grad2021.core.collections.hw6a;

import java.util.Iterator;
import java.util.Objects;

public class MyHashSet extends AbstractHashSet {

  private Object[][] elements = new Object[100][];

  private int hash;

  private int capacity;

  public MyHashSet(){
    capacity = elements.length;
  }

  @Override public boolean add(Object o) {

    return false;
  }

  @Override public boolean remove(Object o) {
    return false;
  }

  @Override public int size() {
    return 0;
  }

  @Override public boolean isEmpty() {
    return true;
  }

  @Override public boolean contains(Object o) {
    return true;
  }

  @Override public Iterator iterator() {
    return null;
  }

  @Override public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    return false;
  }

  public int hash(int value) {
    hash = Objects.hash(value);
    return hash;
  }

  @Override public int hashCode() {
    return Objects.hashCode(hash) % capacity;
  }
}
