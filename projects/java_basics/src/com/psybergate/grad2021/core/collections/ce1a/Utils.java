package com.psybergate.grad2021.core.collections.ce1a;

import java.util.Iterator;

public class Utils {

  public static void main(String[] args) {
    MyStack stack = new ArrayStack();
    stack.push("coffee");
    stack.push("tea");
    stack.push("caffeine");
    stack.push("soda");
    stack.push("coke");

    System.out.println("stack.get(2) = " + stack.get(2));
    System.out.println("stack.pop() = " + stack.pop());

    printCollection(stack);
  }

  private static void printCollection(MyStack stack) {
    for (Iterator iter = stack.iterator(); iter.hasNext(); ) {
      System.out.println(iter.next());
    }
  }
}
