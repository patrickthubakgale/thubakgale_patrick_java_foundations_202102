package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Collection;

public interface MyStack extends Collection {

  public void push(Object o);

  public Object pop();

  public Object get(int position);
}
