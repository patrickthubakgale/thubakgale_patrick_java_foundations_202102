package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class TreeStackIterator implements Iterator {

  private Object[] elements;

  private int currentPos;

  public TreeStackIterator(TreeStack stack) {
    this.elements = stack.elements;
    this.currentPos = stack.size()-1;
  }

  public boolean hasNext() {
    return currentPos >= 0;
  }

  public Object next() {
    if (!hasNext()) {
      throw new NoSuchElementException("No elements in the stack");
    }
    return elements[currentPos--];
  }
}
