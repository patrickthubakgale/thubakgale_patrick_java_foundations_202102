package com.psybergate.grad2021.core.collections.hw4a;

import java.util.*;

public class MyLinkedList implements List {

  private Node head;

  private Node last;

  private int size;

  public MyLinkedList() {
    this.head = new Node(null, null);
    this.last = head;
    this.size = 0;
  }

  public int size () {
    return this.size;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  @Override
  public Iterator iterator() {
    return new LinkedlistIterator(getHead());
  }

  @Override
  public boolean contains(Object o) {
    for (Iterator iterator = iterator(); iterator.hasNext(); ) {
      if (o == iterator.next()) {
        return true;
      }
    }
    return false;
  }

  @Override public Object get(int index) {
    if (isEmpty()) {
      throw new IndexOutOfBoundsException("List is empty");
    }
    int position = 0;
    for (Iterator iter = iterator(); iter.hasNext(); ) {
      Object data = iter.next();
      if (position == index) {
        return data;
      }
      ++position;
    }
    throw new IndexOutOfBoundsException("No element in the specified index");
  }

  @Override
  public boolean add(Object element) {
    Node newNode = new Node(element, null);
    last.setNext(newNode);
    last = newNode;
    ++size;
    return true;
  }

  @Override public boolean remove(Object o) {
    return false;
  }

  @Override public Object remove(int index) {
    if (index < 0 || index > size() - 1) {
      throw new ArrayIndexOutOfBoundsException("Out of bounds");
    }
    Node current = getHead().getNext();
    int position = 0;
    while (current != null) {
      if (position == index - 1) {
        Object data = current.getNext().getElement();
        Node temp = current.getNext();
        current.setNext(current.getNext().getNext());
        temp.setNext(null);
        --size;
        return data;
      }
      position++;
      current = current.getNext();
    }
    return null;
  }

  public Node getHead() {
    return head;
  }

  public void printLinkedList() {
    Node current = getHead().getNext();
    if (current == null) {
      System.out.println("Linkedlist is empty");
      return;
    }

    while (current != null) {
      System.out.println(current.getElement());
      current = current.getNext();
    }
  }

  @Override
  public Object set(int index, Object element) {
    if (index > size() - 1) {
      throw new ArrayIndexOutOfBoundsException("Index out of bounds");
    }
    int position = 0;

    Node current = getHead().getNext();
    while (current != null) {
      if (position == index) {
        Object data = current.getElement();
        current.setElement(element);
        return data;
      }
      current = current.getNext();
      ++position;
    }
    return null;
  }

  @Override public void add(int index, Object element) {

  }

  @Override public Object[] toArray() {
    return new Object[0];
  }

  @Override public boolean addAll(Collection c) {
    return false;
  }

  @Override public boolean addAll(int index, Collection c) {
    return false;
  }

  @Override public void clear() {

  }

  @Override public int indexOf(Object o) {
    return 0;
  }

  @Override public int lastIndexOf(Object o) {
    return 0;
  }

  @Override public ListIterator listIterator() {
    return null;
  }

  @Override public ListIterator listIterator(int index) {
    return null;
  }

  @Override public List subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override public boolean retainAll(Collection c) {
    return false;
  }

  @Override public boolean removeAll(Collection c) {
    return false;
  }

  @Override public boolean containsAll(Collection c) {
    return false;
  }

  @Override public Object[] toArray(Object[] a) {
    return new Object[0];
  }
}
