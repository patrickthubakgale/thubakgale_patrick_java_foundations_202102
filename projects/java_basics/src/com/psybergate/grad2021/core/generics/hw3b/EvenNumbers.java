package com.psybergate.grad2021.core.generics.hw3b;

public class EvenNumbers implements Condition<Integer> {

  @Override public boolean isSatisfiedBy(Integer elem) {
    return elem % 2 == 0;
  }
}
