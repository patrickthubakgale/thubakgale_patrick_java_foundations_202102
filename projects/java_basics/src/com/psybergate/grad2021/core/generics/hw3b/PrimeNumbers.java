package com.psybergate.grad2021.core.generics.hw3b;

public class PrimeNumbers implements Condition<Integer> {

  @Override public boolean isSatisfiedBy(Integer elem) {
    if (elem <= 3) return elem > 1;
    if (elem % 2 == 0 || elem % 3 == 0) return false;
    int num = 5;
    while (Math.pow(num,2) <= elem) {
      if (elem % num == 0 || elem % (num + 2) == 0) {
        return false;
      }
      num += 6;
    }
    return true;
  }
}
