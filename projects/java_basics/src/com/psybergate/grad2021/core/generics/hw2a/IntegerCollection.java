package com.psybergate.grad2021.core.generics.hw2a;

import java.util.ArrayList;
import java.util.Collection;

public class IntegerCollection {

  public static Collection<Integer> greaterThan1000(Collection<Integer> collection) {
    Collection<Integer> newCollection = new ArrayList<>();
    for (Integer integer : collection) {
      if(integer > 1000) {
        newCollection.add(integer);
      }
    }
    return newCollection;
  }
}
