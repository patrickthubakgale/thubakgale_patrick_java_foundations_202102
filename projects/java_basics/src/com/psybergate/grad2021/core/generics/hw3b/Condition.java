package com.psybergate.grad2021.core.generics.hw3b;

public interface Condition<T> {

  public boolean isSatisfiedBy(T elem);
}
