package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReverseString {

  public static void main(String[] args) {
    List<String> list = Arrays.asList("Julian", "James", "Abbot", "Jacob", "Daniel", "Peter");
    print(anyMethod01(list));
    print(anyMethod01AfterErasure(list));
  }

  public static List<String> anyMethod01(List<String> list) {
    List<String> reversed = new ArrayList<>();
    for (int i = 0; i < list.size(); i++) {
      reversed.add(new StringBuilder(list.get(i)).reverse().toString());
    }
    return reversed;
  }

  public static List anyMethod01AfterErasure(List list) {
    List reversed = new ArrayList();
    for (int i = 0; i < list.size(); i++) {
      reversed.add(new StringBuilder((String)list.get(i)).reverse().toString());
    }
    return reversed;
  }

  public static void print(List<String> list) {
    for (String s : list) {
      System.out.println(s);
    }
  }
}
