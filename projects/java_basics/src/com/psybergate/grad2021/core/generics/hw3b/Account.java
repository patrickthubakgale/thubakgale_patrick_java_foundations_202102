package com.psybergate.grad2021.core.generics.hw3b;

public class Account {

  private String name;

  private double balance;

  public Account(String name, double balance) {
    this.name = name;
    this.balance = balance;
  }

  public String getName() {
    return name;
  }

  public double getBalance() {
    return balance;
  }

  @Override public String toString() {
    return "Account{" +
            "name='" + name + '\'' +
            ", balance=" + balance +
            '}';
  }
}
