package com.psybergate.grad2021.core.generics.hw3b;

public class StringCollection implements Condition<String> {

  @Override public boolean isSatisfiedBy(String elem) {
    return elem.charAt(0) == 'c';
  }
}
