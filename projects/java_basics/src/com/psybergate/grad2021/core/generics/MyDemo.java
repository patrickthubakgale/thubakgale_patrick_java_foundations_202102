package com.psybergate.grad2021.core.generics;

import java.util.ArrayList;
import java.util.List;

public class MyDemo {

  public static void main(String[] args) {
    new MyDemo().listObjectNotSameAsList();
  }

  public void listObjectNotSameAsList() {
    List<String> listStr = new ArrayList<String>();

    List list = listStr;
    System.out.println("Added: " + list.add(new Object()));
    list.add(3);
    list.add("Amo");
    System.out.println(list.get(0));
  }
}
