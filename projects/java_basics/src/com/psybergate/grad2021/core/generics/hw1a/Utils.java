package com.psybergate.grad2021.core.generics.hw1a;

public class Utils {

  public static void main(String[] args) {
    MyMap<String, Integer> myMap = new MyMap<>("key", 1);
    System.out.println("myMap.getKey() = " + myMap.getKey());
    System.out.println("myMap.getValue() = " + myMap.getValue());
    myMap.setKey("key2");
    System.out.println("myMap.getKey() = " + myMap.getKey());

    MyMap<String, String> myMap2 = new MyMap<>("key4", "key5");
    System.out.println("myMap.getKey() = " + myMap2.getKey());

    //##############After Erasure####################

    MyMapAfterErasure map = new MyMapAfterErasure("Pat", "Two");
    System.out.println("myMap.getKey() = " + map.getKey());
  }
}