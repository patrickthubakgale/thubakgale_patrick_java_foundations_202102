package com.psybergate.grad2021.core.generics.hw4a;

public class NodeUtils {

  public static void main(String[] args) {
    MyNode node1 = new MyNode(5);
    Node node2 = node1;
    node2.setData("hello");
    Integer x = node1.data;
  }
}
