package com.psybergate.grad2021.core.generics.hw4a;

public class NodeAfterErasure {

  public Object data;

  public NodeAfterErasure(Object data) {
    this.data = data;
  }

  public void setData(Object data) {
    System.out.println("Node.setData");
    this.data = data;
  }
}

class MyNodeAfterErasure extends NodeAfterErasure {

  public MyNodeAfterErasure(Integer data) {
    super(data);
  }
//
//  public void setData(Object data) {
//    setData((Integer) data);
//  } //bridge method created by the compiler

  @Override public void setData(Object data) {
    System.out.println("MyNode.setData");
    super.setData(data);
  }
}
