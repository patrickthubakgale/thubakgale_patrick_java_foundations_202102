package com.psybergate.grad2021.core.generics.hw3b;

import java.util.Arrays;
import java.util.Collection;

public class Utils {

  public static void main(String[] args) {
    Collection<Integer> coll = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    int evenCount = ElementCounter.countElements(coll, new EvenNumbers());
    int oddCount = ElementCounter.countElements(coll, new OddNumbers());
    int primeCount = ElementCounter.countElements(coll, new PrimeNumbers());

    System.out.println("Number of even numbers in the collection is: " + evenCount);
    System.out.println("Number of odd numbers in the collection is: " + oddCount);
    System.out.println("Number of prime numbers in the collection is: " + primeCount);

    Collection<String> coll2 = Arrays.asList("common", "jive", "community", "counsel", "barbaric"
            , "goals", "camel");
    int numberOfWordsStartWithC = ElementCounter.countElements(coll2, new StringCollection());
    System.out.println("Number of words that starts with c in the collection is: " + numberOfWordsStartWithC);
    Collection<Account> coll3 = Arrays.asList(new Account("Jeremy", -2300),
                                              new Account("Suhail", 2400),
                                              new Account("Joseph", -1000),
                                              new Account("Stanley", 800),
                                              new Account("James", 500),
                                              new Account("Patrick", -200));
    int numberOfAccountsWithNegativeBalance =
            ElementCounter.countElements(coll3, new AccountsCollection());
    System.out.println("Number of accounts with negative balance is: " + numberOfAccountsWithNegativeBalance);
  }
}
