package com.psybergate.grad2021.core.generics.hw3b;

public class AccountsCollection implements Condition<Account> {

  @Override public boolean isSatisfiedBy(Account elem) {
    return elem.getBalance() < 0;
  }
}
