package com.psybergate.grad2021.core.generics.hw4a;

import java.time.LocalDate;

public class DateInterval extends Pair<LocalDate> {

  public static void main(String[] args) {
    DateInterval interval = new DateInterval();
    Pair<LocalDate> pair = interval;
    pair.setSecond(LocalDate.of(1903, 12, 3));
  }

  public void setSecond(LocalDate second) {
    if (second.compareTo(getFirst()) >= 0) {
      super.setSecond(second);
    }
    /*
    public void setSecond(Object second) {
      setSecond((LocalDate) second)
    }
     */
  }
}
