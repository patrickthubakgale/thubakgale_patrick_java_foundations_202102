package com.psybergate.grad2021.core.generics.hw2a;

import java.util.Arrays;
import java.util.Collection;

public class CollectionDemo {

  public static void main(String[] args) {
    Collection<Integer> coll = Arrays.asList(300, 1200, 500, 9000, 1001, 4009, 200);
    Collection<Integer> coll2 = IntegerCollection.greaterThan1000(coll); //collection of integers
    // compiles
    for (Integer integer : coll2) {
      System.out.println(integer);
    }

    Collection<String> coll3 = Arrays.asList("Apples", "bananas", "grapes", "oranges",
            "strawberries");
    //Collection<String> coll4 = IntegerCollection.greaterThan1000(coll3); //compiler complains.
    // It wants collection of integers
  }
}
