package com.psybergate.grad2021.core.generics;

public class Student extends Person {

  private String name;

  private int age;

  private String course;

  public Student(String name, int age, String course) {
    this.name = name;
    this.age = age;
    this.course = course;
  }

  public String getName() {
    return name;
  }

  public int getAge() {
    return age;
  }

  public String getCourse() {
    return course;
  }

  @Override public String toString() {
    return "Student{" +
            "name='" + name + '\'' +
            ", age=" + age +
            ", course='" + course + '\'' +
            '}';
  }
}
