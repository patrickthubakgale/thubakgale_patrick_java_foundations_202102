package com.psybergate.grad2021.core.generics.hw1b;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class TeamAfterErasure {

  private static final int MAX_TEAM_SIZE = 15;

  private List people = new ArrayList();

  public static int maxTeamSize() {
    return MAX_TEAM_SIZE;
  }

  public void addPerson(Person t) {
    people.add(t);
  }

  public void anyMethod(Person t) {
    // do nothing
  }

  public Person getPerson(int position) {
    return (Person) people.get(position);
  }

  public int getNumOfPersons() {
    return people.size();
  }


  @SuppressWarnings("unchecked")
  // Perhaps also show this method 2 generic type parameters
  public Person[] asArray(Person[] e) {
    for (int i = 0; i < people.size(); i++) {
      e[i] = getPerson(i);
    }
    return e;
  }

  public void someRandom(List names) {
    names.add(new Student());
    System.out.println(names.get(1));
  }

  public static void main(String[] args)
          throws NoSuchMethodException, SecurityException {
    TeamAfterErasure team = new TeamAfterErasure();

    team.addPerson(new Student("Chris", 40));
    team.addPerson(new Student("Colin", 35));
    for (int i = 0; i < team.getNumOfPersons(); i++) {
      System.out.println(i + " name: " + team.getPerson(i).getName());
      System.out.println(i + " yearregistered: " + ((Student) team.getPerson(i)).getYearRegistered());
    }
    @SuppressWarnings("rawtypes")
    TeamAfterErasure t = new TeamAfterErasure();

    Student[] students = (Student[]) team.asArray(new Student[2]);
    for (Student student : students) {
      System.out.println("StudentArray: " + student.getName());
    }

    // Erasure
    Class teamClass = TeamAfterErasure.class;
    Method method = teamClass.getMethod("getPerson", int.class);
    System.out.println("Return Type : " + method.getReturnType());
  }
}
