package com.psybergate.grad2021.core.generics;

public class Utils {

  public static void main(String[] args) {
   Team team1 = new Team();
   team1.add(new Team<Person>());
   team1.add(new Team<Student>());
   team1.add(new Team<String>());

    System.out.println();
  }


}
