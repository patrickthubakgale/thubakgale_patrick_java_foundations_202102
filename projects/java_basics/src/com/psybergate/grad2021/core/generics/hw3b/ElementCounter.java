package com.psybergate.grad2021.core.generics.hw3b;

import java.util.Collection;

public class ElementCounter {

  public static <T> int countElements(Collection<T> elements, Condition<T> condition) {
    int count = 0;
    for (T element : elements) {
      if (condition.isSatisfiedBy(element)) {
        ++count;
      }
    }
    return count;
  }
}
