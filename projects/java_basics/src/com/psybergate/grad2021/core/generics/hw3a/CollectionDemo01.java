package com.psybergate.grad2021.core.generics.hw3a;

import java.util.Arrays;
import java.util.Collection;

public class CollectionDemo01 {

  public static void main(String[] args) {
    Collection<Integer> coll = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
    System.out.println("Number of even numbers in the collection " + evenNumbers(coll));
    System.out.println("Number of odd numbers in the collection " + oddNumbers(coll));
    System.out.println("Number of prime numbers in the collection " + primeNumbers(coll));
    //#################
    Collection<String> coll2 = Arrays.asList("common", "jive", "community", "counsel", "barbaric"
            , "goals", "camel");
    Collection<Account> coll3 = Arrays.asList(new Account("Jeremy", -2300),
            new Account("Suhail", 2400),
            new Account("Joseph", -1000),
            new Account("Stanley", 800),
            new Account("James", 500),
            new Account("Patrick", -200));
    System.out.println("Number of words starting with letter c is " + numberOfWordsStartWithC(coll2));
    System.out.println("Number of accounts with negative balance is " + numberOfAccountsWithNegativeBal(coll3));
  }

  public static int evenNumbers(Collection<Integer> collection) {
    int count = 0;
    for (Integer integer : collection) {
      if (integer % 2 == 0) ++count;
    }
    return count;
  }

  public static int oddNumbers(Collection<Integer> collection) {
    int count = 0;
    for (Integer integer : collection) {
      if (integer % 2 != 0) ++count;
    }
    return count;
  }

  public static int primeNumbers(Collection<Integer> collection) {
    int count = 0;
    for (Integer integer : collection) {
      if (isPrime(integer)) ++count;
    }
    return count;
  }

  private static boolean isPrime(int i) {
    if (i <= 3) return i > 1;
    if (i % 2 == 0 || i % 3 == 0) return false;
    int num = 5;
    while (Math.pow(num,2) <= i) {
      if (i % num == 0 || i % (num + 2) == 0) {
        return false;
      }
      num += 6;
    }
    return true;
  }

  public static int numberOfWordsStartWithC(Collection<String> collection) {
    int count = 0;
    for (String string : collection) {
      if (string.charAt(0) == 'c') ++count;
    }
    return count;
  }

  public static int numberOfAccountsWithNegativeBal(Collection<Account> collection) {
    int count = 0;
    for (Account account : collection) {
      if (account.getBalance() < 0) ++count;
    }
    return count;
  }


}
