package com.psybergate.grad2021.core.generics.hw4a;

public class WebExample {

  public static <T extends Comparable<T>> T maximum(T x, T y, T z) {
    T max = x;

    if (y.compareTo(max) > 0) max = y;
    if (z.compareTo(max) > 0) max = z;
    return max;
  }

  public static void main(String[] args) {
    System.out.println(maximum(8, 3, 9));
    System.out.println(maximum(8.7, 6.3, 4.9));
    System.out.println(maximum("Darius", "Tom", "James"));
  }
}
