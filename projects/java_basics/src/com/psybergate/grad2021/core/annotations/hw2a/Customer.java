package com.psybergate.grad2021.core.annotations.hw2a;

@DomainClass
public class Customer {

  @DomainProperty(primaryKey = true, unique = true)
  private String customerNum;

  @DomainProperty
  private String name;

  @DomainProperty
  private String surname;

  @DomainProperty
  private Integer dateOfBirth;

  @DomainTransient
  private int age;

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public void setCustomerNum(String customerNum) {
    this.customerNum = customerNum;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public int getAge() {
    return age;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  @Override public String toString() {
    return "Customer{" +
            "customerNum='" + customerNum + '\'' +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", dateOfBirth=" + dateOfBirth +
            ", age=" + age +
            '}';
  }
}
