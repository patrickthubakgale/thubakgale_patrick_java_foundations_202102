package com.psybergate.grad2021.core.annotations.hw1a;

public class Customer {

  private String customerNum;

  private String name;

  private String surname;

  private Integer dateOfBirth;

  private int age;

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setCustomerNum(String customerNum) {
    this.customerNum = customerNum;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public int getAge() {
    return age;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  @Override public String toString() {
    return "Customer{" +
            "customerNum='" + customerNum + '\'' +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", dateOfBirth=" + dateOfBirth +
            ", age=" + age +
            '}';
  }
}