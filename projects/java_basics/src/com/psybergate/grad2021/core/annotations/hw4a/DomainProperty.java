package com.psybergate.grad2021.core.annotations.hw4a;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface DomainProperty {

//  String name

  boolean primaryKey() default false;

  boolean unique() default false;
}
