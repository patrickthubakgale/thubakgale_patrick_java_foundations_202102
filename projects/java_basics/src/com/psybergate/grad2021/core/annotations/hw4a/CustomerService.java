package com.psybergate.grad2021.core.annotations.hw4a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CustomerService{

  public void saveCustomer() {
    try {
      Class.forName("org.postgresql.Driver");
      Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "admin");
      Statement statement = conn.createStatement();
      statement.execute(insertSql());
      statement.close();
      conn.close();
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  private String insertSql() {
    Customer customer = new Customer("123", "Daniil", "Dubov", 19971130);
    String sql = "INSERT INTO CUSTOMER (";
    String values = " VALUES (";

    int i = 0;
    Field[] fields = Customer.class.getDeclaredFields();
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sql += field.getName();
          values += getValue(customer, field.getName());
          break;
        }
      }
      ++i;
      if (i < fields.length - 1) {
        sql += ", ";
        values += ", ";
      }
    }
    sql += ")";
    values += ")";
    return sql + values;
  }

  public Customer getCustomer(String customerNum) {
    List<Object> objList = new ArrayList<>();
    try {
      Class.forName("org.postgresql.Driver");
      Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "admin");
      Statement statement = conn.createStatement();

      ResultSet resultSet = statement.executeQuery("SELECT * FROM CUSTOMER;");
      while (resultSet.next()) {
        if (customerNum.equals(resultSet.getString("customerNum"))) {
          for (String[] field : getFields()) {
            switch (field[1]) {
              case "String":
                objList.add(resultSet.getString(field[0]));
                break;
              case "Integer":
                objList.add(resultSet.getInt(field[0]));
                break;
            }
          }
        }
      }
      resultSet.close();
      statement.close();
      conn.close();
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    Customer customer = createCustomer(objList);
    return customer;
  }

  private Customer createCustomer(List<Object> objList) {
    Customer customer = new Customer((String) objList.get(0), (String) objList.get(1),
            (String) objList.get(2), (Integer) objList.get(3));
    return customer;
  }

  private List<String[]> getFields() {
    List<String[]> fields = new ArrayList<>();
    Field[] customerFields = Customer.class.getDeclaredFields();
    for (Field customerField : customerFields) {
      Annotation[] annotations = customerField.getAnnotations();
      for (Annotation annotation : annotations) {
        String[] field = new String[2];
        field[0] = customerField.getName();
        field[1] = customerField.getType().getSimpleName();
        fields.add(field);
        break;
      }
    }
    return fields;
  }

  private String getValue(Customer customer, String fieldName) {
    switch (fieldName) {
      case "customerNum":
        return "'" + customer.getCustomerNum() + "'";
      case "name":
        return "'" + customer.getName() + "'";
      case "surname":
        return "'" + customer.getSurname() + "'";
      case "dateOfBirth":
        return "'" + customer.getDateOfBirth() + "'";
      default:
        return "Unknown value";
    }
  }
}
