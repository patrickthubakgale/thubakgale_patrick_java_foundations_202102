package com.psybergate.grad2021.core.annotations.hw4a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager {

  public static final String JDBC_DRIVER = "org.postgresql.Driver";

  public static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

  public static final String DB_USER = "postgres";

  public static final String DB_PASSWORD = "admin";

  public void generateDatabase() {
    try {
      Class.forName(JDBC_DRIVER);
      Connection conn = DriverManager.getConnection(DB_URL,
              DB_USER, DB_PASSWORD);
      Statement statement = conn.createStatement();
      statement.execute(dropSql());
      statement.execute(createSql());
      statement.close();
      conn.close();
    } catch (Throwable e) { //propagate the exception
      throw new RuntimeException(e);
    }
  }

  private String dropSql() {
    return "DROP TABLE IF EXISTS CUSTOMER";
  }

  private String createSql() {
    Field[] fields = getClassFields();
    String sql = "CREATE TABLE CUSTOMER (";

    int i = 0;
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sql += field.getName() + " " + getType(field.getType().getSimpleName()) + " NOT NULL";
          break;
        }
      }
      ++i;
      if (i < fields.length - 1) {
        sql += ", ";
      }
    }
    sql += ")";
    return sql;
  }

  private Field[] getClassFields() {
    Field[] fields = null;
    Annotation[] annons = Customer.class.getAnnotations();
    for (Annotation annon : annons) {
      if (annon instanceof DomainClass) {
        fields = Customer.class.getDeclaredFields();
        break;
      }
    }
    return fields;
  }

  private String getType(String name) {
    switch (name) {
      case "int":
      case "Integer":
        return "INT";
      case "String":
        return "TEXT";
      default:
        return "Type does not exist";
    }
  }
}