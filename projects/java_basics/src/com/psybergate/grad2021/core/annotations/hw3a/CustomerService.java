package com.psybergate.grad2021.core.annotations.hw3a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class CustomerService{

  public String saveCustomer() {
    Customer customer = new Customer("123", "Magnus", "Carlsen", 19801212);
    String sql = "INSERT INTO CUSTOMER (";
    String values = " VALUES (";

    int i = 0;
    Field[] fields = Customer.class.getDeclaredFields();
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sql += field.getName();
          values += getValue(customer, field.getName());
          break;
        }
      }
      ++i;
      if (i < fields.length - 1) {
        sql += ", ";
        values += ", ";
      }
    }
    sql += ")";
    values += ")";

    return sql + values;
  }

  private String getValue(Customer customer, String fieldName) {
    switch (fieldName) {
      case "customerNum":
        return "'" + customer.getCustomerNum() + "'";
      case "name":
        return "'" + customer.getName() + "'";
      case "surname":
        return "'" + customer.getSurname() + "'";
      case "dateOfBirth":
        return "'" + customer.getDateOfBirth() + "'";
      default:
        return "Unknown value";
    }
  }
}
