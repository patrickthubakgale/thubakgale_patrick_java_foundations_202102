package com.psybergate.grad2021.core.annotations.hw4a;

public class Utils {

  public static void main(String[] args) {
    DatabaseManager db = new DatabaseManager();
    db.generateDatabase();

    CustomerService service = new CustomerService();
    service.saveCustomer();
    Customer customer = service.getCustomer("123");
    print(customer);
    //set the age
    customer.setAge(24);
    print(customer);
  }

  public static void print(Customer customer) {
    System.out.println(customer.toString());
  }
}
