package com.psybergate.grad2021.core.annotations.hw3a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseManager {

  public void generateDatabase() {
    try {
      Class.forName("org.postgresql.Driver");
      Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                      "postgres", "admin");
      Statement statement = conn.createStatement();
      statement.execute(createSql());
      statement.close();
      conn.close();
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  public String createSql() {
    Field[] fields = Customer.class.getDeclaredFields();
    String sql = "CREATE TABLE CUSTOMER (";

    int i = 0;
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sql += field.getName() + " " + getType(field.getType().getSimpleName()) + " NOT NULL";
          break;
        }
      }
      ++i;
      if (i < fields.length-1) {
        sql += ", ";
      }
    }
    sql += ")";
    return sql;
  }

  private String getType(String name) {
    switch (name) {
      case "int":
      case "Integer":
        return "INT";
      case "String":
        return "TEXT";
      default:
        return "Type does not exist";
    }
  }
}