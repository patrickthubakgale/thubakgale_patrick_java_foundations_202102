package com.psybergate.grad2021.core.reflection.hw2a;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;

public class DynamicLoader {

  private static final Properties PROPERTIES = new Properties();

  public static void loadStream() throws IOException {
    InputStream stream = Utils.class.getClassLoader().getResourceAsStream("com/psybergate" +
            "/grad2021/core/reflection/hw2a/properties.txt");
    PROPERTIES.load(stream);
  }

  public static Class getClass(String list) throws ClassNotFoundException {
    return Class.forName(PROPERTIES.getProperty(list));
  }

  public static List<String> months(Class clazz)
          throws IllegalAccessException, InstantiationException, NoSuchMethodException,
          InvocationTargetException {
    List<String> list = (List<String>) clazz.newInstance();
    Method add = clazz.getMethod("add", Object.class);
    add.invoke(list, "January");
    add.invoke(list, "February");
    add.invoke(list, "March");
    add.invoke(list, "April");
    add.invoke(list, "May");
    add.invoke(list, "June");
    add.invoke(list, "July");
    add.invoke(list, "August");
    add.invoke(list, "September");
    add.invoke(list, "October");
    add.invoke(list, "November");
    add.invoke(list, "December");
    return list;
  }
}
