package com.psybergate.grad2021.core.reflection.hw3a;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;

public class InvokeDynamic {

  public static void main(String[] args) throws Throwable {
    MethodHandles.Lookup lookup = MethodHandles.lookup();

    Point point = new Point();
    //set fields
    MethodHandle mh = lookup.findSetter(Point.class, "x", int.class);
    mh.invoke(point, 23);
    mh = lookup.findSetter(Point.class, "y", int.class);
    mh.invoke(point, 10);

    mh = lookup.findGetter(Point.class, "x", int.class);
    int x = (int) mh.invoke(point);
    System.out.println("x = " + x);
    //do the same for y
  }

}

class Point {

  int x;

  int y;
}