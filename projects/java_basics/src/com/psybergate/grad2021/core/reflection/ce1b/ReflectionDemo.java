package com.psybergate.grad2021.core.reflection.ce1b;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionDemo {

  public static void main(String[] args)
          throws IllegalAccessException, InstantiationException, NoSuchMethodException,
          InvocationTargetException {
    Class<Student> clazz = Student.class;
    Student student1 = clazz.newInstance(); //create an object using the default constructor
    Constructor<Student> constructor = clazz.getConstructor(String.class, String.class, int.class);
    Student student2 = constructor.newInstance("James", "Peters", 19);
    Method method1 = clazz.getMethod("getName");
    System.out.println("method1 = " + method1.invoke(student2));
  }
}