package com.psybergate.grad2021.core.reflection.hw3a_1;

import com.psybergate.grad2021.core.reflection.hw2a.DynamicLoader;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Utils {

  public static void main(String[] args)
          throws IOException, InvocationTargetException, NoSuchMethodException,
          InstantiationException, IllegalAccessException, ClassNotFoundException {
    com.psybergate.grad2021.core.reflection.hw2a.DynamicLoader.loadStream();
    Class clazz = com.psybergate.grad2021.core.reflection.hw2a.DynamicLoader.getClass("LinkedList");
    List<String> months = DynamicLoader.months(clazz);
    print(months);
  }

  public static void print(List<String> list) {
    int i = 0;
    for (String m : list) {
      System.out.println(++i + "." + m);
    }
  }
}
