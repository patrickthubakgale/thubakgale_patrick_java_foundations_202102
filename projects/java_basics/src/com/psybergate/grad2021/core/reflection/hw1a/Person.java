package com.psybergate.grad2021.core.reflection.hw1a;

public class Person {

  protected String name;

  protected String surname;

  public int age;

  public Person(String name, String surname, int age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
  }
}
