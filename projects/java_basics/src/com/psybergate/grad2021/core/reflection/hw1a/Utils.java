package com.psybergate.grad2021.core.reflection.hw1a;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Utils {

  public static void main(String[] args) {
    Class<Student> clazz = Student.class;
    System.out.println("clazz.getSuperclass().getName() = " + clazz.getSuperclass().getName());

    System.out.println("Modifier.toString(clazz.getModifiers()) = " +
            Modifier.toString(clazz.getModifiers()));
    System.out.println("Modifier.toString(clazz.getSuperclass().getModifiers()) = " +
            Modifier.toString(clazz.getSuperclass().getModifiers()));

    Method[] methods = clazz.getDeclaredMethods();
    for (Method method : methods) {
      System.out.println("\nMethod name = " + method.getName() + "\nModifier.toString(method" +
              ".getModifiers()) " +
              "= " +
              Modifier.toString(method.getModifiers()));
      System.out.println("method.getReturnType() = " + method.getReturnType());
    }

    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      System.out.println("\nField name = " + field.getName() + "\nModifier.toString(field" +
              ".getModifiers()) = " +
              Modifier.toString(field.getModifiers()));
    }
    //parent fields
    Field[] parentFields = clazz.getSuperclass().getDeclaredFields();
    for (Field parentField : parentFields) {
      System.out.println("\nField name = " + parentField.getName() + "\nModifier.toString(field" +
              ".getModifiers()) = " +
              Modifier.toString(parentField.getModifiers()));
    }
  }
}
