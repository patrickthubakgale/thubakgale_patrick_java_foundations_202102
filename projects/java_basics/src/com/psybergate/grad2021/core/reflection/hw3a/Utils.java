package com.psybergate.grad2021.core.reflection.hw3a;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

public class Utils {

  public static void main(String[] args) throws Throwable {
    MethodHandles.Lookup lookup = MethodHandles.lookup();
    Student student1 = new Student();

    MethodHandle methodHandle = lookup.findSetter(Student.class, "name", String.class);
    methodHandle.invoke(student1, "James");

    methodHandle = lookup.findGetter(Student.class, "name", String.class);
    String name = (String) methodHandle.invoke(student1);
    System.out.println("name = " + name);

    methodHandle = lookup.findSetter(Student.class, "age", int.class);
    methodHandle.invoke(student1, 45);

    methodHandle = lookup.findGetter(Student.class, "age", int.class);
    int age = (int) methodHandle.invoke(student1);
    System.out.println("age = " + age);

    methodHandle =
            lookup.findVirtual(Student.class, "getName", MethodType.methodType(String.class));
    System.out.println("methodHandle.invoke(student1) = " + methodHandle.invoke(student1));
  }
}
