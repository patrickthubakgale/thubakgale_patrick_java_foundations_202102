package com.psybergate.grad2021.core.reflection.ce1a;

import com.psybergate.grad2021.core.annotations.hw4a.DomainClass;

import java.io.Serializable;

@DomainClass(name = "Student") //redundant but works fine for this demo-remove after usage
public class Student implements Serializable {

  private String name;

  private String surname;

  public int age;

  private String faculty;

  public String course;

  public Student() {
  }

  public Student(String name, String surname, int age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.faculty = "Engineering and Built Environment";
    this.course = "Electrical and Information Engineering";
  }

  public Student(String name, String surname, int age, String faculty, String course) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.faculty = faculty;
    this.course = course;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public int getAge() {
    return age;
  }

  public String getFaculty() {
    return faculty;
  }

  public String getCourse() {
    return course;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAge(int age) {
    this.age = age;
  }
}
