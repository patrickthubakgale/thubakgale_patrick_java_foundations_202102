package com.psybergate.grad2021.core.reflection.hw1a;

import com.psybergate.grad2021.core.annotations.hw4a.DomainClass;

import java.io.Serializable;

@DomainClass(name = "Student") //redundant but works fine for this demo-remove after usage
public class Student extends Person implements Serializable {

  private String faculty;

  public String course;

  public static String randomName;

  public Student() {
    super("Mafia", "Nyokas", 23);
    System.out.println("In default constructor");
  }

  public Student(String name, String surname, int age) {
    super(name, surname, age);
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.faculty = "Engineering and Built Environment";
    this.course = "Electrical and Information Engineering";
    System.out.println("Constructor with three parameters");
  }

  public Student(String name, String surname, int age, String faculty, String course) {
    super(name, surname, age);
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.faculty = faculty;
    this.course = course;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  protected int getAge() {
    doSomethingPowerful();
    return age;
  }

  public String getFaculty() {
    return faculty;
  }

  public String getCourse() {
    return course;
  }

  public void setName(String name) {
    this.name = name;
  }

  protected void setAge(int age) {
    this.age = age;
  }

  private void doSomethingPowerful() {
    System.out.println("Nothing powerful here!");
  }
}
