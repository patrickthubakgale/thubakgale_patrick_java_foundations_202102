package com.psybergate.grad2021.core.reflection.ce1a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ReflectionDemo {

  public static void main(String[] args) {
    Class clazz = Student.class;
    System.out.println("clazz.getSimpleName() = " + clazz.getSimpleName());
    System.out.println("clazz.getName() = " + clazz.getName());
    System.out.println("clazz.getPackage() = " + clazz.getPackage());

    Annotation[] annotations = clazz.getAnnotations();
    for (Annotation annotation : annotations) {
      System.out.println("annotation = " + annotation);
    }

    Class[] interfaces = clazz.getInterfaces();
    for (Class anInterface : interfaces) {
      System.out.println("anInterface.getName() = " + anInterface.getName());
    }

    Field[] publicFields = clazz.getFields();
    for (Field publicField : publicFields) {
      System.out.println("publicField.getName() = " + publicField.getName());
    }

    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
//      if (Modifier.isPublic(field.getModifiers())) {
//        System.out.println("Public field is: " + field.getName());
//        continue;
//      }
      System.out.println("field = " + field.getName());
    }

    Constructor[] constructors = clazz.getDeclaredConstructors();
    for (Constructor constructor : constructors) {
      for (Object parameter : constructor.getParameters()) {
        System.out.println("parameter = " + parameter);
      }
      System.out.println();
    }

    Method[] methods = clazz.getDeclaredMethods();
    for (Method method : methods) {
      System.out.println("method = " + method);
    }

    //#######################################
    System.out.println("\nclazz.getSuperclass().getName() = " + clazz.getSuperclass().getName());
    System.out.println("clazz.getModifiers() = " + clazz.getModifiers());
    System.out.println(
            "Modifier.toString(clazz.getModifiers()) = " + Modifier.toString(clazz.getModifiers()));
    System.out.println();
    for (Field field : fields) {
      System.out.println("Field name = " + field.getName() + " -> Modifier.toString(field" +
              ".getModifiers()) = " +
              Modifier.toString(field.getModifiers()) + "\n");
    }

    for (Method method : methods) {
      System.out.println("Method name = " + method.getName() + "\nModifier.toString(Modifier" +
              ".methodModifiers" +
              "()) = " +
              Modifier.toString(Modifier.methodModifiers()));
      System.out.println("method.getReturnType() = " + method.getReturnType() + "\n");
    }
  }
}
