package com.psybergate.grad2021.core.langoverview.hw1a;

//hit shift key twice or Ctrl + N to search for java classes source code

public class Explore {
  public static void main(String[] args) {
    //explore size of byte (min and max)
    //8 bits, 256 numbers
    byte mybyte1 = 127; // max number
    byte mybyte2 = -128; // min number

    //short = 2 bytes which is 2^16 = 65536
    short myshort1 = 32_767; // max number -> (2^15)-1
    short myshort2 = -32_768; //min number

    //int = 4 bytes which is 2^32 = 4294967296
    int myint1 = 2_147_483_647; // max number -> (2^31)-1
    int myint2 = -2_147_483_648;

    //long = 8 bytes
    long mylong1 = 9_223_372_036_854_775_807L; //max number
    long mylong2 = -9_223_372_036_854_775_808L; //min number

    System.out.println("All is done!");
  }
}
/*
Getting error message: java: class Second is public, should be declared in a file named Second.java

public class Second {
    public static String getMessage() {
        return String.format("Your name is nonsense");
    }
}
*/

/*
Notes:
Primitive types are the most basic data types available within the Java language.
They are boolean , byte , char , short , int , long , float and double.
These types serve as the building blocks of data manipulation in Java.

Data Type	Size	Stores
byte		1 byte	whole numbers from -128 to 127
short		2 bytes	whole numbers from -32,768 to 32,767
int			4 bytes	whole numbers from -2,147,483,648 to 2,147,483,647
long		8 bytes	whole numbers from -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
float		4 bytes	fractional numbers; sufficient for storing 6 to 7 decimal digits
double		8 bytes	fractional numbers; sufficient for storing 15 decimal digits
boolean		1 bit	true or false values
char		2 bytes	single character/letter or ASCII values
 */