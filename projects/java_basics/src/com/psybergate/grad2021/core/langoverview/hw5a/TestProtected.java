package com.psybergate.grad2021.core.langoverview.hw5a;

public class TestProtected extends AccessModifiers{
  public static void main(String[] args) {
    TestProtected obj1 = new TestProtected();
    obj1.acc = "123456"; //protected member variable accessible here
    obj1.setName("Darius"); //protected methods accessible

    //obj1.messageMe(); //private method not accessible within this class
    System.out.println("Name is "+ obj1.getName());
  }
}