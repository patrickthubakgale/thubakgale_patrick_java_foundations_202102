package com.psybergate.grad2021.core.langoverview.hw2a;

public class DemoStatic {
  //static variables (class variables)
  private static int num;
  private static String name;
  private String username;

  //static variables and static initializers get executed during class loading

  //static initializer
  static {
    name = "Steve";
    num = 92;
    System.out.printf("Num is: %d%nName is: %s%n", num, name);
  }

  //instance initializers
  static {
    DemoStatic temp1 = new DemoStatic();
    temp1.setUserName("Robert");
    System.out.printf("New object created with name: %s%n", temp1.getUserName());
  }

  //Divide by Zero would do
  static {
    int num1 = 45;
    int num2 = 0;
    try {
      int sum = num1/num2;
      System.out.printf("Sum of two numbers is $d: ", sum);
    } catch (Exception e) {
      System.out.println("You cannot do that bro! Don't do it again!!!");
      throw new RuntimeException(e.getMessage());
    }
  }

  public static void main(String[] args) {
    System.out.println("Static initializers at work!");
  }

  public void setUserName(String username) {
    this.username = username;
  }

  public String getUserName() {
    return username;
  }
}
