package com.psybergate.grad2021.core.langoverview.anotherpackage;

import com.psybergate.grad2021.core.langoverview.hw5a.AccessModifiers;

public class TestPackage {
  public static void main(String[] args) {
    AccessModifiers obj1 = new AccessModifiers();
    obj1.sayHi();
  }
}
