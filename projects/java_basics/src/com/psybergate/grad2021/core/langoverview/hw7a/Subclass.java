package com.psybergate.grad2021.core.langoverview.hw7a;

public class Subclass extends FinalModifier {

  public Subclass(double e) {
    super(e);
  }

//  @Override
//  public String getName() { //Cannot override final method
//    return String.format("Your Awesome name is: "+ getName());
//  }

//  public static final String newMessage() {
//    return "You got it";
//  }
}
