package com.psybergate.grad2021.core.langoverview.hw4a;

public class Operators {
  private String name;
  private double salary;

  public Operators(String name, double salary) {
    this.name = name;
    this.salary = salary;
  }

  public static void main(String[] args) {
    //mod operator
    int j = 10;
    int k = 2;
    System.out.printf("Mod test%n%d %% %d = %d%nEnd mod test%n%n",j, k, j%k);

    //post and pre increment
    System.out.printf("Post and Pre increment test%nValue of j = %d%n", j);
    j++;
    System.out.printf("Value of j after incrementing by 1: %d%n", j);
    System.out.printf("Value of j after ++j increment: %d%n", ++j);
    System.out.printf("Value of j: %d%n", j);
    System.out.printf("Value of j after j++ increment: %d%n", j++);
    System.out.printf("Final value of j: %d%n%n", j);
    System.out.println("End increment test");

    //Equality operator (Primitive types)
    String temp = "job";
    String temp2 = temp;
    String temp3 = new String("Hello"); //however this is an object now
    int num = 0;
    int num2 = 1;
    if(temp == temp2) {
      System.out.printf("%s == %s%n", temp, temp2);
    } else {
      System.out.printf("%s != %%ns", temp, temp2);
    }

    if (temp2 == temp3) {
      System.out.println("Strings are the same");
    } else {
      System.out.println("Strings not the same");
    }
    System.out.printf("%nObject references now:%n%n");
    //Equality operator (Object reference)
    Operators obj1 = new Operators("Jack", 4000);
    Operators obj2 = obj1;
    Operators obj3 = new Operators("Naruto", 4000);

    if(obj1 == obj2) {
      System.out.println("Two object references reference same object");
    }else {
      System.out.println("Two object references reference different objects");
    }
    if(obj1 == obj3) {
      System.out.println("Two object references (with same contents) reference same object");
    }else {
      System.out.println("Two object references (with same contents) reference different object");
    }

    /*Ampersand Stuff
      When using && (double ampersand operators), if first operand fails
      the program does not check the second operand (only evaluates left operand
      and only evaluates second operand if first is true). Cond1 && Cond2 returns
      true only if both conditions are true. Operates on boolean operands.
      This is logical AND!
      In case of single ampersand operator &(bitwise AND), it evaluates
      both side of the operation.
     */

    if (obj1.getSalary() == 4000 && obj1.getName() == "Jack") {
      System.out.println("Both conditions are true");
    } else {
      System.out.println("One or both conditions failed(false)");
    }

    if (obj2.getSalary() == 4000 & obj2.doubleSalary() == 8000) {
      System.out.println("Both conditions evaluated and are both true");
    } else {
      System.out.println("One of the conditions or both failed (false)");
    }
    /*
    Logical OR (||) evaluates to true if one of the operand is true (it
    does not have to check both). It only does if the first operand was false.
    Prefer to short-circuit the evaluation and choose || instead.

    Bitwise OR (|) evaluates both operands
    The rule:
    a | b -> Evaluates b even if a is false.
    a || b -> Evaluates b only if a is false

    a & b -> Evaluates b even if a is false
    a && b -? Evaluate b only if a is true. Otherwise, fails.
     */

    boolean flag = false;
    if (flag = true || obj1.getSalary() == 4000) {
      System.out.println("Right operand is true.");
    } else {
      System.out.println("All false.");
    }
    //money variable should be updated to show tha obj1.getMoney() got executed

    //+= Stuff
    int number = 4;
    System.out.printf("The number before update is: %d%n", number);
    number += 4;
    System.out.printf("The number after (number += 4) update is: %d%n", number);
    String name = "hello";
    name += 9;
    System.out.println("after string concatenation name is: "+name);

    //Ternary operator stuff
    System.out.println("Ternary operator stuff");
    //if number if greater than 9 return 10, else return 5.
    System.out.println(number > 9 ? 10 : 5);

    //switch statement
    char op = '-';
    int first = 30;
    int second = 15;
    int result = 0;

    switch (op) {
      case '+':
        result = second + first;
        break;
      case '-':
        result = second - first;
        break;
      default:
        System.out.println("Unrecognized operator!");
    }
    System.out.println("Value of result after computation: "+ result);
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }

  public double getSalary() {
    return salary;
  }

  public String getName() {
    return name;
  }

  public double doubleSalary() {
    return 2*getSalary();
  }
}
