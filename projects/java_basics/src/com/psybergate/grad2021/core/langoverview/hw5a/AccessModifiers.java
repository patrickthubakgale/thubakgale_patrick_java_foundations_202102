package com.psybergate.grad2021.core.langoverview.hw5a;

public class AccessModifiers {
  private String name;
  protected String acc;

  public static void main(String[] args) {
    AccessModifiers obj1 = new AccessModifiers();
    obj1.setName("John");
    System.out.println(obj1.getName());
    obj1.messageMe();
  }

  protected void setName(String name) {
    this.name = name;
    System.out.println("Set name method has been invoked by: " + this.name);
  }

  public String getName() { //make public
    return name;
  }

  protected String getAcc() {
    return acc;
  }

  private void messageMe() {
    System.out.println("Today's message is about Rugby player Pieter-Steph Du Toit.");
  }

  public void sayHi() {
    System.out.println("Hello");
  }
}