package com.psybergate.grad2021.core.langoverview.hw6a;

//Cannot have static class -> Concept of static does not exist at that level

public class StaticModifier {
  private static int first; //static variables allowed
  private String name;

  //  public static StaticModifier(String name) { //cannot have static constructor
//    this.name = name;
//  }
  public StaticModifier(String name) {
    this.name = name;
  }

  public static void main(String[] args) {

  }

  public static void StaticVariable(int newVar) { //static methods allowed
    first = newVar;
  }

  /*
  The concept of static variable is that it does not belong to a specific instance of a class,
  but to the whole class. When you are declaring a variable inside a method, it effectively
  becomes a local variable, they do not have any existence outside of that method. So it makes
  no sense really to declare a static variable inside a method.

  JVM allocates memory to static variables when the class is loaded, not when the object is created.
  When you declare a static variable inside a method, it comes under the method’s scope, and JVM
  is unable to allocate memory for it.
   */
}
