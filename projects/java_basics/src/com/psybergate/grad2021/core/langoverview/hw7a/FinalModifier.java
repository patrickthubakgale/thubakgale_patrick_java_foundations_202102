package com.psybergate.grad2021.core.langoverview.hw7a;

//A final class cannot be overridden

public class FinalModifier {
  private final double PI = 4*Math.atan(1); //variable is a constant, cannot be changed
  private static String name;
  private int age;
  private final double e; //this variable can be assigned in the constructor
  private String account;
  public static final int VALUE = 9; //accessible outside this class but cannot be modified

  public FinalModifier(double e) { //Constructor assign value to declared private variable
    this.e = 2.7; //this not applicable in methods. A constant can only be assigned during object construction
  }

  /*
  Note to self: putting final modifier in a method prevents method from being overridden. When the child class
  inherits from the parent class, all members are inherited except the constructor hence there is no need to
  make the constructor final. (It cannot be overridden if it cannot be inherited)->simply put!
   */
//  public final FinalModifier() {
//    System.out.println("I like creating silly objects");
//  }

  public static void main(String[] args) {

  }

  public final String getName() { //method cannot be overridden
    return name;
  }

  public final void setName(String name) { //method cannot be overridden by subclass
    this.name = name;
  }

//  public void newPI(double pi) { //Cannot assign value to final variable
//    PI = pi;
//  }

  public static void updateName() {
    if (name.length() < VALUE) {
      name = "Tim";
    }
  }

  public void setAccount(final String account) { //function cannot modify the parameter value, can only use it.
    this.account = account;
  }

  public static final String newMessage() {
    return "You got it";
  }
}