package com.psybergate.grad2021.core.innerclasses.hw1a;

public interface AnonymousInnerClass {

  public int run();
}

class AnnonClass {

  public static int callRun(AnonymousInnerClass annon) {
    return annon.run();
  }

  public static void main(String[] args) {
    callRun(new AnonymousInnerClass() {
      @Override public int run() {
        return 5 * 5;
      }
    });
  }
}

