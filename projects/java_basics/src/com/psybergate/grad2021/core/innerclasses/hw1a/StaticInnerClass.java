package com.psybergate.grad2021.core.innerclasses.hw1a;

public class StaticInnerClass {

  private static String string;

  private static void someMethod01() {
    System.out.println("String is: " + string);
  }

  public static void someMethod02() {
    StaticNested.doSomething01();
  }

  public static class StaticNested {
    public static void doSomething01() {
      string = "I was called from StaticNested by someMethod01";
      someMethod01();
    }

    public void doSomething02() {

      someMethod02();
    }
  }

  public static void main(String[] args) {
    StaticInnerClass.StaticNested.doSomething01();
    StaticInnerClass.StaticNested doSomething = new StaticInnerClass.StaticNested();
    doSomething.doSomething02();
  }

}
