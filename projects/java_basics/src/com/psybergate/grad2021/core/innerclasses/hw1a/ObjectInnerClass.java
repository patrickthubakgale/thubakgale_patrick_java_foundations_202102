package com.psybergate.grad2021.core.innerclasses.hw1a;

public class ObjectInnerClass {

  private final static int SIZE = 15;

  private int[] arrayOfInts = new int[SIZE];

  public ObjectInnerClass() {
    for (int i = 0; i < SIZE; i++) {
      arrayOfInts[i] = i;
    }
  }

  public void printEvenNumbers() {
    //print
  }

  private class InnerEvenIterator {

    private int next = 0;

    public boolean hasNext() {
      return (next <= SIZE - 1);
    }

    public int getNext() {
      int retValue = arrayOfInts[next];
      next += 2;
      return retValue;
    }
  }

  public static void main(String[] args) {
    ObjectInnerClass obj = new ObjectInnerClass();
    InnerEvenIterator iterator = obj.new InnerEvenIterator();
    while (iterator.hasNext()) {
      System.out.println(iterator.getNext() + " ");
    }
  }

}
