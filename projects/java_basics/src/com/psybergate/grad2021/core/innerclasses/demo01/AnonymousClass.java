package com.psybergate.grad2021.core.innerclasses.demo01;

public class AnonymousClass {

  interface HelloWorld {

    public void greet();

    public void greetSomeone(String someone);
  }

  public void sayHello() {

    class EnglishGreeting implements HelloWorld {

      String name = "world";
      @Override public void greet() {
        greetSomeone("world");
      }

      @Override public void greetSomeone(String someone) {
        name = someone;
        System.out.println("Hello " + name);
      }
    }
    //!=
    HelloWorld greeting1 = new EnglishGreeting();

    HelloWorld greeting2 = new HelloWorld() {
      String name = "tout le monde";
      @Override public void greet() {
        greetSomeone("tout le monde");
      }

      @Override public void greetSomeone(String someone) {
        name = someone;
        System.out.println("Salut " + name);
      }
    };

    HelloWorld greeting3 = new HelloWorld() {
      String name = "mundo";
      @Override public void greet() {
        greetSomeone("mundo");
      }

      @Override public void greetSomeone(String someone) {
        name = someone;
        System.out.println("Hola " + name);
      }
    };

    greeting1.greet();
    greeting2.greet();
    greeting3.greet();
  }

  public static void main(String[] args) {
    AnonymousClass clazz = new AnonymousClass();
    clazz.sayHello();
  }
}
