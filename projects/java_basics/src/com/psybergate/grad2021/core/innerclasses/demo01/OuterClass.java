package com.psybergate.grad2021.core.innerclasses.demo01;

public class OuterClass {
  private int x = 10;

  class InnerClass {//does not work if this class is declared as private
    private int y = 5; //we could make this a method and still access it the same way
  }

  static class StaticInnerClass {
    int y = 5;
  }

  public static void main(String[] args) {
    OuterClass myOuter = new OuterClass();
    OuterClass.InnerClass myInner = myOuter.new InnerClass();
    System.out.println(myInner.y + myOuter.x);

    OuterClass.StaticInnerClass myStaticInner = new OuterClass.StaticInnerClass();
    System.out.println("myInner.y = " + myStaticInner.y);
  }
}
