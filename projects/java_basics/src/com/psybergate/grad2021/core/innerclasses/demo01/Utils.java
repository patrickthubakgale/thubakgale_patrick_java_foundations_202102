package com.psybergate.grad2021.core.innerclasses.demo01;

public class Utils {

  public static void main(String[] args) {
    OuterDemo outerDemo = new OuterDemo();
    outerDemo.displayInnerClass();

    OuterDemo.InnerDemo02 inner = outerDemo.new InnerDemo02();
    System.out.println("inner.getNum() = " + inner.getNum());

    outerDemo.method01();
  }
}
