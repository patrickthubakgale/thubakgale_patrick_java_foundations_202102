package com.psybergate.grad2021.core.innerclasses.demo01;

public class OuterDemo {
  private int num = 175;

  private class InnerDemo {//cannot be accessed from an object outside the class OuterDemo

    public void print() {
      System.out.println("This is an inner class");
    }
  }

  public class InnerDemo02 {
    public int getNum() {
      System.out.println("This is the getnum method of the inner class");
      return num;
    }
  }

  public void method01() {
    int num = 23;

    class MethodInnerDemo {
      public void print() {
        System.out.println("This is method innder class " + num);
      }
    }

    MethodInnerDemo inner = new MethodInnerDemo();
    inner.print();
  }

  public void displayInnerClass() {
    InnerDemo inner = new InnerDemo();
    inner.print();
  }
}
