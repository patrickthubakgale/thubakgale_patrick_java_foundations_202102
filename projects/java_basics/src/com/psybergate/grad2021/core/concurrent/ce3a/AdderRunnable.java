package com.psybergate.grad2021.core.concurrent.ce3a;

public class AdderRunnable implements Runnable {

  private Adder adder;

  private long sum;

  public AdderRunnable(Adder adder) {
    this.adder = adder;
  }

  @Override public void run() {
    sum = adder.sum();
  }

  public long getSum() {
    return sum;
  }
}