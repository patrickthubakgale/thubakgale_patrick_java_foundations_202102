package com.psybergate.grad2021.core.concurrent.hw1a_v2;

public class MessageWriter {

  public void write(String message) {
    System.out.println("Message written: " + message);
    messageCache().add(message);
  }

  public MessageCache messageCache() {
    return MessageCache.getInstance();
  }
}
