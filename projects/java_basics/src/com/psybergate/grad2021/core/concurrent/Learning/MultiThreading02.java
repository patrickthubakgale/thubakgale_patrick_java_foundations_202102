package com.psybergate.grad2021.core.concurrent.Learning;

public class MultiThreading02 implements Runnable {

  @Override
  public void run() {
    System.out.println("My thread is in running state");
  }

  public static void main(String[] args) {
    MultiThreading02 multi = new MultiThreading02();
    Thread thread = new Thread(multi);
    thread.start();
  }
}
