package com.psybergate.grad2021.core.concurrent.ce3a;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ThreadPool {

  private Map<Thread, Runnable> threads = new HashMap<>();

  public ThreadPool() {
  }

  public void add(Thread thread, Runnable runnable) {
    threads.put(thread, runnable);
  }

  public int size() {
    return threads.size();
  }

  public Set<Map.Entry<Thread, Runnable>> getThreads() {
    return threads.entrySet();
  }
}
