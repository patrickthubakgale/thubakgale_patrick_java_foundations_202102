package com.psybergate.grad2021.core.concurrent.hw1a;

public class ReaderRunnable implements Runnable {

  private MessageCache instance = MessageCache.getInstance();

  @Override
  public void run() {
    try {
      read();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void read() throws InterruptedException {
    synchronized (this) {
      System.out.println("sho");
      while (true) {
        try {
          wait();
          String message = MessageCache.getInstance().read();
          System.out.println("Message is: " + message);
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
          System.out.println("Thread interrupted " + e);
          throw new InterruptedException();
        }
      }
    }
    //instance.setWriting(true);
  }
}
