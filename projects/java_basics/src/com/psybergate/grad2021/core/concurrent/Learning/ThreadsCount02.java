package com.psybergate.grad2021.core.concurrent.Learning;

public class ThreadsCount02 implements Runnable {

  Thread thread;

  public ThreadsCount02() {
    thread = new Thread(this, "My runnable thread");
    System.out.println("My thread created " + thread);
    thread.start();
  }

  @Override
  public void run() {
    try{
      for (int i = 0; i < 10; i++) {
        System.out.println("Printing the count " + i);
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    System.out.println("My thread run is over");
  }
}

class ThreadsClient {

  public static void main(String[] args) {
    ThreadsCount02 count = new ThreadsCount02();
    try {
      while (count.thread.isAlive()) {
        System.out.println("Main thread will be alive till the child thread is alive");
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("Main thread run is over");
  }
}