package com.psybergate.grad2021.core.concurrent.onlineexample;

import java.util.concurrent.ThreadLocalRandom;

public class Receiver implements Runnable {

  private MessageCache cache;

  public Receiver(MessageCache cache) {
    this.cache = cache;
  }

  @Override
  public void run() {
    for (String message = cache.receive(); !"End".equals(message); message = cache.receive()) {
      System.out.println(message);
      try {
        Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        e.printStackTrace();
      }
    }
  }
}
