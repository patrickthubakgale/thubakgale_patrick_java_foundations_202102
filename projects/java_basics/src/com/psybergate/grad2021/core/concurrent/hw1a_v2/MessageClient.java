package com.psybergate.grad2021.core.concurrent.hw1a_v2;

public class MessageClient {

  public static void main(String[] args) {
    Thread writer = new Thread(new MessageWriterRunnable());
    Thread reader = new Thread(new MessageReaderRunnable());
    writer.start();
    reader.start();
  }

}
