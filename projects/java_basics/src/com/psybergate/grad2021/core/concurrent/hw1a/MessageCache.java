package com.psybergate.grad2021.core.concurrent.hw1a;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {

  public static final MessageCache INSTANCE = new MessageCache();

  private List<String> messages = new ArrayList<>();

  private boolean writing = true;

  private MessageCache() {
  }

  public synchronized void add(String message) {
    messages.add(message);
  }

  public synchronized String read() {
    if (messages.isEmpty()) {
      return null;
    }
    return messages.remove(0);
  }

  public synchronized void setWriting(boolean writing) {
    this.writing = writing;
  }

  public boolean getWritingStatus() {
    return writing;
  }

  public static MessageCache getInstance() {
    return INSTANCE;
  }
}