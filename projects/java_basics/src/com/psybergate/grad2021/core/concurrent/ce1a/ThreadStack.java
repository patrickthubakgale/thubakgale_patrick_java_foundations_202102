package com.psybergate.grad2021.core.concurrent.ce1a;

public class ThreadStack implements Runnable {

  private SomeClass01 someClass01;

  public ThreadStack(SomeClass01 someClass01) {
    this.someClass01 = someClass01;
  }

  @Override public void run() {
    someClass01.someMethod01();
  }
}

//Still lot to be done on this one!