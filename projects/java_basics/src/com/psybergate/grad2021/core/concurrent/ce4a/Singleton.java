package com.psybergate.grad2021.core.concurrent.hw4a;

public class Singleton {

  public static Singleton instance = null;

  private Singleton() {
  }

  public static Singleton getInstance() {
    synchronized (Singleton.class) {
      if (instance == null) {
        instance = new Singleton();
      }
    }
    return instance;
  }

  public static void doSomething() {
    synchronized (Singleton.class) {
      System.out.println("I am doing something, don't ask what!");
      for (int i = 0; i < 5; i++) {
        System.out.println("I can...");
      }
      System.out.println("No, you can't!");
    }
  }

  public static void doNothing() {
    synchronized (Singleton.class) {
      System.out.println("God, I am lazy but I will count");
      for (int i = 0; i < 5; i++) {
        System.out.println(i);
      }
    }
  }
}
