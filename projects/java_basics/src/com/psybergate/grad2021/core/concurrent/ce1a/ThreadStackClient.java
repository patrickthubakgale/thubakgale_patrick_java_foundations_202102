package com.psybergate.grad2021.core.concurrent.ce1a;

public class ThreadStackClient {

  public static void main(String[] args) {
    ThreadStack multi = new ThreadStack(new SomeClass01());
    Thread thread = new Thread(multi);
    thread.start();
  }
}