package com.psybergate.grad2021.core.concurrent.onlineexample;

import java.util.concurrent.ThreadLocalRandom;

public class Sender implements Runnable {

  private MessageCache cache;

  public Sender(MessageCache cache) {
    this.cache = cache;
  }

  @Override
  public void run() {
    String packets[] = {"First packet", "Second packet", "Third packet", "Fourth packet", "End"};
    for (String packet : packets) {
      cache.send(packet);
      try {
        Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        e.printStackTrace();
      }
    }
  }
}
