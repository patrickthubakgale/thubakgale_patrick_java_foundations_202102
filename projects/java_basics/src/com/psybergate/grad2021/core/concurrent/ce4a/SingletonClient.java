package com.psybergate.grad2021.core.concurrent.hw4a;

public class SingletonClient {

  private static com.psybergate.grad2021.core.concurrent.hw4a.Singleton firstInstance;

  private static com.psybergate.grad2021.core.concurrent.hw4a.Singleton secondInstance;

  public static void main(String[] args) throws InterruptedException {
    Thread thread1 = new Thread(new Runnable() {
      @Override
      public void run() {
        firstInstance = com.psybergate.grad2021.core.concurrent.hw4a.Singleton.getInstance();
      }
    });
    Thread thread2 = new Thread(new Runnable() {
      @Override
      public void run() {
        secondInstance = com.psybergate.grad2021.core.concurrent.hw4a.Singleton.getInstance();
      }
    });
    thread1.start();
    thread2.start();
    firstInstance.doSomething();
    thread1.join();
    thread2.join();

    //check if two instances are equivalent
    System.out.println("(firstInstance == secondInstance) = " + (firstInstance == secondInstance));
    firstInstance.doNothing();
    System.out.println("-------------------------");
    secondInstance.doNothing();
  }
}
