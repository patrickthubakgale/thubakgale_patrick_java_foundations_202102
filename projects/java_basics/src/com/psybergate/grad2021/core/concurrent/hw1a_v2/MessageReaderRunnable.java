package com.psybergate.grad2021.core.concurrent.hw1a_v2;

import static com.psybergate.grad2021.core.utils.Utils.random;

public class MessageReaderRunnable implements Runnable{

  @Override
  public void run() {
    MessageReader reader = new MessageReader();
    while (true) {
      try {
        System.out.println("Reading your messages: ");
        reader.read();
        Thread.sleep(random(2000,5000));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
