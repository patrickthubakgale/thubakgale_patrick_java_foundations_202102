package com.psybergate.grad2021.core.concurrent.hw1a_v2;

public class MessageReader {

  public synchronized void read() {
    while (true) {
      String message = messageCache().read();
      if (message == null) {
        break;
      }
      System.out.println("Your message is: " + message);
    }
    notifyAll();
  }

  public MessageCache messageCache() {
    return MessageCache.getInstance();
  }
}
