package com.psybergate.grad2021.core.concurrent.ce3a;

public class Factorial {

  private long endNum;

  private long startNum;

  public Factorial(long startNum, long endNum) {
    this.startNum = startNum;
    this.endNum = endNum;
  }

  public long factorial() {
    long result = 1;
    for (long i = startNum; i <= endNum; i++) {
      result *= i;
    }
    return result;
  }
}
