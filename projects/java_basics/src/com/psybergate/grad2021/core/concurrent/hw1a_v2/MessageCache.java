package com.psybergate.grad2021.core.concurrent.hw1a_v2;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {

  private static final MessageCache INSTANCE = new MessageCache();

  private List<String> messages = new ArrayList<>();

  public synchronized void add(String message) {
    messages.add(message);
//    notifyAll();
  }

  public synchronized String read() {
    if (messages.isEmpty()) {
      //wait();
      return null;
    }
    return messages.remove(0);
  }

  public static MessageCache getInstance() {
    return INSTANCE;
  }
}
