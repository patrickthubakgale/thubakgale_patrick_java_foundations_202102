package com.psybergate.grad2021.core.concurrent.ce3a;

public class FactorialRunnable implements Runnable {

  private Factorial factorial;

  private long sum;

  public FactorialRunnable(Factorial factorial) {
    this.factorial = factorial;
  }

  @Override public void run() {
    sum = factorial.factorial();
  }

  public long getSum() {
    return sum;
  }
}
