package com.psybergate.grad2021.core.concurrent.hw1a;

import static com.psybergate.grad2021.core.utils.Utils.random;

public class WriterRunnable implements Runnable {

  private MessageCache instance = MessageCache.getInstance();

  @Override
  public void run() {
    int i = 0;
    while (++i <= 3) {
      try {
        String message = "Message number " + random(1, 40);
        write(message);
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        System.out.println("Thread interrupted " + e);//Squash for now
      }
    }
  }

  public void write(String message) {
    synchronized (this) {
      MessageCache.getInstance().add(message);
      System.out.println("Message to be added: " + message);
//    instance.setWriting(false);
      notifyAll();
    }
  }
}