package com.psybergate.grad2021.core.concurrent.ce3a;

import java.util.Map;

public class ConcurrentAdder {

  private long endNum;

  private int numOfRanges;

  public ConcurrentAdder(long endNum, int numOfRanges) {
    this.endNum = endNum;
    this.numOfRanges = numOfRanges;
  }

  public void add() throws InterruptedException {
    Timer timer = new Timer();
    timer.start();
    long finalSum = getConcurrentSum();
    timer.end();
    System.out.println("sum is " + finalSum + " completed in " + timer.duration());
  }

  private long getConcurrentSum() throws InterruptedException {
    long startOfRange = 0;
    long range = endNum / numOfRanges;
    long endOfRange = 0;
    ThreadPool threadPool = new ThreadPool();
    createAndRunThreads(startOfRange, range, endOfRange, threadPool);
    return getFinalSum(threadPool);
  }

  private long getFinalSum(ThreadPool threadPool) throws InterruptedException {
    long finalSum = 0;
    for (Map.Entry<Thread, Runnable> entry : threadPool.getThreads()) {
      Thread thread = entry.getKey();
      thread.join();
      AdderRunnable runnable = (AdderRunnable) entry.getValue();
      finalSum += runnable.getSum();
    }
    return finalSum;
  }

  private void createAndRunThreads(long startOfRange, long range, long endOfRange,
                                   ThreadPool threadPool) {
    for (int i = 1; i <= numOfRanges; i++) {
      startOfRange = endOfRange + 1;
      if (i == numOfRanges) {
        endOfRange = endNum;
      } else {
        endOfRange += range;
      }
      Adder a = new Adder(startOfRange, endOfRange);
      AdderRunnable r = new AdderRunnable(a);
      Thread t = new Thread(r);
      t.start();
      threadPool.add(t, r);
    }
  }
}
