package com.psybergate.grad2021.core.concurrent.Learning;

public class ThreadsCount extends Thread {
  public ThreadsCount() {
    super("My extending thread");
    System.out.println("My thread created " + this);
    start();
  }

  @Override
  public void run() {
    try {
      for (int i = 0; i < 10; i++) {
        System.out.println("Printing the count " + i);
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    System.out.println("My thread run is over");
  }
}

class ThreadClient {
  public static void main(String[] args) {
    ThreadsCount count = new ThreadsCount();
    try {
      while (count.isAlive()) {
        System.out.println("Main thread will be alive till the child thread is live");
        Thread.sleep(1500);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("Main thread run is over");
  }
}

