package com.psybergate.grad2021.core.concurrent.hw1a_v2;

import java.util.List;

import static com.psybergate.grad2021.core.utils.Utils.random;

public class MessageWriterRunnable implements Runnable{

  @Override
  public void run() {
    int count = 1;
    MessageWriter writer = new MessageWriter();
    MessagePool pool = MessagePool.getInstance();
    List<String> messagePool = pool.getMessages();
    while (true) {
      try {
        String message = messagePool.get(random(7));
        writer.write(message);
        Thread.sleep(random(2000,5000));
//        if (count++ == 3) {
//          break;
//        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
