package com.psybergate.grad2021.core.concurrent.hw1a;

public class MessageClient {

  public static void main(String[] args) {
    Thread thread1 = new Thread(new WriterRunnable());
    Thread thread2 = new Thread(new ReaderRunnable());
    thread1.start();
    thread2.start();
  }
}
