package com.psybergate.grad2021.core.concurrent.hw1a_v2;

import java.util.Arrays;
import java.util.List;

public class MessagePool {

  private static final MessagePool INSTANCE = new MessagePool();

  private List<String> messages = Arrays.asList("Let's go to town", "Market share drops", "Oil companies benefiting",
          "Football sucks", "Favourite rugby player is Pieter-Steph du Toit", "Mr 360 pondering a return to " +
                  "international cricket", "Psybergate is the best");

  public List<String> getMessages() {
    return messages;
  }

  public static MessagePool getInstance() {
    return INSTANCE;
  }
}
