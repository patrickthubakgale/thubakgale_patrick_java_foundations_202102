package com.psybergate.grad2021.core.concurrent.ce3a;

public class ConcurrentAdderSimple {

  public ConcurrentAdderSimple() {
  }

  public void add() throws InterruptedException {
    Timer timer = new Timer();
    timer.start();
    Adder a1 = new Adder(1, 50000);
    Adder a2 = new Adder(50001, 100000);

    AdderRunnable r1 = new AdderRunnable(a1);
    AdderRunnable r2 = new AdderRunnable(a2);
    Thread t1 = new Thread(r1);
    Thread t2 = new Thread(r2);
    t1.start();
    t2.start();
    t1.join();
    t2.join();

    long sum = r1.getSum() + r2.getSum();
    timer.end();
    System.out.println("sum is " + sum + " completed in " + timer.duration());
  }
}
