package com.psybergate.grad2021.core.concurrent.onlineexample;

public class Client {

  public static void main(String[] args) {
    MessageCache cache = new MessageCache();
    Thread sender = new Thread(new Sender(cache));
    Thread receiver = new Thread(new Receiver(cache));

    sender.start();
    receiver.start();
  }
}
