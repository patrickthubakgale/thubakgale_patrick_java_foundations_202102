package com.psybergate.grad2021.core.concurrent.ce1a;

public class SomeClass01 {

  public static void someMethod01() {
    someMethod02();
  }

  public static void someMethod02() {
    someMethod03();
  }

  public static void someMethod03() {
    throw new RuntimeException();
  }

}
