package com.psybergate.grad2021.core.concurrent.ce3a;

public class Client {

  public static void main(String[] args) throws InterruptedException {
    new ConcurrentAdder(10, 3).add();
    new ConcurrentFactorial(6, 2).add();
  }
}
