package com.psybergate.grad2021.core.concurrent.Learning;

public class MultiThreading extends Thread {
  @Override
  public void run() {
    System.out.println("My thread is in running state");
  }

  public static void main(String[] args) {
    MultiThreading multi = new MultiThreading();
    multi.start();
  }
}
