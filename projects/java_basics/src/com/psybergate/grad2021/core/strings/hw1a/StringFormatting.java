package com.psybergate.grad2021.core.strings.hw1a;

import java.util.Formatter;
import java.util.Locale;

public class StringFormatting {

  public static void main(String[] args) {
    StringBuilder stringBuilder = new StringBuilder();
    Formatter formatter = new Formatter(stringBuilder, Locale.US);

    formatter.format("%3$2s %1$2s %4$2s %2$2s", "a", "b", "c", "d");
    System.out.println("stringBuilder = " + stringBuilder);

    StringBuilder stringBuilder1 = new StringBuilder();
    Formatter formatter1 = new Formatter(stringBuilder1);
    formatter1.format("E = %f%n", Math.E);
    System.out.print("String builder = " + stringBuilder1.toString());
  }
}
