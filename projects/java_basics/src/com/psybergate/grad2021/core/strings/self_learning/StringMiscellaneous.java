package com.psybergate.grad2021.core.strings.self_learning;

public class StringMiscellaneous {
  public static void main(String[] args) {
    String s1 = "hello there";
    char[] charArray = new char[5];

    System.out.printf("s1: %s", s1);
    System.out.printf("%nLength of s1: %d", s1.length());
    System.out.printf("%nThe string reversed is: ");

    for (int count = s1.length() - 1; count >= 0; count--) {
      System.out.printf("%c ", s1.charAt(count));
    }

    //copy characters from string into charArray
    s1.getChars(0, 5, charArray, 0);
    System.out.printf("%nThe character array is: ");

    for (char character : charArray) {
      System.out.print(character);
    }

    System.out.println("\nnew string stuff\n");

    char[] c1 = new char[4];
    s1.getChars(0, 4, c1, 0);
    for (char c : c1) {
      System.out.print(c);
    }
    System.out.println();
  }
}
