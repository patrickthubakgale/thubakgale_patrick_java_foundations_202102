package com.psybergate.grad2021.core.strings.self_learning;

public class StringInterninig {
  public static void main(String[] args) {
    String s1 = "abc";
    String s2 = s1.concat("def");

    System.out.println("s2 = " + s2);
    System.out.println("(\"abcdef\" == s2) = " + ("abcdef" == s2));
    System.out.println("(\"abcdef\" == s2.intern()) = " + ("abcdef" == s2.intern()));
    System.out.println("(\"abc\" == s1) = " + ("abc" == s1));
  }
}
