package com.psybergate.grad2021.core.strings.self_learning;

public class Test {
  public static void main(String[] args) {
    String s1 = new String("hello");
    String s2 = new String("hello");
    System.out.println("s1.equals(s2) = " + s1.equals(s2));
    System.out.println("s1 == s2 = " + (s1 == s2));

    String s3 = new String("Welcome");
    if (s3.regionMatches(3, "Come", 0, 4)) {
      System.out.println("Strings matches");
    } else {
      System.out.println("Nothing matches here!");
    }
  }
}
