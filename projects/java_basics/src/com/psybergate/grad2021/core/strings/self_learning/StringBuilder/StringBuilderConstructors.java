package com.psybergate.grad2021.core.strings.self_learning.StringBuilder;

public class StringBuilderConstructors {
  public static void main(String[] args) {
    StringBuilder buffer1 = new StringBuilder();
    StringBuilder buffer2 = new StringBuilder(10);
    StringBuilder buffer3 = new StringBuilder("hello");

    System.out.println("buffer1 = \"" + buffer1 + "\"");
    System.out.println("buffer1 = \"" + buffer2 + "\"");
    System.out.println("buffer1 = \"" + buffer3 + "\"");
  }
} // end class StringBuilderConstructors
