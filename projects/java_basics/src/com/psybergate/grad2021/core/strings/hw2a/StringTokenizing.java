package com.psybergate.grad2021.core.strings.hw2a;

import java.util.StringTokenizer;

public class StringTokenizing {

  public static void main(String[] args) {
    StringTokenizer stringTokenizer = new StringTokenizer("All football lovers usually make noise about their " +
            "overrated players from their respective teams. So immature!");
    while (stringTokenizer.hasMoreTokens()) {
      System.out.println(stringTokenizer.nextToken());
    }
  }
}
