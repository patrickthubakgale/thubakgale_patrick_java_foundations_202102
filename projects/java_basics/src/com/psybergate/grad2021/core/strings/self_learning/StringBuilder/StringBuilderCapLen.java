package com.psybergate.grad2021.core.strings.self_learning.StringBuilder;

public class StringBuilderCapLen {
  public static void main(String[] args) {
    StringBuilder buffer = new StringBuilder("Hello, how are you?");

    System.out.println("buffer = " + buffer.toString() + "\nlength = " + buffer.length() + " " +
            "\ncapacity = " + buffer.capacity());

    buffer.ensureCapacity(75);
    System.out.println("\nNew capacity = " + buffer.capacity());

    buffer.setLength(10);
    System.out.println("\nNew length = " + buffer.length() + "\nbuffer = " + buffer.toString());
  }
}
