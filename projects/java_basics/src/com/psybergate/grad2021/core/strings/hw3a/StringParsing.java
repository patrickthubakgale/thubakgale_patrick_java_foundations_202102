package com.psybergate.grad2021.core.strings.hw3a;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class StringParsing {

  public static void main(String[] args) throws ParseException {
    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");

    String string1 = new String("05/03/2021");
    String string2 = new String("22-April-2000");
    Date date1 = simpleDateFormat1.parse(string1);
    Date date2 = simpleDateFormat2.parse(string2);

    System.out.println("date1 = " + date1);
    System.out.println("date2 = " + date2);

    String string3    = "a non making sense demo string";
  }
}
