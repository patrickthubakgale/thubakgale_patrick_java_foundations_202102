package com.psybergate.grad2021.core.strings.hw4a;

public class StringConcatOptimisation {

  public static void main(String[] args) {
    String string1 = "hello";
    System.out.println(string1 + " there");
    String string2 = " there";
    System.out.println(string1.concat(string2));

    //Optimisation need to be seen on the JVM. not easy to do!
  }
}
