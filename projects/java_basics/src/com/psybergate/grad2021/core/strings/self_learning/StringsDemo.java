package com.psybergate.grad2021.core.strings.self_learning;

public class StringsDemo {

  public static void main(String[] args) {
    String s = "abc";
    System.out.println(s.concat("def"));
    System.out.println(s);

    String s2 = new String("abc");
    String s3 = "abc";

    System.out.println("(s3 == s) = " + (s3 == s));
    System.out.println("(s3 == s2) = " + (s3 == s2));

    //String buffering
    StringBuffer stringBuffer = new StringBuffer("hello ");
    System.out.println("stringBuffer = " + stringBuffer);
    stringBuffer.append("world");
    System.out.println("stringBuffer after append = " + stringBuffer);

    //delete
    stringBuffer.delete(6, 11);
    System.out.println("stringBuffer after delete = " + stringBuffer);

    //insert
    System.out.println("stringBuffer.length() = " + stringBuffer.length());
    String s4 = "there";
    stringBuffer.insert(6, s4);
    System.out.println("stringBuffer after insert = " + stringBuffer);
    System.out.println("stringBuffer.reverse() = " + stringBuffer.reverse());
    System.out.println("stringBuffer.toString() = " + stringBuffer.reverse().toString());
  }
}
