package com.psybergate.grad2021.core.strings.hw3a;

public class SplitString {

  public static void main(String[] args) {
    //split method returns an array of strings
    //takes in regular expression hence throws PatternSyntaxException

    String colon = "com:psybergate:grad2021:core:strings:self_learning";
    String[] strings = colon.split(":");

    System.out.println("Original string = " + colon);
    System.out.println("New splitted string after split() method call");

    for (String string : strings) {
      System.out.println(string);
    }

    System.out.println("\n##################################################\n");
    //splitting using space as delimiter
    String s1 = "Hello we are underway on the crease as the Proteas takes on the Kiwis";
    String[] space = s1.split("\\s");

    for (String s : space) {
      System.out.println(s);
    }
  }
}
