package com.psybergate.grad2021.core.strings.hw3a;

import java.util.regex.Pattern;

public class LearningRegex {

  public static void main(String[] args) {
    System.out.println("learning regular expression in java");
    String finder = "\\d+(\\.\\d+)?";
    String longString = "2.333 is a small 1 number 3.45 is better than 2,4 and 8.000";

    String[] broken = longString.split(finder);

//    for (String s : broken) {
//      System.out.println(s);
//    }

    //Pattern and Matcher classes
    boolean b2 = Pattern.compile("As they sat down astounded, he arrived same time").matcher("as").matches();
    System.out.println("b2 = " + b2);
  }
}
/* REGEX RULES
* . (dot) matches any character
* ^regex finds regex that matches at the beginning of a line
* [abc] Set definition, can match letters a or b or c.
* [abc][vz] Set definition, can match letters a or b or c followed by v or z.
* [^abc] Negates the pattern, matches any character except a or b or c. (^ this is a caret)
* [a-d1-7] Matches a letter between a and d and figures from 1 to 7
* \w A word character. Similar to [a-zA-Z_0-9]
* */