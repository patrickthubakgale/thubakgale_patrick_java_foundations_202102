package com.psybergate.grad2021.core.strings.ce2a;

public class StringImmutability {

  public static void main(String[] args) {
    String s1 = "Hello";
    System.out.println("s1 = " + s1);

    //demo immutability
    s1.concat(" world");
    System.out.println("s1 after s1.concat(world) = " + s1);
    System.out.println("s1.concat(\" world\") = " + s1.concat(" world"));
  }
}
