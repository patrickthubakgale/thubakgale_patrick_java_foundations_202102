package com.psybergate.grad2021.core.strings.self_learning;

public class StringIndexMethods {
  public static void main(String[] args) {
    String letters = "abcdefghijklmabcdefghijklm";

    //test
    System.out.printf("'c' is located at index %d%n", letters.indexOf('c'));
    System.out.printf("'a' is located at index %d%n", letters.indexOf('a', 1));
    System.out.printf("'$' is located at index %d%n", letters.indexOf('$'));
    System.out.printf("Last 'c' is located at index %d%n", letters.lastIndexOf('c'));
    System.out.printf("Last 'a' is located at index %d%n", letters.lastIndexOf('a', 25));
    System.out.printf("'$ is located at index %d%n", letters.lastIndexOf('$'));

    //test indexOf to locate a substring in a string
    System.out.printf("\"def\" is located at index %d%n", letters.indexOf("def"));
    System.out.printf("\"def\" is located at index %d%n", letters.indexOf("def", 7));
    System.out.printf("\"hello\" is located at index %d%n", letters.indexOf("hello"));

    //test lastIndexOf to find a substring in a string
    System.out.printf("Last \"def\" is located at index %d%n", letters.lastIndexOf("def"));
    System.out.printf("Last \"def\" is located at index %d%n", letters.lastIndexOf("def", 25));
    System.out.printf("Last \"hello\" is located at index %d%n", letters.lastIndexOf("hello"));

    //Substring stuff. On your marks...
    letters = "abcdefghijklmabcdefghijklm";

    //test substring methods
    System.out.printf("Substring from index 20 to end is \"%s\"%n", letters.substring(20));
    System.out.printf("%s \"%s\"%n", "Substring from index 3 up to, but not including 6 is", letters.substring(3, 6));
  }
} // end class StringIndexMethods
