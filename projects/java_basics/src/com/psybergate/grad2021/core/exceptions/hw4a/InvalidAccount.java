package com.psybergate.grad2021.core.exceptions.hw4a;

public class InvalidAccount extends Exception {

  public InvalidAccount(String message) {
    super(message);
  }
}
