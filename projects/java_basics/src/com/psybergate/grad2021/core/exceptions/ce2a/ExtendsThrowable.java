package com.psybergate.grad2021.core.exceptions.ce2a;

public class ExtendsThrowable extends Throwable{

  public ExtendsThrowable(String message) {
    super(message);
  }
}
