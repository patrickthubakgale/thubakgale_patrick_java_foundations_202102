package com.psybergate.grad2021.core.exceptions.ce2a;

import static com.psybergate.grad2021.core.exceptions.ce2a.ClassB.*;

public class ClassA {

  public static void main(String[] args) {
   someMethod1();
  }

  private static void someMethod1() {
    someMethod02();
  }

  private static void someMethod2() {
    someMethod03();
  }

  private static void someMethod3() throws ExtendsThrowable {
    someMethod04();
  }

  private static void someMethod4() throws CheckedExceptionDemo {
    someMethod01();
  }


}
