package com.psybergate.grad2021.core.exceptions.hw4a;

public class AccountService {

  private AccountDB accountDB = AccountDB.ACCOUNT_DB;

  public void setNewAccount(String accountNum, String name, double balance) {
    CheckAccount checkAccount = new CheckAccount(accountNum, name, balance);
    AccountDB.addAccounts(checkAccount);
  }

  public void deposit(String accountNum, double amount) {
    CheckAccount checkAccount = AccountDB.getAccount(accountNum);
    checkAccount.deposit(amount);
  }

  public void withdrawal(String accountNum, double amount) {
    CheckAccount checkAccount = AccountDB.getAccount(accountNum);
    checkAccount.withdrawal(amount);
  }

  public double getBalance(String accountNum) {
    CheckAccount checkAccount = AccountDB.getAccount(accountNum);
    return checkAccount.getBalance();
  }
}
