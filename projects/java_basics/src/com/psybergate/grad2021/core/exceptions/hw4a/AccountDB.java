package com.psybergate.grad2021.core.exceptions.hw4a;

import java.util.HashMap;

public final class AccountDB {

  public static final AccountDB ACCOUNT_DB = new AccountDB();

  private static HashMap<String, CheckAccount> accounts = new HashMap<>();

  private AccountDB() {

  }

  public static void addAccounts(CheckAccount account) {
    accounts.put(account.getAccountNum(), account);
  }

  public static CheckAccount getAccount(String accountNum) {
    return accounts.get(accountNum);
  }
}