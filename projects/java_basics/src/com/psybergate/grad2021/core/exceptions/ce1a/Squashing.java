package com.psybergate.grad2021.core.exceptions.ce1a;

public class Squashing {

  public static void main(String[] args) {
    try {
      someMethod01();
    } catch (NumNotDefinedProperly e) {
      System.out.println("Squashing the exception!\n" + e.getMessage());
    }
  }

  private static void someMethod01() throws NumNotDefinedProperly {
    throw new NumNotDefinedProperly("Unknown call to the method");
  }
}
