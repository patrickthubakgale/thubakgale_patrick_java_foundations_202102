package com.psybergate.grad2021.core.exceptions.hw1a;

public class RuntimeExceptionsInNestedMethods {

  public static void main(String[] args) {
      someMethod01();
  }

  private static void someMethod01() {
    someMethod02();
  }

  private static void someMethod02()  {
    someMethod03();
  }

  private static void someMethod03() {
    someMethod04();
  }

  private static void someMethod04() {
    someMethod05("Dan");
  }

  private static void someMethod05(String name) {
    if (name.length() < 4) {
      throw new IllegalArgumentException("Good names have at least four characters");
    }
    System.out.println("Nice name dude!");
  }
}
