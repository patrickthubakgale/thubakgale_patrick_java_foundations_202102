package com.psybergate.grad2021.core.exceptions.ce2a;

public class ClassB {

  public static void main(String[] args) {
    try {
      someMethod04();
    } catch (ExtendsThrowable e) {
      e.printStackTrace();
    }
  }

  public static void someMethod01() throws CheckedExceptionDemo {
    throw new CheckedExceptionDemo("Checked exception");
  }

  public static void someMethod02() {
    try {
      someMethod01();
    } catch (CheckedExceptionDemo e) {
      throw new ExtendsError("Extends error");
    }
  }

  public static void someMethod03() {
    try {
      someMethod02();
    } catch (Exception e) {
      throw new RuntimeExceptionDemo("Runtime exception");
    }
  }

  public static void someMethod04() throws ExtendsThrowable {
    try {
      someMethod03();
    } catch (Exception e) {
      throw new ExtendsThrowable("Extends Throwable");
    }
  }

}
