package com.psybergate.grad2021.core.exceptions.ce3a;

public class PropagatedException extends Exception {

  public PropagatedException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
