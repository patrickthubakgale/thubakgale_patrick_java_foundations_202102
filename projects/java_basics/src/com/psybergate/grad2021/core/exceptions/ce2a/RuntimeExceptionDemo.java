package com.psybergate.grad2021.core.exceptions.ce2a;

public class RuntimeExceptionDemo extends RuntimeException {

  public RuntimeExceptionDemo(String message) {
    super(message);
  }
}
