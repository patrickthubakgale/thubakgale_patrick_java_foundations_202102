package com.psybergate.grad2021.core.exceptions.hw4a;

public class AccountController {

  private AccountService service;

  public AccountController(AccountService service) {
    this.service = service;
  }

  public void setNewAccount(String accountNum, String name, double balance) {
    service.setNewAccount(accountNum, name, balance);
  }

  public AccountService getService() {
    return service;
  }

  public void deposit(String accountNum, double amount) {
    service.deposit(accountNum,amount);
  }

  public void withdraw(String accountNum, double amount) {
    service.withdrawal(accountNum, amount);
  }

  public double getBalance(String accountNum) {
    return service.getBalance(accountNum);
  }
}
