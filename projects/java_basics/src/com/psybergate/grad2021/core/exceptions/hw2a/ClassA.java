package com.psybergate.grad2021.core.exceptions.hw2a;

import java.sql.SQLException;

public class ClassA {

  public static void main(String[] args) {
    try {
      methodA();
    } catch (ApplicationException01 e) {
      if (e.getCause() instanceof SQLException) {
        e.printStackTrace();
//        System.out.println("shucks");
      }
      System.out.println(e.getMessage());
    }
  }

  public static void methodA()  throws ApplicationException01 {
    try {
      methodB();
    } catch (SQLException e) {
      throw new ApplicationException01("Error occured", e); //What does input Stream look like?
    }
  }

  public static void methodB() throws SQLException {
    int i = 0; //successfully retrieved data from database, false otherwise
    if (i != 1) {
      throw new SQLException("Data could not be retrieved from the database");
    }
    System.out.println("Data retrieved successfully");
  }
}
