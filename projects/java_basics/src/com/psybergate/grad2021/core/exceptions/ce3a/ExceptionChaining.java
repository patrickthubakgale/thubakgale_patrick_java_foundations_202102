package com.psybergate.grad2021.core.exceptions.ce3a;

import com.psybergate.grad2021.core.exceptions.ce2a.NameLessThanFourChars;

public class ExceptionChaining {

  public static void main(String[] args) throws PropagatedException {
    try {
      someMethod01();
    } catch (IllegalArgumentException e) {
      throw new PropagatedException("Unknown", e);
//      e.printStackTrace();
      //System.out.println("propa");
    }
  }

  private static void someMethod01() {
    try {
      someMethod02("Dan");
    } catch (NameLessThanFourChars e) {
      throw new IllegalArgumentException("Dirty stuff", e);
    }
  }

  private static void someMethod02(String name) throws NameLessThanFourChars {
    if (name.length() < 4) {
      throw new NameLessThanFourChars("Good name must have at least four characters");
    }
    System.out.println("Nice name dude!");
  }

}
