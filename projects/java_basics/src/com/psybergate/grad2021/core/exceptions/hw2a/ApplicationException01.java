package com.psybergate.grad2021.core.exceptions.hw2a;

public class ApplicationException01 extends Exception {

  public ApplicationException01(String message, Throwable cause) {
    super(message, cause);
  }
}
