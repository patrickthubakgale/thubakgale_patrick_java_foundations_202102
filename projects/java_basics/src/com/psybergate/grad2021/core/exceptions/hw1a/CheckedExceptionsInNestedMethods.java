package com.psybergate.grad2021.core.exceptions.hw1a;

import com.psybergate.grad2021.core.exceptions.ce2a.NameLessThanFourChars;

public class CheckedExceptionsInNestedMethods {

  public static void main(String[] args) {
    try {
      someMethod01();
    } catch (NameLessThanFourChars e) {
      e.printStackTrace();
    }
  }

  private static void someMethod01() throws NameLessThanFourChars {
    someMethod02();
  }

  private static void someMethod02() throws NameLessThanFourChars {
    someMethod03();
  }

  private static void someMethod03() throws NameLessThanFourChars {
    someMethod04();
  }

  private static void someMethod04() throws NameLessThanFourChars {
    someMethod05("Dan");
  }

  private static void someMethod05(String name) throws NameLessThanFourChars {
    if (name.length() < 4) {
      throw new NameLessThanFourChars("Good names have at least four characters");
    }
    System.out.println("Nice name dude!");
  }
}
