package com.psybergate.grad2021.core.exceptions.hw4a;

public class CheckAccount {

  private String accountNum;

  private String name;

  private double balance;

  public CheckAccount(String accountNum, String name, double balance) {
    this.accountNum = accountNum;
    this.name = name;
    this.balance = balance;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  public void setAccountNum(String accountNum) {
    this.accountNum = accountNum;
  }

  public void deposit(double amount) {
    //You may want to restrict users from depositing certain amount of money
    this.balance += amount;
  }

  public void withdrawal(double amount) {
    //validate this: amount must be less than balance for this operation
    this.balance -= amount;
  }

  public String getName() {
    return name;
  }

  public String getAccountNum() {
    return accountNum;
  }

  public double getBalance() {
    return balance;
  }
}
