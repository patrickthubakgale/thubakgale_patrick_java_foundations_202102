package com.psybergate.grad2021.core.exceptions.ce2a;

public class NameLessThanFourChars extends Exception {

  public NameLessThanFourChars(String message) {
    super(message);
  }
}
