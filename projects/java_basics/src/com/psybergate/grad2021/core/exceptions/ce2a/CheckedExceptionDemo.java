package com.psybergate.grad2021.core.exceptions.ce2a;

public class CheckedExceptionDemo extends Exception{

  public CheckedExceptionDemo(String message) {
    super(message);
  }
}
