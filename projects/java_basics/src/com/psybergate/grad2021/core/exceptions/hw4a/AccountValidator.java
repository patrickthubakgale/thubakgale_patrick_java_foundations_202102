package com.psybergate.grad2021.core.exceptions.hw4a;

public class AccountValidator {

  public static void validateAccount(CheckAccount account) throws InvalidAccount {
    if (!(account.getAccountNum().length() != 5)) {
      throw new InvalidAccount("Invalid account number");
    }
  }
}
