package com.psybergate.grad2021.core.exceptions.ce1a;

public class NumNotDefinedProperly extends Throwable {

  public NumNotDefinedProperly(String message) {
    super(message);
  }
}
