package com.psybergate.javafnds.cmdline.hw5a.app;

import com.psybergate.javafnds.cmdline.hw5a.service.TaxService;
import com.psybergate.javafnds.cmdline.hw5a.service.impl.TaxServiceImpl;

/**
 * Tax Service managed by Psybergate
 * 
 * @since 19 Jan 2020
 * <p>
 * Author: Tom
 * Senior dev
 * <p>
 *
 * No duplication of code is allowed. Go argue with yourself.
 *
 *
 */
public class MainApp {

  public static void main(String[] args) {
    TaxService taxService = new TaxServiceImpl();
    System.out.println("Calculated Tax for John Smith is: " + taxService.calculateTax("123456"));
  }

}
