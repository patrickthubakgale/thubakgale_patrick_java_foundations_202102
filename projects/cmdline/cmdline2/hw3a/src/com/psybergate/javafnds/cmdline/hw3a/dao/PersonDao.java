package com.psybergate.javafnds.cmdline.hw5a.dao;

import com.psybergate.javafnds.cmdline.hw5a.domain.Person;

/**
 * 
 * @since 19 Jan 2020
 */
public class PersonDao {

  public Person getPerson(String taxReferenceNum) {
    return new Person(taxReferenceNum, "John", "Smith", 200000);
  }

}
