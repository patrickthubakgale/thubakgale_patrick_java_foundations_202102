package com.psybergate.javafnds;

import java.util.Scanner;

public class Profiler {
	public static void main(String[] args) {
		System.out.print("Enter your name");
		Scanner sc = new Scanner(System.in);
		String name = sc.nextLine();
		for(int i = 0; i < 10; i++) {
			System.out.println(i);
		}
		System.out.println("Done counting.");
	}
}