package com.psybergate.dir1.hw1a.mystringutils;

public class Second{
	public static String reverse(String str){
		String reversedString = "";
		for(int i = str.length()-1; i >= 0; i--){
			reversedString = reversedString + str.charAt(i);
		}
		return reversedString;
	}
}

/*
	char[] arr = str.toCharArray();
    for (int i = arr.length - 1; i >= 0; i--) {
        
    }
*/