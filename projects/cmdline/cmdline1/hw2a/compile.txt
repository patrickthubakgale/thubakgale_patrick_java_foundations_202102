compile from root directory
javac -d bin -sourcepath src src\com\psybergate\dir2\hw1b\stringreverse\First.java

run
java -cp bin com.psybergate.dir2.hw1b.stringreverse.First

create jar file from bin
jar cf ..\lib\mystringutils.jar com\psybergate\dir1\hw1a\mystringutils\Second.class

run class referencing jar
java -cp lib\mystringutils.jar;bin com.psybergate.dir2.hw1b.stringreverse.First
