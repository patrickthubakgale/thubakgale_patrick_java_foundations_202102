package com.psybergate.second;

import com.psybergate.third.*;

public class ClassB{
	public static void method2() {
		System.out.println("Class B calls Class C");
		ClassC classC = new ClassC();
		classC.method3();
	}	
}