package com.psybergate.first;

import com.psybergate.second.*;

public class ClassA{
	public static void main(String[] args) {
		method1(0);	
	}

	public static void method1(int stop) {
		ClassB classB = new ClassB();
		if(stop == -1)
			return;

		System.out.println("Class A calls Class B");
		classB.method2();
	}
}