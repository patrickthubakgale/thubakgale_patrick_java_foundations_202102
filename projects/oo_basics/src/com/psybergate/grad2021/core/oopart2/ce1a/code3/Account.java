package com.psybergate.grad2021.core.oopart2.ce1a.code3;

public class Account {
  private String accountNumber;
  private double balance;

  public Account(String accountNumber, double balance) {
    this.accountNumber = accountNumber;
    this.balance = balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }
}
