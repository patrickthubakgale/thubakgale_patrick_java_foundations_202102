package com.psybergate.grad2021.core.oopart2.explore;

public class Overriden extends Overriding {
  public Overriden(String name, String surname, double money) {
    super(name, surname, money);
  }

  /*
  * This class inherits from Overriding but does not inherit private members.
  * */
  private void setName(final String name) {
    this.name = name;
  }

  /*
  * Overriding requires that method signature and access modifiers in subclasses be consistent with
  * that of the parent class
  * */

  public void newName(final String name, final String surname) {
    setName(name);
    this.surname = surname;
    System.out.println("I am overriding newName method from parent class!");
  }
}
