package com.psybergate.grad2021.core.oopart2.explore;

public class OverridingUtils {
  public static void main(String[] args) {
    Overriding overriding = new Overriden("Cop", "Daniels", 200);
    overriding.newName("Pat", "Jacobs");
    System.out.println(overriding.getName());
  }
}
