package com.psybergate.grad2021.core.oopart2.hw5a1;

public class LocalOrder extends Order {
  private double discount;

  public LocalOrder(String orderNumber, double discount) {
    super(orderNumber);
    this.discount = discount;
  }

  public double calcTotal() {
    return (super.calcTotal() * (1-discount));
  }
}
