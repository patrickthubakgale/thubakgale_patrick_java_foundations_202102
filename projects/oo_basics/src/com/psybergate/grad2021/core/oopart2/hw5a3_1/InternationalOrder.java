package com.psybergate.grad2021.core.oopart2.hw5a3_1;

public class InternationalOrder extends Order {

  private final String ORDER_TYPE = "INTERNATIONAL";

  private double importDuties;

  public InternationalOrder(String orderNumber, Customer customer,
                            DiscountPolicy discountPolicy, double importDuties) {
    super(orderNumber, customer, discountPolicy);
    this.importDuties = importDuties;
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
