package com.psybergate.grad2021.core.oopart2.hw5a1_1;

public class Customer {

  private String name;

  private String customerId;

  private CustomerType customerType;

  public Customer(String name, String customerId, CustomerType customerType) {
    this.name = name;
    this.customerId = customerId;
    this.customerType = customerType;
  }

  public String getName() {
    return name;
  }

  public String getCustomerId() {
    return customerId;
  }

  public CustomerType getCustomerType() {
    return customerType;
  }
}
