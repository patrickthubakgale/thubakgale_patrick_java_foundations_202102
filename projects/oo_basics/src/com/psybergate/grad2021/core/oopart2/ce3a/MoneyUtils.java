package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class MoneyUtils {
  public static void main(String[] args) {
    Currency currency = new Currency("South Africa", "ZAR");
    Money money = new Money(new BigDecimal(3), currency);
    Money money1 = new Money(new BigDecimal(10), currency);
    System.out.println("money.add(new BigDecimal(4)) = " + money.add(new BigDecimal(4)));
  }
}
