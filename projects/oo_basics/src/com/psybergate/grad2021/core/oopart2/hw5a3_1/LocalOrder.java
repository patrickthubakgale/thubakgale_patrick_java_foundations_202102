package com.psybergate.grad2021.core.oopart2.hw5a3_1;

public class LocalOrder extends Order {

  private final String ORDER_TYPE = "LOCAL";

  public LocalOrder(String orderNumber, Customer customer, DiscountPolicy discountPolicy) {
    super(orderNumber, customer, discountPolicy);
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
