package com.psybergate.grad2021.core.oopart2.demo2a;

import java.util.ArrayList;
import java.util.List;

public class Equality {
  public static void main(String[] args) {
    Equality equality = new Equality();
    System.out.println("System.identityHashCode() = " + System.identityHashCode(equality));

    //demo equality
    Account account = new CurrentAccount("1234", 3000, 1000);
    Account account1 = new SavingsAccount("1234", 3000, 1000);
    Account account2 = new SavingsAccount("1234", 3000, 1000);

    System.out.println(account == account1);
    List<Account> currentAccounts = new ArrayList<>();
    currentAccounts.add(account);
    System.out.println(currentAccounts.contains(account));
    System.out.println(currentAccounts.contains(account1));
    //test using equals
    System.out.println(account2.equals(account1));
  }
}
