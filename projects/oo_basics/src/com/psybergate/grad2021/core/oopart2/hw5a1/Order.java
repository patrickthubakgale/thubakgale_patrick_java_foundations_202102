package com.psybergate.grad2021.core.oopart2.hw5a1;

//import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class Order {
  private String orderNumber;
  List<OrderItem> orderItems = new ArrayList<>();

  public Order(String orderNumber) {
    this.orderNumber = orderNumber;
    //this.orderItems = orderItems;
  }

  public void addOrder(final String name, final double price, final int quantity) {
    orderItems.add(new OrderItem(quantity, new Product(price, name)));
  }

  public double calcTotal() {
    double total = 0;
    for (OrderItem orderItem : orderItems) {
      total += (orderItem.getProduct().getPrice() * orderItem.getQuantity());
    }
    return total;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public String getOrderNumber() {
    return orderNumber;
  }
}
