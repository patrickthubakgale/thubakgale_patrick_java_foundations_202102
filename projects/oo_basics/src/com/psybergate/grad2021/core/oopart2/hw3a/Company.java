package com.psybergate.grad2021.core.oopart2.hw3a;

public class Company extends Customer {
  public Company(final String name, final String customerNum,
                 final String customerAddress) {
    super(name, customerNum, customerAddress);
  }
}
