package com.psybergate.grad2021.core.oopart2.hw5a3;

public class InternationalOrder extends Order {
  private final double importDuties;

  public InternationalOrder(String orderNumber, String policy, int customerDuration, double importDuties) {
    super(orderNumber, policy, customerDuration);
    this.importDuties = importDuties;
  }

  @Override
  public double calcTotal() {
    return (super.calcTotal() + importDuties);
  }

  @Override public double totalAfterDiscount() {
    return calcTotal() * (1 - applyPolicy());
  }
}
