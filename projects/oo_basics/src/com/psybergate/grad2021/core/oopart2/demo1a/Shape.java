package com.psybergate.grad2021.core.oopart2.demo1a;

public abstract class Shape {
  private static final String TYPE = "shape";

  //for demo
  public Shape() {
    System.out.println("I am Shape!");
  }

  public void print() {
    System.out.println("Dimension/s are: " + dimensions() + " and Area is: " + getArea());
  }

  public String getType() {
    return TYPE;
  }

  public static void callMe(final String name) {
    System.out.println("name = " + name);
  }

  public abstract double getArea();

  public abstract String dimensions();
}