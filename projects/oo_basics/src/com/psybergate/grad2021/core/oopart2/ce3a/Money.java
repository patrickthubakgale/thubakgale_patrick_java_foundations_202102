package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class Money {
  private BigDecimal money;
  private Currency currency;

  public Money(BigDecimal money, Currency currency) {
    this.money = money;
    this.currency = new Currency(currency.getName(), currency.getCurrencyCode());
  }

  public BigDecimal add(BigDecimal money) {
    return this.money.add(money);
  }

  public Currency getCurrency() {
    return new Currency(currency.getName(), currency.getCurrencyCode());
  }
}
