package com.psybergate.grad2021.core.oopart2.hw6a;

public class InternationalOrder extends Order {
  private double importDuties;

  public InternationalOrder(String orderNumber, String policy, int customerDuration, double importDuties) {
    super(orderNumber, policy, customerDuration);
    this.importDuties = importDuties;
  }

  public double calcTotal() {
    return (super.calcTotal() + importDuties);
  }

  @Override public double totalAfterDiscount() {
    return calcTotal() * (1 - applyPolicy());
  }
}
