package com.psybergate.grad2021.core.oopart2.hw5a2;

public class LocalOrder extends Order {

  private int customerDuration;

  public LocalOrder(String orderNumber, int customerDuration) {
    super(orderNumber);
    this.customerDuration = customerDuration;
  }

  public int getCustomerDuration() {
    return customerDuration;
  }

  public double getDiscount() {
    double discount = 0;

    if (customerDuration <= 2) {
      discount = 0;
    } else if (customerDuration > 2 && customerDuration <= 5) {
      discount = 0.075;
    } else {
      discount = 0.12;
    }
    return discount;
  }

  public double calcTotal() {
    return (super.calcTotal() * (1 - getDiscount()));
  }
}
