package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class AccountUtils {
  public static void main(String[] args) {
    Account account = new CurrentAccount("1234", 2000, 1000);
  }
}

//Parent constructor is called first to initialize object state and then
//child class is called after to initialize instance variables.