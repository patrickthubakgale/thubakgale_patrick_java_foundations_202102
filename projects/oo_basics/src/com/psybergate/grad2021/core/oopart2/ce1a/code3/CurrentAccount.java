package com.psybergate.grad2021.core.oopart2.ce1a.code3;

public class CurrentAccount extends Account {
  private double overdraft;

  public CurrentAccount(String accountNumber, double balance, double overdraft) {
    super(accountNumber, balance);
    this.overdraft = overdraft;
  }
}
