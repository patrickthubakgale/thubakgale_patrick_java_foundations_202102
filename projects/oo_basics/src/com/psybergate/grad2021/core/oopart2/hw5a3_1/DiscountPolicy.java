package com.psybergate.grad2021.core.oopart2.hw5a3_1;

public class DiscountPolicy {

  private Customer customer;

  private PolicyType policyType;

  public DiscountPolicy(Customer customer, PolicyType policyType) {
    this.customer = customer;
    this.policyType = policyType;
  }

  public double localDiscount(double orderTotal) {
    double discount = 0;

    if (customer.getCustomerDuration() <= 2) {
      discount = 0;
    } else if (customer.getCustomerDuration() <= 5) {
      discount = 0.075;
    } else {
      discount = 0.125;
    }
    return orderTotal * (1 - discount);
  }

  public double internationalDiscount(double orderTotal) {
    double discount = 0;
    if (orderTotal <= 500_000) {
      discount = 0;
    } else if (orderTotal <= 1_000_000) {
      discount = 0.05;
    } else {
      discount = 0.1;
    }
    return orderTotal * (1 - discount);
  }

  public PolicyType getPolicyType() {
    return policyType;
  }
}
