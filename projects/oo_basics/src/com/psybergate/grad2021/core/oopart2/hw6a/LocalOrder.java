package com.psybergate.grad2021.core.oopart2.hw6a;

public class LocalOrder extends Order {
  public LocalOrder(String orderNumber, String policy, int customerDuration) {
    super(orderNumber, policy, customerDuration);
  }

  @Override public double totalAfterDiscount() {
    return calcTotal() * (1 - applyPolicy());
  }
//
//  public double calcTotal() {
//    return (super.calcTotal() * (1 - applyPolicy()));
//  }

}
