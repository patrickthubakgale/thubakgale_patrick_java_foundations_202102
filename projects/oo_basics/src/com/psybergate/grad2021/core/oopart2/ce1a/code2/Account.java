package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class Account {
  private String accountNumber;
  private double balance;

  public Account(String accountNumber, double balance) {
    this.accountNumber = accountNumber;
    this.balance = balance;
    System.out.println("Balance is: " + this.balance);
    System.out.println("Account number is: " + this.accountNumber);
  }
}
