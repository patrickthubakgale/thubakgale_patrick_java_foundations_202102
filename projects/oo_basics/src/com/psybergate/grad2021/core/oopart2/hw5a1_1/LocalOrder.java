package com.psybergate.grad2021.core.oopart2.hw5a1_1;

public class LocalOrder extends Order {

  private final String ORDER_TYPE = "LOCAL";

  private double discount;

  public LocalOrder(String orderNumber, Customer customer, double discount) {
    super(orderNumber, customer);
    this.discount = discount;
  }

  public double calcTotal() {
    return super.calcTotal() * (1-discount);
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
