package com.psybergate.grad2021.core.oopart2.ce1a.code1;

public class CurrentAccount extends Account {

  //There is default constructor, we might just explicitly put it here
  public CurrentAccount() {
  }

  //This is the fix, but could also put default constructor in parent class and this would work.
//  public CurrentAccount(String accountNumber) {
//    super(accountNumber);
//  }
}

/*
1). We can put the default constructor and the code will compile.
2). We can declare a constructor and call the parent class to initialize instance variables.
 */