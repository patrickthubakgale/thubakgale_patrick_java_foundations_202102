package com.psybergate.grad2021.core.oopart2.hw4a;

public class EmployeeUtils {
  public static void main(String[] args) {
    Employee employee1 = new ContractedEmployee("David", 1000);

    System.out.println(ContractedEmployee.getSalaryMultiplier());
    System.out.println("employee1.calculateOvertime() = " + employee1.calculateOvertime());
  }
}
