package com.psybergate.grad2021.core.oopart2.hw5a2_1;

public class LocalOrder extends Order {

  private final String ORDER_TYPE = "LOCAL";

  public LocalOrder(String orderNumber, Customer customer) {
    super(orderNumber, customer);
  }

  public double applyDiscount(double orderTotal) {
    double discount = 0;

    if (getCustomer().getCustomerDuration() <= 2) {
      discount = 0;
    } else if (getCustomer().getCustomerDuration() > 2 &&
            getCustomer().getCustomerDuration() <= 5) {
      discount = 0.075;
    } else {
      discount = 0.125;
    }
    return orderTotal*(1-discount);
  }

  public double calcTotal() {
    return applyDiscount(super.calcTotal());
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
