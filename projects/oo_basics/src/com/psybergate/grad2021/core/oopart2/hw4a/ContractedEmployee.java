package com.psybergate.grad2021.core.oopart2.hw4a;

public class ContractedEmployee extends Employee {
  public ContractedEmployee(String name, double salary) {
    super(name, salary);
  }

  public static double getSalaryMultiplier() {
    return 0.5; //20%
  }
}
