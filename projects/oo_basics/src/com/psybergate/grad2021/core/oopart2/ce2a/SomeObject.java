package com.psybergate.grad2021.core.oopart2.ce2a;

public class SomeObject {
  private double value;

  public SomeObject(double value) {
    this.value = value;
  }

  public double getValue() {
    return value;
  }
}
