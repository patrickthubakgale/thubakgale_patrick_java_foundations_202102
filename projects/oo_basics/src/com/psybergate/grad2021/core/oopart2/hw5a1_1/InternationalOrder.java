package com.psybergate.grad2021.core.oopart2.hw5a1_1;

public class InternationalOrder extends Order {

  private final String ORDER_TYPE = "INTERNATIONAL";

  private double importDuties;

  public InternationalOrder(String orderNumber, Customer customer, double importDuties) {
    super(orderNumber, customer);
    this.importDuties = importDuties;
  }

  public double calcTotal() {
    return super.calcTotal() + importDuties;
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
