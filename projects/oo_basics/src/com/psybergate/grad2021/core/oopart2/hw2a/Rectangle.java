package com.psybergate.grad2021.core.oopart2.hw2a;

public class Rectangle {
  private static final double MAX_AREA = 15000;
  private static final double MAX_SIDE1 = 200;
  private static final double MAX_SIDE2 = 100;

  private double side1;
  private double side2;

  public Rectangle(double side1, double side2) {
    if (validateInputs(side1, side2)) {
      throw new RuntimeException("Violated constraints!");
    }
    this.side1 = side1;
    this.side2 = side2;
  }

  private static boolean validateInputs(final double side1, final double side2) {
    return (side1 > MAX_SIDE1 || side2 > MAX_SIDE2 || (side1 * side2) > MAX_AREA);
  }

  public double area() {
    return side1 * side2;
  }

  public static double getMaxArea() {
    return MAX_AREA;
  }

  public static double getMaxSide1() {
    return MAX_SIDE1;
  }

  public static double getMaxSide2() {
    return MAX_SIDE2;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null) return false;
    if (!(o instanceof Rectangle)) return false;
    Rectangle rectangle = (Rectangle) o;
    return this.side1 == rectangle.side1 && this.side2 == rectangle.side2;
  }
}
