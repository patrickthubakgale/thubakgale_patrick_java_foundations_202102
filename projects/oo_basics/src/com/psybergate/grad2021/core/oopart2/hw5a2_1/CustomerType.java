package com.psybergate.grad2021.core.oopart2.hw5a2_1;

public enum CustomerType {
  LOCAL, INTERNATIONAL
}
