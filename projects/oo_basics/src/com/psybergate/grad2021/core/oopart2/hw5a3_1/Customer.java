package com.psybergate.grad2021.core.oopart2.hw5a3_1;

import java.time.LocalDate;
import java.time.Period;

public class Customer {

  private String customerId;

  private String name;

  private CustomerType customerType;

  private LocalDate customerStartDate;

  public Customer(String customerId, String name, CustomerType customerType, LocalDate customerStartDate) {
    this.customerId = customerId;
    this.name = name;
    this.customerType = customerType;
    this.customerStartDate = customerStartDate;
  }

  public String getName() {
    return name;
  }

  public String getCustomerId() {
    return customerId;
  }

  public CustomerType getCustomerType() {
    return customerType;
  }

  public int getCustomerDuration() {
    Period period = Period.between(LocalDate.now(), customerStartDate);
    return Math.abs(period.getYears());
  }
}
