package com.psybergate.grad2021.core.oopart2.ce2a;

public class PassByValue {

  public void swap(SomeObject someObject1, SomeObject someObject2) {
    SomeObject temp = someObject1;
    someObject1 = someObject2;
    someObject2 = temp;
  }

  public void someMethod() {
    SomeObject a = new SomeObject (1);
    SomeObject b = new SomeObject (2);
    swap(a, b);
    System.out.println(a.getValue());
    System.out.println(b.getValue());
  }

  public static void main(String[] args) {
    PassByValue passByValue = new PassByValue();
    passByValue.someMethod();
  }
}
