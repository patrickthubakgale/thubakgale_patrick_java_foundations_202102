package com.psybergate.grad2021.core.oopart2.hw5a2_1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
  public static void main(String[] args) {
    List<Customer> customers = new ArrayList<>();
    List<Order> orders = new ArrayList<>();

    //Add customers
    customers.add(new Customer("Jan", "1", CustomerType.LOCAL, LocalDate.of(2017, 6,
            24)));
    customers.add(new Customer("Pieter-Steph", "2", CustomerType.LOCAL, LocalDate.of(2010, 11,
            10)));
    customers.add(new Customer("Wilfred", "3", CustomerType.INTERNATIONAL,LocalDate.of(2000, 3,
            13)));
    customers.add(new Customer("Rashid", "4", CustomerType.INTERNATIONAL, LocalDate.of(2019, 9,
            16)));

    //Add orders
    orders.add(new LocalOrder("42", customers.get(0)));
    orders.add(new LocalOrder("12", customers.get(1)));
    orders.add(new InternationalOrder("23", customers.get(2), 3000));
    orders.add(new InternationalOrder("10", customers.get(3), 5000));

    orders.get(0).addOrder(new Product(30, "socks"), 20);
    orders.get(0).addOrder(new Product(100, "shirt"), 50);
    orders.get(0).addOrder(new Product(500, "shoes"), 50);

    orders.get(1).addOrder(new Product(5, "apples"), 300);
    orders.get(1).addOrder(new Product(10, "bananas"), 150);
    orders.get(1).addOrder(new Product(20, "water"), 100);
    orders.get(1).addOrder(new Product(10, "strawberries"), 50);

    orders.get(2).addOrder(new Product(2000, "socks"), 100);
    orders.get(2).addOrder(new Product(1000, "shirt"), 100);
    orders.get(2).addOrder(new Product(2000, "trousers"), 100);
    orders.get(2).addOrder(new Product(3000, "shoes"), 100);
    orders.get(2).addOrder(new Product(5000, "jackets"), 50);

    orders.get(3).addOrder(new Product(5, "apples"), 300);
    orders.get(3).addOrder(new Product(10, "bananas"), 150);
    orders.get(3).addOrder(new Product(20, "water"), 100);
    orders.get(3).addOrder(new Product(10, "strawberries"), 50);
    orders.get(3).addOrder(new Product(10, "oranges"), 500);
    orders.get(3).addOrder(new Product(50, "mangoes"), 200);

    printCustomerTotals(orders);
  }

  public static void printCustomerTotals(List<Order> orders) {
    for (Order order : orders) {
      System.out.println(order.getCustomer().getName() + " has order totals " +
              "of " + order.calcTotal());
    }
  }
}