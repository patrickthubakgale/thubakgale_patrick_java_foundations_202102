package com.psybergate.grad2021.core.oopart2.ce3a;

public class Currency {

  private String name;

  private String currencyCode;

  public Currency(String name, String currencyCode) {
    this.name = name;
    this.currencyCode = currencyCode;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getName() {
    return name;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }
}
