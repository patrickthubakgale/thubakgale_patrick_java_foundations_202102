package com.psybergate.grad2021.core.oopart2.hw3a;

public class CustomerUtils {
  public static void main(String[] args) {
    Customer customer1 = new Person("John", "1234", "Midrand", 34);
    Customer customer2 = new Person("Thato", "1234", "Pretoria", 33);
    Customer customer3 = new Company("Absa", "8975", "Parktown");

    //Demo identity
    System.out.print("customer1 == customer2: ");
    System.out.println(customer1 == customer2);
    System.out.print("customer1.getCustomerNum() == customer2.getCustomerNum(): ");
    System.out.println(customer1.getCustomerNum() == customer2.getCustomerNum());

    //Demo Equality
    System.out.print("\ncustomer1.equals(customer2): ");
    System.out.println(customer1.equals(customer2));
    System.out.print("customer1.equals(customer3): ");
    System.out.println(customer1.equals(customer3));
  }
}