package com.psybergate.grad2021.core.oopart2.explore;

public class Overriding {
  protected String name;
  protected String surname;
  private double money;

  public Overriding(final String name, final String surname, final double money) {
    this.name = name;
    this.surname = surname;
    this.money = money;
  }

  private void setName(final String name) {
    this.name = name;
  }

  public void newName(final String name, final String surname) {
    setName(name);
    this.surname = surname;
  }

  public String getName() {
    return this.name;
  }
}
