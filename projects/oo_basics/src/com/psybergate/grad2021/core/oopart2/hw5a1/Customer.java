package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class Customer {
  private final String CUSTOMER_TYPE;
  private List<Order> orders = new ArrayList<>();

  public Customer(String customerType) {
    this.CUSTOMER_TYPE = customerType;
    //this.orders = orders;
  }

  public void makeOrder(Order order) {
    orders.add(order);
  }

  public void print() {
    System.out.println("Total cost for " + getCUSTOMER_TYPE() + " is " + getCustomerOrderTotals());
  }

  public double getCustomerOrderTotals() {
    double total = 0;
    for (Order order : orders) {
      total += order.calcTotal();
    }
    return total;
  }

  public String getCUSTOMER_TYPE() {
    return CUSTOMER_TYPE;
  }

  public List<Order> getOrders() {
    return orders;
  }
}
