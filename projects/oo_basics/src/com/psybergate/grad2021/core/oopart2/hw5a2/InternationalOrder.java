package com.psybergate.grad2021.core.oopart2.hw5a2;

public class InternationalOrder extends Order {

  private double importDuties;

  public InternationalOrder(String orderNumber, double importDuties) {
    super(orderNumber);
    this.importDuties = importDuties;
  }

  public double getDiscount() {
    double discount = 0;
    if (super.calcTotal() <= 500_000) {
      discount = 0;
    } else if (super.calcTotal() > 500_000 && super.calcTotal() <= 1_000_000) {
      discount = 0.05;
    } else {
      discount = 0.1;
    }
    return discount;
  }

  public double calcTotal() {
    return ((super.calcTotal() + importDuties) * (1 - getDiscount()));
  }
}
