package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class CurrentAccount extends Account{
  private final double MAX_OVERDRAFT = 100_000;
  private double overdraft;

  public CurrentAccount(String accountNumber, double balance, double overdraft) {
    super(accountNumber, balance);
    this.overdraft = overdraft;
    System.out.println("overdraft is: " + this.overdraft);
    System.out.println("overdraft is: " + this.MAX_OVERDRAFT);
  }
}
