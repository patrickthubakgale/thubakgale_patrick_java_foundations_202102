package com.psybergate.grad2021.core.oopart2.hw5a2_1;

public class InternationalOrder extends Order {

  private final String ORDER_TYPE = "INTERNATIONAL";

  private double importDuties;

  public InternationalOrder(String orderNumber, Customer customer, double importDuties) {
    super(orderNumber, customer);
    this.importDuties = importDuties;
  }

  public double applyDiscount(double orderTotal) {
    double discount = 0;
    if (orderTotal <= 500_000) {
      discount = 0;
    } else if (orderTotal > 500_000 && orderTotal <= 1_000_000) {
      discount = 0.05;
    } else {
      discount = 0.1;
    }
    return orderTotal*(1-discount);
  }

  public double calcTotal() {
    return applyDiscount(super.calcTotal()) + importDuties;
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
