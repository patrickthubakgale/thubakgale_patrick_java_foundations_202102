package com.psybergate.grad2021.core.oopart2.hw5a1_1;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
  public static void main(String[] args) {
    List<Customer> customers = new ArrayList<>();
    List<Order> orders = new ArrayList<>();

    //Add customers
    customers.add(new Customer("Jan", "1", CustomerType.LOCAL));
    customers.add(new Customer("Pieter-Steph", "2", CustomerType.LOCAL));
    customers.add(new Customer("Wilfred", "3", CustomerType.INTERNATIONAL));
    customers.add(new Customer("Rashid", "4", CustomerType.INTERNATIONAL));

    //Add orders
    orders.add(new LocalOrder("42", customers.get(0), 0.1));
    orders.add(new LocalOrder("12", customers.get(1), 0.05));
    orders.add(new InternationalOrder("23", customers.get(2), 3000));
    orders.add(new InternationalOrder("10", customers.get(3), 5000));

    orders.get(0).addOrder(new Product(30, "socks"), 20);
    orders.get(0).addOrder(new Product(100, "shirt"), 50);
    orders.get(0).addOrder(new Product(500, "shoes"), 50);

    orders.get(1).addOrder(new Product(5, "apples"), 300);
    orders.get(1).addOrder(new Product(10, "bananas"), 150);
    orders.get(1).addOrder(new Product(20, "water"), 100);
    orders.get(1).addOrder(new Product(10, "strawberries"), 50);

    orders.get(2).addOrder(new Product(20, "socks"), 30);
    orders.get(2).addOrder(new Product(100, "shirt"), 100);
    orders.get(2).addOrder(new Product(200, "trousers"), 100);
    orders.get(2).addOrder(new Product(300, "shoes"), 100);
    orders.get(2).addOrder(new Product(500, "jackets"), 50);

    orders.get(3).addOrder(new Product(5, "apples"), 300);
    orders.get(3).addOrder(new Product(10, "bananas"), 150);
    orders.get(3).addOrder(new Product(20, "water"), 100);
    orders.get(3).addOrder(new Product(10, "strawberries"), 50);
    orders.get(3).addOrder(new Product(10, "oranges"), 500);
    orders.get(3).addOrder(new Product(50, "mangoes"), 200);

    printCustomerTotals(orders);
  }

  public static void printCustomerTotals(List<Order> orders) {
    for (Order order : orders) {
      System.out.println(order.getCustomer().getName() + " has order totals " +
              "of " + order.calcTotal());
    }
  }
}