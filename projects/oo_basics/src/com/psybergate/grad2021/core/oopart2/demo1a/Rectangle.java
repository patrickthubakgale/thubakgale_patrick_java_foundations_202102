package com.psybergate.grad2021.core.oopart2.demo1a;

public class Rectangle extends Shape {
  private final String TYPE = "Rectangle";
  private double side1;
  private double side2;
/*
* Note:
* Constructors have same name as class name.
* This makes it impossible for child class to inherit constructor of parent class due to name
* clashes.
* If we define Parent class constructor inside Child class it will give compile time error for
* return type and consider it a method.
* In essence we prevent:  Child child = new Parent();
 * */

  //Trying to override Shape constructor
//  public Shape() {
//    System.out.println("I am Shape in Rectangle");
//  }
//
  public Rectangle(double side1, double side2) {
    this.side1 = side1;
    this.side2 = side2;
  }

  @Override
  public String getType() {
    return TYPE;
  }

  //This method does not override callMe(final String name) method
  //from parent class. It shadows/hides it.
  public static void callMe(final String name) {
    System.out.println("New name is: " + name);
  }


  @Override
  public double getArea() {
    return side1 * side2;
  }

  @Override
  public String dimensions() {
    return "side1 is: " + side1 + ", side2 is: " + side2;
  }
}