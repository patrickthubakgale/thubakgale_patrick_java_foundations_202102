package com.psybergate.grad2021.core.oopart2.demo2a;

import java.util.Objects;

public abstract class Account {
  private String accountNum;
  protected double balance;

  public Account(final String accountNum, final double balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public double getBalance() {
    return this.balance;
  }

  public String getAccountNum() {
    return this.accountNum;
  }

  public abstract void deposit(final double amount);

  public abstract void withdraw(final double amount);
}
