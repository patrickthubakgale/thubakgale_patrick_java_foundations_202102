package com.psybergate.grad2021.core.oopart2.hw4a;

public class Employee {
  private String name;
  private double salary;

  public Employee(String name, double salary) {
    this.name = name;
    this.salary = salary;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }

  public static double getSalaryMultiplier() {
    return 0.2; //20%
  }

  public double calculateOvertime() {
    return this.salary * getSalaryMultiplier();
  }
}
