package com.psybergate.grad2021.core.oopart2.hw5a2_1;

import java.time.LocalDate;
import java.time.Period;

public class Customer {

  private String name;

  private String customerId;

  private CustomerType customerType;

  private LocalDate customerStartDate;

  public Customer(String name, String customerId, CustomerType customerType, LocalDate customerStartDate) {
    this.name = name;
    this.customerId = customerId;
    this.customerType = customerType;
    this.customerStartDate = customerStartDate;
  }

  public String getName() {
    return name;
  }

  public String getCustomerId() {
    return customerId;
  }

  public CustomerType getCustomerType() {
    return customerType;
  }

  public int getCustomerDuration() {
    Period period = Period.between(java.time.LocalDate.now(), customerStartDate);
    return Math.abs(period.getYears());
  }
}
