package com.psybergate.grad2021.core.oopart2.demo2a;

public class SavingsAccount extends Account {
  private final double minBalance = 1000;
  private double overdraft;

  public SavingsAccount(final String accountNum, final double balance, final double overdraft) {
    super(accountNum, balance);
    this.overdraft = overdraft;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null) return false;
    if (!(o instanceof SavingsAccount)) return false;
    SavingsAccount that = (SavingsAccount) o;
    return this.getBalance() == that.getBalance();
  }

  @Override
  public void deposit(final double amount) {
    this.balance = amount;
  }

  @Override
  public void withdraw(final double amount) {
    this.balance -= amount;
  }
}
