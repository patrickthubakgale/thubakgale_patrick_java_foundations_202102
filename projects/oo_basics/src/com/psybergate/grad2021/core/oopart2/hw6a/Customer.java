package com.psybergate.grad2021.core.oopart2.hw6a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
  private final String CUSTOMER_TYPE;
  private List<Order> orders = new ArrayList<>();

  public Customer(String CUSTOMER_TYPE) {
    this.CUSTOMER_TYPE = CUSTOMER_TYPE;
    //this.orders = orders;
  }

  public void makeOrder(Order order) {
    orders.add(order);
  }

  public double getCustomerOrderTotals() {
    double total = 0;
    for (Order order : orders) {
      total += order.totalAfterDiscount();
    }
    return total;
  }

  public String getCUSTOMER_TYPE() {
    return CUSTOMER_TYPE;
  }

  public List<Order> getOrders() {
    return orders;
  }
}