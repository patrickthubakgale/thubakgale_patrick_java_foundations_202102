package com.psybergate.grad2021.core.oopart2.hw5a1_1;

import java.util.ArrayList;
import java.util.List;

public abstract class Order {

  private String orderNumber;

  private Customer customer;

  private final String ORDER_TYPE = "ORDER";

  List<OrderItem> orderItems = new ArrayList<>();

  public Order(String orderNumber, Customer customer) {
    this.orderNumber = orderNumber;
    this.customer = customer;
  }

  public void addOrder(Product product, int quantity) {
    if (getCustomer().getCustomerType() == CustomerType.LOCAL &&
            getOrderType().equalsIgnoreCase("local")) {
      orderItems.add(new OrderItem(product, quantity));
    }
    else if (getCustomer().getCustomerType() == CustomerType.INTERNATIONAL &&
            getOrderType().equalsIgnoreCase("international")) {
      orderItems.add(new OrderItem(product, quantity));
    }
    else {
      throw new RuntimeException("Order type does not match customer type");
    }
  }

  public double calcTotal() {
    double total = 0;
    for (OrderItem orderItem : orderItems) {
      total += (orderItem.getProduct().getPrice() * orderItem.getQuantity());
    }
    return total;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public Customer getCustomer() {
    return customer;
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
