package com.psybergate.grad2021.core.oopart2.hw5a1;

public class InternationalOrder extends Order {
  private double importDuties;

  public InternationalOrder(String orderNumber, double importDuties) {
    super(orderNumber);
    this.importDuties = importDuties;
  }

  public double calcTotal() {
    return super.calcTotal() + importDuties;
  }
}
