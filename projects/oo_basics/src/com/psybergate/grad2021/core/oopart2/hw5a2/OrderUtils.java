package com.psybergate.grad2021.core.oopart2.hw5a2;

public class OrderUtils {
  public static void main(String[] args) {
    Customer customer1 = new Customer("LocalCustomer");
    Customer customer2 = new Customer("LocalCustomer");
    Customer customer3 = new Customer("InternationalCustomer");
    Customer customer4 = new Customer("InternationalCustomer");

    Order order1 = new InternationalOrder("123", 5000);
    Order order2 = new LocalOrder("230", 1);
    Order order3 = new LocalOrder("231", 2);
    Order order4 = new LocalOrder("232", 3);

    order1.addOrder("Apples", 5, 200);
    order1.addOrder("Strawberries", 10, 100);
    order1.addOrder("Banana", 3, 200);

    order2.addOrder("Shoes", 50, 10);
    order2.addOrder("Socks", 10, 20);
    order2.addOrder("Trousers", 100, 10);
    order2.addOrder("Cigarette", 50, 500);

    order3.addOrder("Biscuits", 10, 100);
    order3.addOrder("Coke", 10, 300);
    order3.addOrder("Pillows", 40, 150);
    order3.addOrder("Sim Card", 1, 300);
    order3.addOrder("Router", 200, 500);

    order4.addOrder("Biscuits", 10, 100);
    order4.addOrder("Coke", 10, 300);
    order4.addOrder("Pillows", 40, 150);
    order4.addOrder("Sim Card", 1, 300);
    order4.addOrder("Router", 200, 500);
    order4.addOrder("Router", 200, 500);

    customer1.makeOrder(order1);
    customer2.makeOrder(order2);
    customer3.makeOrder(order3);
    customer4.makeOrder(order4);

    printCustomerTotals(customer1);
    printCustomerTotals(customer2);
    printCustomerTotals(customer3);
    printCustomerTotals(customer4);
  }

  public static void printCustomerTotals(Customer customer) {
    System.out.println("Total cost for " + customer.getCUSTOMER_TYPE() + " is " + customer.getCustomerOrderTotals());
  }
}
