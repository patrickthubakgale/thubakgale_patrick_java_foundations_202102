package com.psybergate.grad2021.core.oopart2.hw3a;

public abstract class Customer {
  private String name;
  private String customerNum;
  private String getCustomerAddress;

  public Customer(final String name, final String customerNum, final String getCustomerAddress) {
    this.name = name;
    this.customerNum = customerNum;
    this.getCustomerAddress = getCustomerAddress;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null) return false;
    if (!(o instanceof Customer)) return false;
    Customer customer = (Customer) o;
    return this.customerNum == customer.customerNum;
  }
}
