package com.psybergate.grad2021.core.oopart2.hw5a2;

public class Product {
  private double price;
  private String name;

  public Product(double price, String name) {
    this.price = price;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }
}
