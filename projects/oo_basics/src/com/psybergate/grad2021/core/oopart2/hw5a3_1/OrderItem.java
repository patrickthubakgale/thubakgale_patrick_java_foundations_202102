package com.psybergate.grad2021.core.oopart2.hw5a3_1;

public class OrderItem {

  private Product product;

  private int quantity;

  public OrderItem(Product product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public Product getProduct() {
    return product;
  }

  public int getQuantity() {
    return quantity;
  }
}
