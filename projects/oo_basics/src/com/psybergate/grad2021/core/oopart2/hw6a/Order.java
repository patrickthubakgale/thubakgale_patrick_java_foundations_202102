package com.psybergate.grad2021.core.oopart2.hw6a;

import java.util.ArrayList;
import java.util.List;

public abstract class Order {
  private String orderNumber;
  List<OrderItem> orderItems = new ArrayList<>();
  private String policy;
  private int customerDuration;
  private double discount;

  public Order(String orderNumber, String policy, int customerDuration) {
    this.orderNumber = orderNumber;
    //this.orderItems = orderItems;
    this.policy = policy;
    this.customerDuration = customerDuration;
  }

  public void addOrder(final String name, final double price, final int quantity) {
    orderItems.add(new OrderItem(quantity, new Product(price, name)));
  }

  public abstract double totalAfterDiscount();

  public double calcTotal() {
    double total = 0;
    for (OrderItem orderItem : orderItems) {
      total += (orderItem.getProduct().getPrice() * orderItem.getQuantity());
    }
    return total;
  }

  public String getPolicy() {
    return policy;
  }

  public int getCustomerDuration() {
    return customerDuration;
  }

  public double applyPolicy() {
    if (getPolicy().equalsIgnoreCase("Local")) {
      return getLocalDiscount();
    } else {
      return getInternationalDiscount();
    }
  }

  private double getInternationalDiscount() {
    if (calcTotal() <= 500_000) {
      discount = 0;
    } else if (calcTotal() > 500_000 && calcTotal() <= 1_000_000) {
      discount = 0.05;
    } else {
      discount = 0.1;
    }
    return discount;
  }

  private double getLocalDiscount() {
    if (customerDuration <= 2) {
      discount = 0;
    } else if (customerDuration > 2 && customerDuration <= 5) {
      discount = 0.075;
    } else {
      discount = 0.12;
    }
    return discount;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public String getOrderNumber() {
    return orderNumber;
  }
}