package com.psybergate.grad2021.core.oopart2.hw6a;

public class OrderItem {
  private int quantity;
  private Product product;

  public OrderItem(int quantity, Product product) {
    this.quantity = quantity;
    this.product = product;
  }

  public int getQuantity() {
    return quantity;
  }

  public Product getProduct() {
    return product;
  }
}
