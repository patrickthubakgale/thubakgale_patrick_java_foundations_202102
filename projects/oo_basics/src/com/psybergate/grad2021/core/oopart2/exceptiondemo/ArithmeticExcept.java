package com.psybergate.grad2021.core.oopart2.exceptiondemo;

public class ArithmeticExcept {

  public static double calculate(double numerator, double denominator) throws ArithmeticException {
    return numerator/denominator;
  }

  public static void main(String[] args) {
    try {
      double result = calculate(200, 0);
      System.out.println("result = " + result);
    } catch (ArithmeticException e) {
      e.printStackTrace();
    }
  }
}
