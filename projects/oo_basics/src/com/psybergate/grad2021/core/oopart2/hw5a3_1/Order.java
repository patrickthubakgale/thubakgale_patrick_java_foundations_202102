package com.psybergate.grad2021.core.oopart2.hw5a3_1;

import java.util.ArrayList;
import java.util.List;

public abstract class Order {

  private final String ORDER_TYPE = "ORDER";

  private String orderNumber;

  private Customer customer;

  private DiscountPolicy discountPolicy;

  List<OrderItem> orderItems = new ArrayList<>();

  public Order(String orderNumber, Customer customer, DiscountPolicy discountPolicy) {
    validation(customer);
    this.orderNumber = orderNumber;
    this.customer = customer;
    this.discountPolicy = discountPolicy;
  }

  public void addOrderItem(Product product, int quantity) {
    orderItems.add(new OrderItem(product, quantity));
  }

  private void validation(Customer customer) {
    if (customer.getCustomerType().equals(CustomerType.LOCAL) &&
            getOrderType().equalsIgnoreCase("local")) {
      System.out.println("local customer, local order");
    }
    else if (customer.getCustomerType().equals(CustomerType.INTERNATIONAL) &&
            getOrderType().equalsIgnoreCase("international")) {
      System.out.println("International customer, international order");
    }
    else {
      throw new RuntimeException("Order type does not match customer type");
    }
  }

  public double calcTotal() {

    double total = 0;

    for (OrderItem orderItem : orderItems) {
      total += (orderItem.getProduct().getPrice() * orderItem.getQuantity());
    }

    if (discountPolicy.getPolicyType() == PolicyType.LOCAL) {
      return discountPolicy.localDiscount(total);
    }
    else if (discountPolicy.getPolicyType() == PolicyType.INTERNATIONAL) {
      return discountPolicy.internationalDiscount(total);
    }
    else {
      throw new RuntimeException("Unknown discount policy");
    }
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public Customer getCustomer() {
    return customer;
  }

  public String getOrderType() {
    return ORDER_TYPE;
  }
}
