package com.psybergate.grad2021.core.oopart2.hw2a;

public class RectangleUtils {
  public static void main(String[] args) {
    Rectangle rectangle1 = new Rectangle(34, 23);
    Rectangle rectangle2 = new Rectangle(34, 23);

    System.out.println(rectangle1 == rectangle2); //using identity
    System.out.println(rectangle1.equals(rectangle2)); //using equality
  }

}
