package com.psybergate.grad2021.core.oopart1.ce2a;

public class Test {
  public static void main(String[] args) {
    Account account1 = new CurrentAccount("1234", "FNB", 10000, 10000);
    Account account2  = new CurrentAccount("3467", "FNB", 20000, 5000);
    Account account3 = new CurrentAccount("9876", "FNB", -20000, 5000);

    System.out.println("Account needs to be reviewed: " + account1.needsToBeReviewed());
    System.out.println("Account type of account2 is: " + account2.getAccountType());
    System.out.println("Balance of " + account3.getName() + " is : " + account3.getBalance());
    System.out.println("Account " + account3.getName() + " should be reviewed? " + account3.needsToBeReviewed());
  }
}
