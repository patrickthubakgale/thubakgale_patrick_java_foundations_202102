package com.psybergate.grad2021.core.oopart1.hw5a;

import java.util.List;

public class Customer {
  private String name;
  private String customerNum;
  private List<Account> accounts;

  public Customer(final String name, final String customerNum, final List<Account> accounts) {
    this.name = name;
    this.customerNum = customerNum;
    this.accounts = accounts;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getCustomerNum() {
    return this.customerNum;
  }

  public String getName() {
    return this.name;
  }

  public List<Account> getAccounts() {
    return accounts;
  }
}
