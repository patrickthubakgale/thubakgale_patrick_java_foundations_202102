package com.psybergate.grad2021.core.oopart1.hw4a2;

public class CustomerUtils {
  public static void main(String[] args) {
    Customer customer1 = new Company("Nedbank", "1234", "Pretoria");
    Customer customer2 = new Person("Peter", "0584", "Midrand", 23);
    Customer customer3 = new Person("Tom", "1001", "Parktown", 23);

    System.out.println("customer1.getCustomerNum() = " + customer1.getCustomerNum());
  }
}
