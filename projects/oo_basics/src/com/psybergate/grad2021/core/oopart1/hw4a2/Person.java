package com.psybergate.grad2021.core.oopart1.hw4a2;

public class Person extends Customer {
  private static final int MINIMUM_AGE = 18;
  private int age;

  public Person(final String name, final String customerNum, final String customerAddress,
                final int age) {
    super(name, customerNum, customerAddress);

    if (validateAge(age)) {
      throw new RuntimeException("You are a minor");
    }
    this.age = age;
  }

  public static boolean validateAge(final int age) {
    return age <= 18;
  }

  public void setAge(final int age) {
    this.age = age;
  }

  public int getAge() {
    return age;
  }

  public static int getMinimumAge() {
    return MINIMUM_AGE;
  }
}

