package com.psybergate.grad2021.core.oopart1.ce2a;

public class CurrentAccount extends Account {
  private final double MAX_OVERDRAFT = 100_000;
  private final String ACC_TYPE = "CurrentAccount";
  private double overdraft;

  public CurrentAccount(String accountNum, String name, double balance, double overdraft) {
    super(accountNum, name, balance);
    if (isOverMaxOverdraft(overdraft)) {
      this.overdraft = 5000;
    }
    this.overdraft = overdraft;
  }

  private boolean isOverMaxOverdraft(final double overdraft) {
    return overdraft > this.MAX_OVERDRAFT;
  }

  public boolean needsToBeReviewed() {
    return (getBalance() < -((1.2) * overdraft) || getBalance() <= -50_000);
  }

  public boolean isOverdrawn() {
    return getBalance() < -(overdraft);
  }

  public String getAccountType() {
    return ACC_TYPE;
  }
}