package com.psybergate.grad2021.core.oopart1.demo2a;

public class Triangle extends Shape{
  private final String TYPE = "Triangle";
  private double side1;
  private double side2;

  public Triangle(double side1, double side2) {
    this.side1 = side1;
    this.side2 = side2;
  }

  @Override
  public String getType() {
    return TYPE;
  }

  @Override
  public double getArea() {
    return 0.5 * side1 * side2;
  }

  @Override
  public String dimensions() {
    return "side1 is: " + side1 + ", side2 is: " + side2;
  }
}
