package com.psybergate.grad2021.core.oopart1.ce4a2;

public class CurrentAccount extends Account {
  private final double MAX_OVERDRAFT = 100_000;
  private final String ACC_TYPE = "CurrentAccount";
  private double overdraft;

  public CurrentAccount(final String accountNumber, final String name, final double balance, final double overdraft) {
    super(accountNumber, name, balance);
    this.overdraft = overdraft;
  }

  public String getAccountType() {
    return ACC_TYPE;
  }

  public boolean isOverdrawn() {
    return getBalance() < -(overdraft);
  }

  public boolean needsToBeReviewed() {
    return getBalance() < -(1.2 * overdraft) || getBalance() <= -(50_000);
  }
}