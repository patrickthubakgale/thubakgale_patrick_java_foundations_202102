package com.psybergate.grad2021.core.oopart1.demo2a;

import java.util.ArrayList;
import java.util.List;

public class ShapeUtils {
  public static void main(String[] args) {
    Shape shape1 = new Circle(5);
    Shape shape2 = new Rectangle(2, 4);
    Shape shape3 = new Triangle(3, 5);
    List<Shape> shapes = new ArrayList<>();
    shapes.add(shape1);
    shapes.add(shape2);
    shapes.add(shape3);
    printShapes(shapes);

    shape1.print();
    shape2.print();
    shape3.print();
  }

  public static void printShapes(List<Shape> shapes) {
    for (Shape shape : shapes) {
      System.out.println(shape.getType());
    }
  }
}
