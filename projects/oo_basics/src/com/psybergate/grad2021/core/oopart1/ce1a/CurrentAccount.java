package com.psybergate.grad2021.core.oopart1.ce1a;

public class CurrentAccount {
  private String accountNumber;
  private double balance;

  public CurrentAccount(final String accountNumber, final double balance) {
    this.accountNumber = accountNumber;
    this.balance = balance;
  }

  public double getBalance() {
    return balance;
  }
}
