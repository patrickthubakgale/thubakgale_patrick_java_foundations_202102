package com.psybergate.grad2021.core.oopart1.hw5a;

public class SavingsAccount extends Account{
  private final double minBalance = 1000;
  private double overdraft;

  public SavingsAccount(final String accountNum, final double balance, final double overdraft) {
    super(accountNum, balance);
    this.overdraft = overdraft;
  }

  @Override
  public void deposit(final double amount) {
    this.balance = amount;
  }

  @Override
  public void withdraw(final double amount) {
    this.balance -= amount;
  }
}
