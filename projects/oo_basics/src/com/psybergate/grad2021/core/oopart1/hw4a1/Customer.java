package com.psybergate.grad2021.core.oopart1.hw4a1;

public abstract class Customer {
  private String name;
  private String customerNum;
  private String getCustomerAddress;

  public Customer(final String name, final String customerNum, final String getCustomerAddress) {
    this.name = name;
    this.customerNum = customerNum;
    this.getCustomerAddress = getCustomerAddress;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public String getCustomerNum() {
    return customerNum;
  }
}
