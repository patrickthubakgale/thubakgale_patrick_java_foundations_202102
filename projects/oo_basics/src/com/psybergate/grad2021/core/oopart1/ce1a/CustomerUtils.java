package com.psybergate.grad2021.core.oopart1.ce1a;

public class CustomerUtils {

  public static void main(String[] args) {
    Customer customer = new Customer("Magnus", "1234", "Newtown");
    customer.addCurrentAccount(new CurrentAccount("0000", 60));
    customer.addCurrentAccount(new CurrentAccount("0001", 30));
    customer.addCurrentAccount(new CurrentAccount("0002", 50));
    test(customer);
  }

  public static void test(final Customer customer) {
    System.out.println("Customer with name " + customer.getName() + " has balance of " + customer.getTotal());
  }
}
