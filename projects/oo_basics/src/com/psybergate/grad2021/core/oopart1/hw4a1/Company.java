package com.psybergate.grad2021.core.oopart1.hw4a1;

public class Company extends Customer{
  public Company(final String name, final String customerNum,
                 final String customerAddress) {
    super(name, customerNum, customerAddress);
  }
}
