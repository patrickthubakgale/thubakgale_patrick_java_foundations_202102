package com.psybergate.grad2021.core.oopart1.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
  private String name;
  private String customerNumber;
  private String customerAddress;
  private List<CurrentAccount> accounts = new ArrayList<>();

  public Customer(final String name, final String customerNumber, final String customerAddress) {
    this.name = name;
    this.customerNumber = customerNumber;
    this.customerAddress = customerAddress;
  }

  public void addCurrentAccount(final CurrentAccount account) {
    accounts.add(account);
  }

  public List<CurrentAccount> getAccounts() {
    return accounts;
  }

  public String getName() {
    return name;
  }

  public double getTotal() {
    double totalBalance = 0;
    for (CurrentAccount account : getAccounts()) {
      totalBalance += account.getBalance();
    }
    return totalBalance;
  }
}
