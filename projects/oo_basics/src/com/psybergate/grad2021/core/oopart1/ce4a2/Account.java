package com.psybergate.grad2021.core.oopart1.ce4a2;

//import java.util.ArrayList;
//import java.util.List;

public abstract class Account {
  private final String accountNumber;
  private final String ACC_TYPE = "Account";
  private String name;
  protected double balance;

  protected Account(final String accountNumber, final String name, final double balance) {
    this.accountNumber = accountNumber;
    this.name = name;
    this.balance = balance;
  }

  public String getAccountType() {
    return ACC_TYPE;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(final double balance) {
    this.balance = balance;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public abstract boolean needsToBeReviewed();

  public abstract boolean isOverdrawn();
}
