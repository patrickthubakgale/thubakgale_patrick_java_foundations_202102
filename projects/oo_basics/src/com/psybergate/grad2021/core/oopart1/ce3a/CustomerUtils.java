package com.psybergate.grad2021.core.oopart1.ce3a;

public class CustomerUtils {

  public static void main(String[] args) {
    Customer customer = new Customer("Magnus", "1234", "Newtown");
    customer.addCurrentAccount(new CurrentAccount("0000", "FNB", 1200, 5000));
    customer.addCurrentAccount(new CurrentAccount("0201", "Capitec", 1000,9000));
    customer.addCurrentAccount(new CurrentAccount("0110", "Nedbank", 2000,10000));
    test(customer);

    //get the element out and mutate it
    CurrentAccount currentAccount = customer.getAccounts().get(0);
    customer.getAccounts().remove(0);
    currentAccount.setBalance(1000); //set new balance

    test(customer);

    //put the object back
    customer.addCurrentAccount(currentAccount);
    test(customer);
  }

  public static void test(final Customer customer) {
    System.out.println("Customer with name " + customer.getName() + " has balance of " + customer.getTotal());
  }
}
