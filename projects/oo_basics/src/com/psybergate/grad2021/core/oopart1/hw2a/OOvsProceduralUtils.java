package com.psybergate.grad2021.core.oopart1.hw2a;

import static com.psybergate.grad2021.core.oopart1.hw2a.MyDatePP.updateDays;

public class OOvsProceduralUtils {
  public static void main(String[] args) {
    //OO demo
    MyDateOO myDateOO = new MyDateOO(11, 12, 2000);
    System.out.println("Current date: " + myDateOO);
    myDateOO.updateDays(3);
    myDateOO.updateDays(3);
    myDateOO.updateDays(3);

    System.out.println("Date after update: " + myDateOO);

    //check if date is valid
    System.out.print("Is my date valid: " + myDateOO.isValidDate(54, 11, 2000) +"\n\n");

    //Procedural demo
    MyDatePP myDatePP = new MyDatePP(11, 23, 2001);
    MyDatePP myDatePP1 = new MyDatePP(11, 23, 2001);
    System.out.println("Current date: " + myDatePP);
    updateDays(myDatePP, 7);
    updateDays(myDatePP, 7);
    updateDays(myDatePP, 7);
    System.out.println("Date after update: " + myDatePP);
    System.out.print("Is my date valid: " + myDateOO.isValidDate(30, 11, 2000) + "\n\n");

    //Check if two dates are equal
    System.out.println(isDateEqual(myDateOO, myDatePP));
  }

  public static int isDateEqual(MyDateOO myDateOO, MyDatePP myDatePP) {
    if (myDateOO.getYear() > myDatePP.getYear()) {
      return 1;
    } else if (myDateOO.getMonth() > myDatePP.getMonth()) {
      return 1;
    } else if (myDateOO.getDay() > myDatePP.getDay()) {
      return 1;
    } else if (myDateOO.getYear() == myDatePP.getYear() && myDateOO.getMonth() ==
            myDatePP.getMonth() && myDateOO.getDay() == myDatePP.getDay()) {
      return 0;
    } else {
      return -1;
    }
  }
}
