package com.psybergate.grad2021.core.oopart1.ce3b;

public class ClassA {
  private String name;
  private String ssn;
  private double salary;

  public ClassA() {}; //just for demonstration

  public ClassA(String name, String ssn) {
    this.name = name;
    this.ssn = ssn;
    this.salary = 10_000;
  }

  public ClassA(String name, String ssn, double salary) {
    this(name, ssn);
    this.salary = salary;
  }

  public double doSomething(int i) {
    return salary * i;
  }

  public String toString() {
    return String.format("%s with social security number %s has $%.2f in their account", name, ssn, salary);
  }
}
