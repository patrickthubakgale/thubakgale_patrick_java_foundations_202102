package com.psybergate.grad2021.core.oopart1.hw5a;

import java.util.ArrayList;
import java.util.List;

public class CustomerUtils {
  public static void main(String[] args) {
    List<Account> accounts = new ArrayList<>();
    accounts.add(new CurrentAccount("0001", 1200, 1000));
    accounts.add(new SavingsAccount("0002", 5000, 1000));
    accounts.add(new SavingsAccount("0003", 4500, 1000));
    Customer customer = new Customer("John", "1234", accounts);

    printBalances(customer);
  }

  public static void printBalances(Customer customer) {
    for (Account account : customer.getAccounts()) {
      System.out.println("CustomerNum is " + customer.getCustomerNum() + ", AccountNum is " +
            account.getAccountNum() + ", balance is " +
            account.getBalance());
    }
    }
}
