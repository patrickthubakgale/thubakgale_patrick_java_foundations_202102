package com.psybergate.grad2021.core.oopart1.ce4a1;

public abstract class Account {
  private final String ACC_TYPE = "Account";
  private final String accountNum;
  private String name;
  protected double balance;

  protected Account(final String accountNum, final String name, final double balance) {
    this.accountNum = accountNum;
    this.name = name;
    this.balance = balance;
  }

  public String getAccountType() {
    return ACC_TYPE;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(final double balance) {
    this.balance = balance;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public abstract boolean isOverdrawn();

  public abstract boolean needsToBeReviewed();
}