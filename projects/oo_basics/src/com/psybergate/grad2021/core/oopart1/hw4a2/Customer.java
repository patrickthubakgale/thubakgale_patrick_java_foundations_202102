package com.psybergate.grad2021.core.oopart1.hw4a2;

public class Customer {
  private String name;
  private String customerNum;
  private String customerAddress;

  public Customer(final String name, final String customerNum, final String customerAddress) {
    this.name = name;
    this.customerNum = customerNum;
    this.customerAddress = customerAddress;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getCustomerAddress() {
    return customerAddress;
  }
}
