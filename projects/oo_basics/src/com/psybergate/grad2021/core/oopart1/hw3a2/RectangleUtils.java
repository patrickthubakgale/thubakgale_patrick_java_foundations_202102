package com.psybergate.grad2021.core.oopart1.hw3a2;

public class RectangleUtils {
  public static void main(String[] args) {
    Rectangle rectangle1 = new Rectangle(100, 100);
    Rectangle rectangle2 = new Rectangle(150, 900);
    Rectangle rectangle3 = new Rectangle(200, 140);
    System.out.println(rectangle1.area());
    System.out.println(rectangle2.area());
    System.out.println(rectangle3.area());
  }
}

//Object state is different
//They all have same type (i.e. Rectangle) and same constraints.