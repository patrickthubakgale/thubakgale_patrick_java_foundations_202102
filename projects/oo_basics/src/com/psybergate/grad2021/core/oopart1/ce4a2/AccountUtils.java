package com.psybergate.grad2021.core.oopart1.ce4a2;

import java.util.ArrayList;
import java.util.List;

public class AccountUtils {
  public static void main(String[] args) {
    Account account1 = new CurrentAccount("0000", "FNB", -6000, 5000);
    Account account2 = new CurrentAccount("0001", "ABSA", 15000, 5000);
    Account account3 = new SavingsAccount("0002", "Standard", 20000, 4500);
    Account account4 = new SavingsAccount("0003", "Investec", 1000, 1500);

    List<Account> accountList = new ArrayList<>();
    accountList.add(account1);
    accountList.add(account2);
    accountList.add(account3);
    accountList.add(account4);
    print(accountList);
  }

  private static void print(List<Account> accountList) {
    for (Account account : accountList) {
      System.out.println("Needs to be reviewed: " + account.needsToBeReviewed());
      System.out.println("Is overdrawn(): " + account.isOverdrawn());
    }
  }
}
