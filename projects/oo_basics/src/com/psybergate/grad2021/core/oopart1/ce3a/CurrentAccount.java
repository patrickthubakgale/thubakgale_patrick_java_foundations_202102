package com.psybergate.grad2021.core.oopart1.ce3a;

public class CurrentAccount extends Account {
  private final double MAX_OVERDRAFT = 100_000;
  private final String ACC_TYPE = "CurrentAccount";
  private double overdraft;

  public CurrentAccount(final String accountNum, final String surname, final double balance, final double overdraft) {
    super(accountNum, surname, balance);
    if (isOverMaxOverdraft(overdraft)) {
      this.overdraft = 5000;
    }
    this.overdraft = overdraft;
  }

  private boolean isOverMaxOverdraft(final double overdraft) {
    return overdraft > this.MAX_OVERDRAFT;
  }

  public String getAccountType() {
    return ACC_TYPE;
  }

  public boolean isOverdrawn() {
    return getBalance() < -(overdraft);
  }

  public boolean needsToBeReviewed() {
    return (getBalance() < -(1.2 * overdraft)) || getBalance() <= -50_000;
  }
}
