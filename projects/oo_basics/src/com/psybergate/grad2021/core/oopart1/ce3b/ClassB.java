package com.psybergate.grad2021.core.oopart1.ce3b;

public class ClassB extends ClassA {
  private double rate;

  public ClassB(String name, String ssn, double salary, double rate) {
    super(name, ssn, salary);
    this.rate = rate;
  }

  @Override
  public double doSomething(int i) {
    return super.doSomething(i) + rate;
  }

  //Make things up!
  private int number;
  public ClassB() {number = 1;};

  public ClassB multiply(int number) {
    this.number *= number;
    return this;
  }

  public ClassB add(int number) {
    this.number += number;
    return this;
  }

  public int getNumber() {
    return number;
  }

  public static void main(String[] args) {
    int number1 = new ClassB().multiply(3).add(3).getNumber();
    System.out.println("The updated number is " + number1);

    //demo doSomething
    ClassA classB = new ClassB("Thato", "4444", 500, 100);
    System.out.println("classB.doSomething(2) = " + classB.doSomething(2));
    System.out.println("classB.print() = " + print(classB));
  }

  public static String print(ClassA classA) {
    return classA.toString();
  }
}
