package com.psybergate.grad2021.core.oopart1.demo2a;

public abstract class Shape {
  private final String TYPE = "shape";

  public void print() {
    System.out.println("Dimension/s are: " + dimensions() + " and Area is: " + getArea());
  }

  public String getType() {
    return TYPE;
  }

  public abstract double getArea();

  public abstract String dimensions();
}
