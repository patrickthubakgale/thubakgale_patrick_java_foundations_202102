package com.psybergate.grad2021.core.oopart1.ce4a1;

public class SavingsAccount extends Account {
  private final String ACC_TYPE = "SavingsAccount";
  private double overdraft;

  public SavingsAccount(final String accountNum, final String name, final double balance, final double overdraft) {
    super(accountNum, name, balance);
    this.overdraft = overdraft;
  }

  public String getAccountType() {
    return ACC_TYPE;
  }

  public void deposit(final double amount) {
    this.balance += amount;
  }

  @Override public boolean isOverdrawn() {
    return getBalance() < -(overdraft);
  }

  @Override
  public boolean needsToBeReviewed() {
    return getBalance() < 0;
  }
}
