package com.psybergate.grad2021.core.oopart1.hw5a;

public class CurrentAccount extends Account {
  private final double minBalance = 1000;
  private double overdraft;

  public CurrentAccount(final String accountNum, final double balance, final double overdraft) {
    super(accountNum, balance);
    this.overdraft = overdraft;
  }

  @Override
  public void deposit(final double amount) {
    this.balance += amount;
  }

  @Override
  public void withdraw(double amount) {
    if (this.balance < amount) {
      System.out.println("Insufficient funds");
    }
    this.balance -= amount;
  }

  public boolean isOverdrawn() {
    return getBalance() < -(overdraft);
  }

  public boolean needsToBeReviewed() {
    return isOverdrawn() && (getBalance() > -((1+0.2) * overdraft) || getBalance() <= -50_000);
  }
}
