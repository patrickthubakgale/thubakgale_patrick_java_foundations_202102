package com.psybergate.grad2021.core.oopart1.demo2a;

public class Circle extends Shape {
  private final String TYPE = "Circle";
  private double radius;

  public Circle(double radius) {
    this.radius = radius;
  }

  @Override
  public String getType() {
    return TYPE;
  }

  @Override
  public double getArea() {
    return Math.PI * radius * radius;
  }

  @Override
  public String dimensions() {
    return "radius is: " + radius;
  }
}
