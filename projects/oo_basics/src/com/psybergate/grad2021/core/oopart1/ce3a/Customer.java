package com.psybergate.grad2021.core.oopart1.ce3a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
  private String name;
  private String customerNumber;
  private String customerAddress;
  private List<CurrentAccount> accounts = new ArrayList<>();

  public Customer(String name, String customerNumber, String customerAddress) {
    this.name = name;
    this.customerNumber = customerNumber;
    this.customerAddress = customerAddress;
  }

  public void addCurrentAccount(CurrentAccount account) {
    accounts.add(account);
  }

  public List<CurrentAccount> getAccounts() {
    return accounts;
  }

  public String getName() {
    return name;
  }

  public double getTotal() {
    double totalBalances = 0;
    for (CurrentAccount currentAccount : getAccounts()) {
      totalBalances += currentAccount.getBalance();
    }
    return totalBalances;
  }
}