package com.psybergate.grad2021.core.oopart1.hw3a2;

public class Rectangle {
  private static final double MAX_AREA = 15000;
  private static final double MAX_SIDE1 = 250;
  private static final double MAX_SIDE2 = 150;

  private double side1;
  private double side2;

  public Rectangle(double side1, double side2) {
    if (validateInputs(side1, side2)) {
      throw new RuntimeException("Violated constraints!");
    }
    this.side1 = side1;
    this.side2 = side2;
  }

  private boolean validateInputs(final double side1, final double side2) {
    return (side1 > MAX_SIDE1 || side2 > MAX_SIDE2 || (side1 * side2) > MAX_AREA);
  }

  public double area() {
    return side1 * side2;
  }

  public static double getMaxArea() {
    return MAX_AREA;
  }

  public static double getMaxSide1() {
    return MAX_SIDE1;
  }

  public static double getMaxSide2() {
    return MAX_SIDE2;
  }
}
