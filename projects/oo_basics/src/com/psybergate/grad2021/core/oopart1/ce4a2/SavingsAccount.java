package com.psybergate.grad2021.core.oopart1.ce4a2;

public class SavingsAccount extends Account {
  private final String ACC_TYPE = "SavingsAccount";
  private final double overdraft;

  public SavingsAccount(final String accountNumber, final String name, final double balance,
                        final double overdraft) {
    super(accountNumber, name, balance);
    if (validateBalance(balance)) {
      this.balance = 0;
    }
    this.overdraft = overdraft;
  }

  private boolean validateBalance(final double balance) {
    return balance < 0;
  }

  public String getAccountType() {
    return ACC_TYPE;
  }

  @Override
  public boolean needsToBeReviewed() {
    return getBalance() < 2000;
  }

  @Override
  public boolean isOverdrawn() {
    return getBalance() < 5000;
  }
}