package com.psybergate.grad2021.core.oopart1.ce4a1;

public class AccountUtils {
  public static void main(String[] args) {
    Account account1 = new CurrentAccount("1487", "FNB", 1000, 2000);
    Account account2 = new SavingsAccount("1234", "Investec", 500, 3000);

    System.out.println("account1.getAccountType() = " + account1.getAccountType());
    System.out.println("account2.getAccountType() = " + account2.getAccountType());

    System.out.println("Current balance is: " + account2.getBalance());
    SavingsAccount savingsAccount = (SavingsAccount) account2;
    savingsAccount.deposit(6000);
    System.out.println("New balance is: " + account2.getBalance());
  }
}
