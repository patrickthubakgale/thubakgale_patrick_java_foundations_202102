package com.psybergate.grad2021.core.oopart1.ce2a;

public abstract class Account {
  private final String ACC_TYPE = "Account";
  private final String accountNum;
  private final String name;
  private double balance;

  public Account(final String accountNum, final String name, final double balance) {
    this.accountNum = accountNum;
    this.name = name;
    this.balance = balance;
  }

  public String getAccountType() {
    return ACC_TYPE;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(final double balance) {
    //this.balance = balance;
  }

  public void setName(final String name) {
    //this.name = name;
  }

  public void deposit(final double amount) {
    balance += amount;
  }

  public void withdraw(final double amount) {
    if (balance > amount) { //do this for now
      balance -= amount;
    }
  }

  public String getName() {
    return name;
  }

  public void setSurname(final String surname) {
    //this.surname = surname;
  }

  public abstract boolean needsToBeReviewed();
  public abstract boolean isOverdrawn();
}
